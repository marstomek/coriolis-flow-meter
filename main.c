#include <mcs51\ADuC84x.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "hd44780.h"
#include "CRC16_tab.h"
#include "time_delay.h"
//------- Definicja portow wejsciowych ------------------
#define  K1 P1_0		// Wejscie analogowe pomiaru temperatury
#define  S1 P1_1		// (T2EX) sygnal z przezutnika D
#define	 S2 P3_2		// (INT0, Bramka Licznika T0) sygnal z  przezutnika D
#define                 P_16        0xA001
// -------- Deklaracje zmiennych globalnych w Inernal RAM ----------------------
static char buffer[20]; //bufor znakowy
static unsigned char wiadomosc[12];	  // Tablica wyników pomiarów
static unsigned char str[10];         // Do wyświetlenia na LCD
static unsigned int WynikADC;	// Wynik przetwarzania ADC
static unsigned int CzasT2;	// Wynik pomiaru czasu na Liczniku T2
static unsigned int WynikOkres;	// Wynik pomiaru czasu (przyrost czasu na Liczniku T2)
static unsigned int WynikFaza;	// Wynik pomiaru fazy (czas na liczniku T0)
static bool NoweWyniki=0;	// Znacznik "nowe wyniki"  1 wyniki 0 czekaj
static int n=0;             // Liczba do średniej arytmetycznej
static float sr_przeplM=0;     //
static float sr_przeplO=0;     // Średni przepływ objętościowy
static float calk_przeplM=0;
static float calk_przeplO=0;   // Całkoity
static unsigned short crc_modbus; // CRC wysyłany do mastera
static unsigned char zapytanie_master[8];

static unsigned char Znak, literka;
static bool modbus=0; //modbus uruchamia modbusa
//---------- Predefinicje funkcji pomocniczych ---------------------------------
unsigned short update_crc_16( unsigned short crc, char c );
//static void init_crc16_tab( void );
void putchar(unsigned char);		// Wysy³anie znaku do RS232
float Obl_przeplywM(void);
float Obl_przeplywO(void);
float Obl_czestotliwosc(void);
float Obl_gestosc(void);
extern char getchar(void);
//---------- Przerwanie zegarowe T0 (przekroczenie czasu)-----------------------
void IntO_int (void) __interrupt (0) 	// przerwanie z Int0
  {
//  WynikFaza = (TH0<<8)+TL0;         	// Przepisz wynik pomiaru fazy z Licznika T0
  __asm									/**Kod assemblera**/
  mov _WynikFaza,TL0					/**Przepisz dolną czêœæ fazy z licznika T0**/
  mov _WynikFaza+1,TH0					/**Przepisz górną czêœæ fazy z licznika T0**/
  __endasm;								/**Koniec kodu assemblera**/
  TH0 = 0;			  	// Zeruj czêœæ doln¹ licznika T0
  TL0 = 0;				// Zeruj czêœæ górn¹ licznika T0
  WynikOkres = CzasT2;		  	// Zapamietaj poprzedni wynik pomiaru czasu
//  CzasT2 = (RCAP2H<<8)+RCAP2L;      	// Przepisz wynik aktualnego pomiaru okresu
  __asm									/**Kod assemblera**/
  mov _CzasT2,RCAP2L					/**Przepisz czêœæ górn¹ aktualnego pomiaru okresu**/
  mov _CzasT2+1,RCAP2H					/**Przepisz czêœæ doln¹ aktualnego pomiaru okresu**/
  __endasm;								/**Koniec kodu assemblera**/
  WynikOkres =CzasT2-WynikOkres;  	// Oblicz okres jako przyrost czasu
  NoweWyniki = 1;			// Ustaw znacznik  "nowe wyniki"
  }
//---------- Program glowny ----------------------------------------------------
void main(void)
  {
  int i;

  P1=0b00000001;		// p1.0  Wejscie analogowe reszta cyfrowe ( 0 cyfrowe, 1 analogowe)
  // ---- Inicjacja portu szeregowego taktowanie z T3 (BaudRateGenerator)--------
  T3CON =0x82;			// T3BAUDEN(0x80) + DIV ( DIV=log(Fosc/16/BaudRate)/log(2)) 115200
  T3FD =40;			// T3FD (2*Fosc/2^(DIV-1)/BaudRate)-64
  //SCON =0x50;      		// Tryb pracy portu szeregowego mode 1 , Enable receiver=10H
  SCON = SM1 | REN;
  TI=1;					//Flaga transmisji portu szeregowego
  // ---- Inicjacja przetwornika ADC
  ADCCON1 =0xB4;		// Tryb paracy przetwornika ADC (ADC Clock 12MHz/3 )
  ADCCON2 =0x00;			// Ustawienie kanalu przetwornika KANAL 0
  SCONV =0;			// Gotowosc do konwersji
  // ---- Inicjacja licznika T0
  TH0 =0;			//Zeruj starsz¹ czêœæ licznika T0
  TL0 =0;			//Zeruj m³odsz¹ czêœæ licznika T0
  TMOD =0b00001001;		// T1: wyl,  T0: Gate0=1, C/T0=0 tryb0=01
  TR0 =1;			// T0 start zliczania
  // ---- Inicjacja licznika T2
  TH2 = 0;			// Zeruj czêœæ górną licznika T2
  TL2 = 0;			// Zeruj czêœæ dolną licznika T2
  CAP2 = 1;			// T2 praca z zatraskiwaniem wartosci licznika
  CNT2 = 0;			// Czasomierz (zliczanie impulsow 12 MHz)
  EXEN2 = 1;			// Aktywacja wejscia T2EX (P1.1)
  TR2 = 1;			// Sterowanie zliczaniem (1 start zliczania)
  // ---- Inicjacja systemu przerwan
  IT0 =1;			// I0 zglaszane zboczem opadajacym
  EX0 =1;			// Odblokuj przerwanie od INT0
  EA =1;			// Odblokuj niezablokowane przerwania
  //------Inicjalizacja zegara zewnętrznego
  CFG842 = EXTCLK; // Okblokowuje źródło zegara zewnętrznego 32,768kHz
  TIMECON = TCEN | ITS0; // Inicjalizacja TIC, interwal 1/128s
  INTVAL=127; // Ustawia bit TII rejestru CFG842 co sekunde
  LCD_Initalize(); // Inicjalizacja LCD
  LCD_Clear();

  while(1)			//***********************Pêtla g³ówna programu**************************
     {
     if (NoweWyniki)		// jesli nowe wyniki to wyslij
        {
        crc_modbus=0xffff; // CRC dla modbus musi zaczynac się od ffff
  	    SCONV =1;     	      	// Inicjuj pomiar w kanale 0
        NoweWyniki =0;		    // Kasuj znacznik "nowe wyniki"

//**** Jeżeli jest zapytanie od MASTERA do urządzenia o adresie 17 to:******************
// timeout w tym miejscu
    if (modbus==1)
    {
		for (i=0; i<8; i++){
            zapytanie_master[i]=getchar();
            }
        // Sprawdzenie crc zapytania
        for (i=0; i<8; i++){
                crc_modbus=update_crc_16(crc_modbus,zapytanie_master[i]);
            }
		}
//****** sprawdzenie crc, adres 17, czy żąda odczytu rejestrów	*********
		if (crc_modbus==0 && (zapytanie_master[0]==0x11) && (zapytanie_master[1]==0x03)){
        //Switch(których rejestrów żądam?)
        crc_modbus=0xffff; // CRC dla modbus musi zaczynac się od ffff
		wiadomosc[0]=0x11;		// Adres SLAVE 17
		wiadomosc[1]=0x03;		// Funkcja - czystanie stanu rejestrów
		wiadomosc[3]=0x06;      // Liczba bajtów do wysłania
		wiadomosc[4]=WynikOkres>>8;	// Starsza czesc Okresu
		wiadomosc[5]=WynikOkres;	// Mlodsza czesc Okresu
		wiadomosc[6]=WynikFaza>>8;	// Starsza cesc Fazy
		wiadomosc[7]=WynikFaza;	// Mlodsza czesc Fazy
        while (SCONV);		    // Czekaj na koniec konwersji
		wiadomosc[8]=ADCDATAH;	// Wyslij czesc starsza Temperatury
		wiadomosc[9]=ADCDATAL;	// Wyslij czesc mlodsza Temperatury
		_delay_us(200);
		_delay_us(147);// Czas 4 znaków
		for (i=0; i<=9; i++){
                crc_modbus=update_crc_16(crc_modbus,wiadomosc[i]);
		}
		wiadomosc[10]=crc_modbus;       //Część mlodsza
		wiadomosc[11]=crc_modbus>>8;    //Część starsza
		//Wysyłanie wiadomości do mastera po UART
		for (i=0; i<=11; i++){
            putchar(wiadomosc[i]);
            }
		}
    }

//***********************Pisanie na wyświetlaczu LCD*********************************************
		//z czestotliwością co 1s, domyślne uśrednianie wynosi tyle ile wyników nadejdzie przez 1s
				if(TIMECON&TII) // Sprawdza czy upłynęła sekunda
		{
		    TIMECON = ~TII; // Zeruje bit, odliczanie czasu od nowa
		    sr_przeplM = sr_przeplM/(float)n; // Uśrednianie
		    sr_przeplO = sr_przeplO/(float)n;
		    n=0;
		    //całkuj co 1s
		    calk_przeplM+=sr_przeplM; //całkowanie przepływu
		    calk_przeplO+=sr_przeplO;
		    //wypisz LCD
		    sprintf(str,"fi_m %f ", sr_przeplM);
		    LCD_WriteText(str);
		    sprintf(str,"fi_m %f ", sr_przeplO);
		    LCD_WriteText(str);
			//...
		}
		else // Jeśli trwa aktualna sekunda
        {
            n++;    //dzielnik do średniej arytmetycznej
            sr_przeplM+=Obl_przeplywM();    //
            sr_przeplO+=Obl_przeplywO();    //

        }
     if(!RI) continue;				// Brak znaku skok na poczatek petli (czekaj na odbior znaku)
     RI =0;				// Zeruj flage odebranego znaku
     Znak =SBUF;		//Wysy³a znak do bufora nadawczego
     switch (Znak)
      	{
      	case 'A':			// Rzadanie potwierdzenia komunikacji
        	putchar('E');		// Potwierdzenie komunikacji
      	break;
		case 'B':
			putchar('e');
			literka=getchar();
        break;
        case 'M':
            modbus=1;
        break;
        case 'm':
            modbus=0;
        break;
	default:			// Odeslij nierozpoznany znak
		putchar(Znak);  // Odsy³a nierozpoznany znak
	break;
        }
    }
  }
// =========== Funkcje pomocnicze ==============================================

// ------------ wyslanie znaku do RS232 -----------------------------------------
void putchar(unsigned char V)
  {
  while(! TI);				// Czekaj na ewentualny koniec wyslania
  TI =0;				// Zeruj flage wyslania
  SBUF=V;				// Wyslij znak do bufora nadawczego
  }

extern char getchar(void)
{
    unsigned char znak1;
	while(!RI); //czekaj aż odbierze
	RI=0;
	znak1 = SBUF;
    return znak1;
}
//---------------------CRC-------------------------------------------------------
    /*******************************************************************\
    *                                                                   *
    *   unsigned short update_crc_16( unsigned short crc, char c );     *
    *                                                                   *
    *   The function update_crc_16 calculates a  new  CRC-16  value     *
    *   based  on  the  previous value of the CRC and the next byte     *
    *   of the data to be checked.                                      *
    *                                                                   *
    \*******************************************************************/

unsigned short update_crc_16( unsigned short crc, char c )
{
    unsigned short tmp, short_c;

    short_c = 0x00ff & (unsigned short) c;

    tmp =  crc       ^ short_c;
    crc = (crc >> 8) ^ crc_tab16[ tmp & 0xff ];

    return crc;
}
//--------------------Obliczanie przeplywów i gestości--------------------------------------
float Obl_przeplywM(void)
{
    float fiM;
    fiM = (float)WynikFaza;
    fiM = (0.8096*fiM)-127.26;
    return fiM;
}

float Obl_czestotliwosc(void)
{
    float f;
    f = (float)WynikOkres/12000000.0;
    f = 1/f;
    return f;
}

float Obl_przeplywO(void)
{
    float fiO;
    fiO = Obl_przeplywM()/Obl_gestosc();
    return fiO;
}

float Obl_gestosc(void)
{
    float rho;
    rho = (powf(81.92543/Obl_czestotliwosc(),2)-1)/0.084090608;
    return rho;
}
//---------- Koniec programu ---------------------------------------------------
