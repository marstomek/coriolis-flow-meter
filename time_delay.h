#ifndef TIME_DELAY_H_INCLUDED
#define TIME_DELAY_H_INCLUDED
void _delay_us(unsigned char delay);
void _delay_ms(unsigned char delay);
#endif // TIME_DELAY_H_INCLUDED
