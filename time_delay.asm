;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.2.0 #8008 (Jul  6 2012) (MINGW32)
; This file was generated Tue Feb 05 12:21:29 2013
;--------------------------------------------------------
	.module time_delay
	.optsdcc -mmcs51 --model-small
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl __delay_us
	.globl __delay_ms
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function '_delay_us'
;------------------------------------------------------------
;delay                     Allocated to registers r7 
;t                         Allocated to registers 
;------------------------------------------------------------
;	time_delay.c:7: void _delay_us(unsigned char delay) //opoznienie czasowy us.
;	-----------------------------------------
;	 function _delay_us
;	-----------------------------------------
__delay_us:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
;	time_delay.c:9: unsigned char t=delay*3; //
	mov	a,dpl
	mov	b,#0x03
	mul	ab
	mov	r7,a
;	time_delay.c:11: while(t!=0) // operacja trwa 4 takty 12 to 1us
00101$:
	mov	a,r7
	jz	00104$
;	time_delay.c:12: t--;
	dec	r7
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function '_delay_ms'
;------------------------------------------------------------
;delay                     Allocated to registers 
;t                         Allocated to registers 
;------------------------------------------------------------
;	time_delay.c:15: void _delay_ms(unsigned char delay)
;	-----------------------------------------
;	 function _delay_ms
;	-----------------------------------------
__delay_ms:
;	time_delay.c:18: _delay_us(250);
	mov	dpl,#0xFA
	lcall	__delay_us
;	time_delay.c:19: _delay_us(250);
	mov	dpl,#0xFA
	lcall	__delay_us
;	time_delay.c:20: _delay_us(250);
	mov	dpl,#0xFA
	lcall	__delay_us
;	time_delay.c:21: _delay_us(250);
	mov	dpl,#0xFA
	ljmp	__delay_us
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
