/**-----------------------------
*Generuje opóźniania czasowe w milisekundach i w mikrosekundach
*/
#include "time_delay.h"


void _delay_us(unsigned char delay) //opoznienie czasowy us.
{
    unsigned char t=delay*3; //

    while(t!=0) // operacja trwa 4 takty 12 to 1us
        t--;
}

void _delay_ms(unsigned char delay)
{
    unsigned char t=delay;
    _delay_us(250);
    _delay_us(250);
    _delay_us(250);
    _delay_us(250);
}
