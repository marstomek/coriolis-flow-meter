;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.2.0 #8008 (Jul  6 2012) (MINGW32)
; This file was generated Tue Feb 05 12:21:29 2013
;--------------------------------------------------------
	.module HD44780
	.optsdcc -mmcs51 --model-small
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl __LCD_Write
	.globl __LCD_OutNibble
	.globl __delay_ms
	.globl __delay_us
	.globl _SPR0
	.globl _SPR1
	.globl _CPHA
	.globl _CPOL
	.globl _SPIM
	.globl _SPE
	.globl _WCOL
	.globl _ISPI
	.globl _CS0
	.globl _CS1
	.globl _CS2
	.globl _CS3
	.globl _SCONV
	.globl _CCONV
	.globl _DMA
	.globl _ADCI
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _I2CI
	.globl _I2CTX
	.globl _I2CRS
	.globl _I2CM
	.globl _I2CID0
	.globl _I2CID1
	.globl _I2CGC
	.globl _I2CSI
	.globl _MDI
	.globl _MCO
	.globl _MDE
	.globl _MDO
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _CAP2
	.globl _CNT2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK
	.globl _RCLK
	.globl _EXF2
	.globl _TF2
	.globl _WDWR
	.globl _WDE
	.globl _WDS
	.globl _WDIR
	.globl _PRE0
	.globl _PRE1
	.globl _PRE2
	.globl _PRE3
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _PADC
	.globl _PSI
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES
	.globl _ET2
	.globl _EADC
	.globl _EA
	.globl _RI
	.globl _TI
	.globl _RB8
	.globl _TB8
	.globl _REN
	.globl _SM2
	.globl _SM1
	.globl _SM0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _T2
	.globl _T2EX
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl _DACCON
	.globl _DAC1H
	.globl _DAC1L
	.globl _DAC0H
	.globl _DAC0L
	.globl _SPICON
	.globl _SPIDAT
	.globl _ADCGAINH
	.globl _ADCGAINL
	.globl _ADCOFSH
	.globl _ADCOFSL
	.globl _ADCDATAH
	.globl _ADCDATAL
	.globl _ADCCON3
	.globl _ADCCON2
	.globl _ADCCON1
	.globl _B
	.globl _I2CCON
	.globl _ACC
	.globl _PSMCON
	.globl _PLLCON
	.globl _DMAP
	.globl _DMAH
	.globl _DMAL
	.globl _PSW
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _CHIPID
	.globl _WDCON
	.globl _EADRH
	.globl _EADRL
	.globl _EDATA4
	.globl _EDATA3
	.globl _EDATA2
	.globl _EDATA1
	.globl _ECON
	.globl _IP
	.globl _PWM1H
	.globl _PWM1L
	.globl _PWM0H
	.globl _PWM0L
	.globl _PWMCON
	.globl _IEIP2
	.globl _IE
	.globl _INTVAL
	.globl _HOUR
	.globl _MIN
	.globl _SEC
	.globl _HTHSEC
	.globl _TIMECON
	.globl _T3CON
	.globl _T3FD
	.globl _SBUF
	.globl _SCON
	.globl _I2CDAT
	.globl _I2CADD3
	.globl _I2CADD2
	.globl _I2CADD1
	.globl _I2CADD
	.globl _P3
	.globl _P2
	.globl _P1
	.globl _P0
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPCON
	.globl _DPP
	.globl _DPH
	.globl _DPL
	.globl _SPH
	.globl _SP
	.globl _CFG842
	.globl _CFG841
	.globl _LCD_GoTo_PARM_2
	.globl _LCD_WriteCommand
	.globl _LCD_WriteData
	.globl _LCD_WriteText
	.globl _LCD_GoTo
	.globl _LCD_Clear
	.globl _LCD_Home
	.globl _LCD_Initalize
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_CFG841	=	0x00af
_CFG842	=	0x00af
_SP	=	0x0081
_SPH	=	0x00b7
_DPL	=	0x0082
_DPH	=	0x0083
_DPP	=	0x0084
_DPCON	=	0x00a7
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P0	=	0x0080
_P1	=	0x0090
_P2	=	0x00a0
_P3	=	0x00b0
_I2CADD	=	0x009b
_I2CADD1	=	0x0091
_I2CADD2	=	0x0092
_I2CADD3	=	0x0093
_I2CDAT	=	0x009a
_SCON	=	0x0098
_SBUF	=	0x0099
_T3FD	=	0x009d
_T3CON	=	0x009e
_TIMECON	=	0x00a1
_HTHSEC	=	0x00a2
_SEC	=	0x00a3
_MIN	=	0x00a4
_HOUR	=	0x00a5
_INTVAL	=	0x00a6
_IE	=	0x00a8
_IEIP2	=	0x00a9
_PWMCON	=	0x00ae
_PWM0L	=	0x00b1
_PWM0H	=	0x00b2
_PWM1L	=	0x00b3
_PWM1H	=	0x00b4
_IP	=	0x00b8
_ECON	=	0x00b9
_EDATA1	=	0x00bc
_EDATA2	=	0x00bd
_EDATA3	=	0x00be
_EDATA4	=	0x00bf
_EADRL	=	0x00c6
_EADRH	=	0x00c7
_WDCON	=	0x00c0
_CHIPID	=	0x00c2
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_PSW	=	0x00d0
_DMAL	=	0x00d2
_DMAH	=	0x00d3
_DMAP	=	0x00d4
_PLLCON	=	0x00d7
_PSMCON	=	0x00df
_ACC	=	0x00e0
_I2CCON	=	0x00e8
_B	=	0x00f0
_ADCCON1	=	0x00ef
_ADCCON2	=	0x00d8
_ADCCON3	=	0x00f5
_ADCDATAL	=	0x00d9
_ADCDATAH	=	0x00da
_ADCOFSL	=	0x00f1
_ADCOFSH	=	0x00f2
_ADCGAINL	=	0x00f3
_ADCGAINH	=	0x00f4
_SPIDAT	=	0x00f7
_SPICON	=	0x00f8
_DAC0L	=	0x00f9
_DAC0H	=	0x00fa
_DAC1L	=	0x00fb
_DAC1H	=	0x00fc
_DACCON	=	0x00fd
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_TF1	=	0x008f
_TR1	=	0x008e
_TF0	=	0x008d
_TR0	=	0x008c
_IE1	=	0x008b
_IT1	=	0x008a
_IE0	=	0x0089
_IT0	=	0x0088
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_T2EX	=	0x0091
_T2	=	0x0090
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_SM0	=	0x009f
_SM1	=	0x009e
_SM2	=	0x009d
_REN	=	0x009c
_TB8	=	0x009b
_RB8	=	0x009a
_TI	=	0x0099
_RI	=	0x0098
_EA	=	0x00af
_EADC	=	0x00ae
_ET2	=	0x00ad
_ES	=	0x00ac
_ET1	=	0x00ab
_EX1	=	0x00aa
_ET0	=	0x00a9
_EX0	=	0x00a8
_PSI	=	0x00bf
_PADC	=	0x00be
_PT2	=	0x00bd
_PS	=	0x00bc
_PT1	=	0x00bb
_PX1	=	0x00ba
_PT0	=	0x00b9
_PX0	=	0x00b8
_PRE3	=	0x00c7
_PRE2	=	0x00c6
_PRE1	=	0x00c5
_PRE0	=	0x00c4
_WDIR	=	0x00c3
_WDS	=	0x00c2
_WDE	=	0x00c1
_WDWR	=	0x00c0
_TF2	=	0x00cf
_EXF2	=	0x00ce
_RCLK	=	0x00cd
_TCLK	=	0x00cc
_EXEN2	=	0x00cb
_TR2	=	0x00ca
_CNT2	=	0x00c9
_CAP2	=	0x00c8
_CY	=	0x00d7
_AC	=	0x00d6
_F0	=	0x00d5
_RS1	=	0x00d4
_RS0	=	0x00d3
_OV	=	0x00d2
_F1	=	0x00d1
_P	=	0x00d0
_ACC_0	=	0x00e0
_ACC_1	=	0x00e1
_ACC_2	=	0x00e2
_ACC_3	=	0x00e3
_ACC_4	=	0x00e4
_ACC_5	=	0x00e5
_ACC_6	=	0x00e6
_ACC_7	=	0x00e7
_MDO	=	0x00ef
_MDE	=	0x00ee
_MCO	=	0x00ed
_MDI	=	0x00ec
_I2CSI	=	0x00ef
_I2CGC	=	0x00ee
_I2CID1	=	0x00ed
_I2CID0	=	0x00ec
_I2CM	=	0x00eb
_I2CRS	=	0x00ea
_I2CTX	=	0x00e9
_I2CI	=	0x00e8
_B_0	=	0x00f0
_B_1	=	0x00f1
_B_2	=	0x00f2
_B_3	=	0x00f3
_B_4	=	0x00f4
_B_5	=	0x00f5
_B_6	=	0x00f6
_B_7	=	0x00f7
_ADCI	=	0x00df
_DMA	=	0x00de
_CCONV	=	0x00dd
_SCONV	=	0x00dc
_CS3	=	0x00db
_CS2	=	0x00da
_CS1	=	0x00d9
_CS0	=	0x00d8
_ISPI	=	0x00ff
_WCOL	=	0x00fe
_SPE	=	0x00fd
_SPIM	=	0x00fc
_CPOL	=	0x00fb
_CPHA	=	0x00fa
_SPR1	=	0x00f9
_SPR0	=	0x00f8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_LCD_GoTo_PARM_2:
	.ds 1
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
__LCD_OutNibble_sloc0_1_0:
	.ds 1
__LCD_Write_sloc0_1_0:
	.ds 1
_LCD_WriteData_sloc0_1_0:
	.ds 1
_LCD_Initalize_sloc0_1_0:
	.ds 1
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function '_LCD_OutNibble'
;------------------------------------------------------------
;nibbleToWrite             Allocated to registers r7 
;------------------------------------------------------------
;	HD44780.c:21: void _LCD_OutNibble(unsigned char nibbleToWrite)
;	-----------------------------------------
;	 function _LCD_OutNibble
;	-----------------------------------------
__LCD_OutNibble:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
;	HD44780.c:23: if(nibbleToWrite & 0x01)
	mov	a,dpl
	mov	r7,a
	jnb	acc.0,00102$
;	HD44780.c:24: LCD_DB4_PORT |= LCD_DB4;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_3
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_3,c
	sjmp	00103$
00102$:
;	HD44780.c:26: LCD_DB4_PORT  &= ~LCD_DB4;
	mov	c,_P2_3
	mov	_P2_3,c
00103$:
;	HD44780.c:28: if(nibbleToWrite & 0x02)
	mov	a,r7
	jnb	acc.1,00105$
;	HD44780.c:29: LCD_DB5_PORT |= LCD_DB5;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_4
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_4,c
	sjmp	00106$
00105$:
;	HD44780.c:31: LCD_DB5_PORT  &= ~LCD_DB5;
	mov	c,_P2_4
	mov	_P2_4,c
00106$:
;	HD44780.c:33: if(nibbleToWrite & 0x04)
	mov	a,r7
	jnb	acc.2,00108$
;	HD44780.c:34: LCD_DB6_PORT |= LCD_DB6;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_5
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_5,c
	sjmp	00109$
00108$:
;	HD44780.c:36: LCD_DB6_PORT  &= ~LCD_DB6;
	mov	c,_P2_5
	mov	_P2_5,c
00109$:
;	HD44780.c:38: if(nibbleToWrite & 0x08)
	mov	a,r7
	jnb	acc.3,00111$
;	HD44780.c:39: LCD_DB7_PORT |= LCD_DB7;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_6
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_6,c
	ret
00111$:
;	HD44780.c:41: LCD_DB7_PORT  &= ~LCD_DB7;
	mov	c,_P2_6
	mov	_P2_6,c
	ret
;------------------------------------------------------------
;Allocation info for local variables in function '_LCD_Write'
;------------------------------------------------------------
;dataToWrite               Allocated to registers r7 
;------------------------------------------------------------
;	HD44780.c:48: void _LCD_Write(unsigned char dataToWrite)
;	-----------------------------------------
;	 function _LCD_Write
;	-----------------------------------------
__LCD_Write:
	mov	r7,dpl
;	HD44780.c:50: LCD_E_PORT |= LCD_E;
	setb	__LCD_Write_sloc0_1_0
	mov	c,_P2_2
	mov	c,__LCD_Write_sloc0_1_0
	mov	_P2_2,c
;	HD44780.c:51: _LCD_OutNibble(dataToWrite >> 4);
	mov	a,r7
	swap	a
	anl	a,#0x0F
	mov	dpl,a
	push	ar7
	lcall	__LCD_OutNibble
	pop	ar7
;	HD44780.c:52: LCD_E_PORT &= ~LCD_E;
	mov	c,_P2_2
	mov	_P2_2,c
;	HD44780.c:53: LCD_E_PORT |= LCD_E;
	setb	__LCD_Write_sloc0_1_0
	mov	c,_P2_2
	mov	c,__LCD_Write_sloc0_1_0
	mov	_P2_2,c
;	HD44780.c:54: _LCD_OutNibble(dataToWrite);
	mov	dpl,r7
	lcall	__LCD_OutNibble
;	HD44780.c:55: LCD_E_PORT &= ~LCD_E;
	mov	c,_P2_2
	mov	_P2_2,c
;	HD44780.c:56: _delay_us(50);
	mov	dpl,#0x32
	ljmp	__delay_us
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_WriteCommand'
;------------------------------------------------------------
;commandToWrite            Allocated to registers 
;------------------------------------------------------------
;	HD44780.c:63: void LCD_WriteCommand(unsigned char commandToWrite)
;	-----------------------------------------
;	 function LCD_WriteCommand
;	-----------------------------------------
_LCD_WriteCommand:
;	HD44780.c:65: LCD_RS_PORT &= ~LCD_RS;
	mov	c,_P2_1
	mov	_P2_1,c
;	HD44780.c:66: _LCD_Write(commandToWrite);
	ljmp	__LCD_Write
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_WriteData'
;------------------------------------------------------------
;dataToWrite               Allocated to registers 
;------------------------------------------------------------
;	HD44780.c:73: void LCD_WriteData(unsigned char dataToWrite)
;	-----------------------------------------
;	 function LCD_WriteData
;	-----------------------------------------
_LCD_WriteData:
;	HD44780.c:75: LCD_RS_PORT |= LCD_RS;
	setb	_LCD_WriteData_sloc0_1_0
	mov	c,_P2_1
	mov	c,_LCD_WriteData_sloc0_1_0
	mov	_P2_1,c
;	HD44780.c:76: _LCD_Write(dataToWrite);
	ljmp	__LCD_Write
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_WriteText'
;------------------------------------------------------------
;text                      Allocated to registers 
;------------------------------------------------------------
;	HD44780.c:83: void LCD_WriteText(char * text)
;	-----------------------------------------
;	 function LCD_WriteText
;	-----------------------------------------
_LCD_WriteText:
	mov	r5,dpl
	mov	r6,dph
	mov	r7,b
;	HD44780.c:85: while(*text)
00101$:
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r4,a
	jz	00104$
;	HD44780.c:86: LCD_WriteData(*text++);
	mov	dpl,r4
	inc	r5
	cjne	r5,#0x00,00112$
	inc	r6
00112$:
	push	ar7
	push	ar6
	push	ar5
	lcall	_LCD_WriteData
	pop	ar5
	pop	ar6
	pop	ar7
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_GoTo'
;------------------------------------------------------------
;y                         Allocated with name '_LCD_GoTo_PARM_2'
;x                         Allocated to registers r7 
;------------------------------------------------------------
;	HD44780.c:93: void LCD_GoTo(unsigned char x, unsigned char y)
;	-----------------------------------------
;	 function LCD_GoTo
;	-----------------------------------------
_LCD_GoTo:
	mov	r7,dpl
;	HD44780.c:95: LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
	mov	a,_LCD_GoTo_PARM_2
	rr	a
	rr	a
	anl	a,#0xC0
	add	a,r7
	orl	a,#0x80
	mov	dpl,a
	ljmp	_LCD_WriteCommand
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_Clear'
;------------------------------------------------------------
;	HD44780.c:102: void LCD_Clear(void)
;	-----------------------------------------
;	 function LCD_Clear
;	-----------------------------------------
_LCD_Clear:
;	HD44780.c:104: LCD_WriteCommand(HD44780_CLEAR);
	mov	dpl,#0x01
	lcall	_LCD_WriteCommand
;	HD44780.c:105: _delay_ms(2);
	mov	dpl,#0x02
	ljmp	__delay_ms
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_Home'
;------------------------------------------------------------
;	HD44780.c:112: void LCD_Home(void)
;	-----------------------------------------
;	 function LCD_Home
;	-----------------------------------------
_LCD_Home:
;	HD44780.c:114: LCD_WriteCommand(HD44780_HOME);
	mov	dpl,#0x02
	lcall	_LCD_WriteCommand
;	HD44780.c:115: _delay_ms(2);
	mov	dpl,#0x02
	ljmp	__delay_ms
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_Initalize'
;------------------------------------------------------------
;i                         Allocated to registers r7 
;------------------------------------------------------------
;	HD44780.c:122: void LCD_Initalize(void)
;	-----------------------------------------
;	 function LCD_Initalize
;	-----------------------------------------
_LCD_Initalize:
;	HD44780.c:125: LCD_DB4_DIR |= LCD_DB4; // Konfiguracja kierunku pracy wyprowadze�
	orl	_P2,#0x10
;	HD44780.c:126: LCD_DB5_DIR |= LCD_DB5; //
	orl	_P2,#0x20
;	HD44780.c:127: LCD_DB6_DIR |= LCD_DB6; //
	orl	_P2,#0x40
;	HD44780.c:128: LCD_DB7_DIR |= LCD_DB7; //
	orl	_P2,#0x80
;	HD44780.c:129: LCD_E_DIR 	|= LCD_E;   //
	orl	_P2,#0x08
;	HD44780.c:130: LCD_RS_DIR 	|= LCD_RS;  //
	orl	_P2,#0x04
;	HD44780.c:131: _delay_ms(15); // oczekiwanie na ustalibizowanie si� napiecia zasilajacego
	mov	dpl,#0x0F
	lcall	__delay_ms
;	HD44780.c:132: LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
	mov	c,_P2_1
	mov	_P2_1,c
;	HD44780.c:133: LCD_E_PORT &= ~LCD_E;  // wyzerowanie linii E
	mov	c,_P2_2
	mov	_P2_2,c
;	HD44780.c:135: for(i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
	mov	r7,#0x00
00101$:
	cjne	r7,#0x03,00112$
00112$:
	jnc	00104$
;	HD44780.c:137: LCD_E_PORT |= LCD_E; //  E = 1
	setb	_LCD_Initalize_sloc0_1_0
	mov	c,_P2_2
	mov	c,_LCD_Initalize_sloc0_1_0
	mov	_P2_2,c
;	HD44780.c:138: _LCD_OutNibble(0x03); // tryb 8-bitowy
	mov	dpl,#0x03
	push	ar7
	lcall	__LCD_OutNibble
;	HD44780.c:139: LCD_E_PORT &= ~LCD_E; // E = 0
	mov	c,_P2_2
	mov	_P2_2,c
;	HD44780.c:140: _delay_ms(5); // czekaj 5ms
	mov	dpl,#0x05
	lcall	__delay_ms
	pop	ar7
;	HD44780.c:135: for(i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
	inc	r7
	sjmp	00101$
00104$:
;	HD44780.c:143: LCD_E_PORT |= LCD_E; // E = 1
	setb	_LCD_Initalize_sloc0_1_0
	mov	c,_P2_2
	mov	c,_LCD_Initalize_sloc0_1_0
	mov	_P2_2,c
;	HD44780.c:144: _LCD_OutNibble(0x02); // tryb 4-bitowy
	mov	dpl,#0x02
	lcall	__LCD_OutNibble
;	HD44780.c:145: LCD_E_PORT &= ~LCD_E; // E = 0
	mov	c,_P2_2
	mov	_P2_2,c
;	HD44780.c:147: _delay_ms(1); // czekaj 1ms
	mov	dpl,#0x01
	lcall	__delay_ms
;	HD44780.c:148: LCD_WriteCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
	mov	dpl,#0x28
	lcall	_LCD_WriteCommand
;	HD44780.c:149: LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wy��czenie wyswietlacza
	mov	dpl,#0x08
	lcall	_LCD_WriteCommand
;	HD44780.c:150: LCD_WriteCommand(HD44780_CLEAR); // czyszczenie zawartos�i pamieci DDRAM
	mov	dpl,#0x01
	lcall	_LCD_WriteCommand
;	HD44780.c:151: _delay_ms(2);
	mov	dpl,#0x02
	lcall	__delay_ms
;	HD44780.c:152: LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);// inkrementaja adresu i przesuwanie kursora
	mov	dpl,#0x06
	lcall	_LCD_WriteCommand
;	HD44780.c:153: LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); // w��cz LCD, bez kursora i mrugania
	mov	dpl,#0x0C
	ljmp	_LCD_WriteCommand
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
