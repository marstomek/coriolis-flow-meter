                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : free open source ANSI-C Compiler
                              3 ; Version 3.2.0 #8008 (Jul  6 2012) (MINGW32)
                              4 ; This file was generated Fri Feb 08 23:32:48 2013
                              5 ;--------------------------------------------------------
                              6 	.module time_delay
                              7 	.optsdcc -mmcs51 --model-medium
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl __delay_us
                             13 	.globl __delay_ms
                             14 ;--------------------------------------------------------
                             15 ; special function registers
                             16 ;--------------------------------------------------------
                             17 	.area RSEG    (ABS,DATA)
   0000                      18 	.org 0x0000
                             19 ;--------------------------------------------------------
                             20 ; special function bits
                             21 ;--------------------------------------------------------
                             22 	.area RSEG    (ABS,DATA)
   0000                      23 	.org 0x0000
                             24 ;--------------------------------------------------------
                             25 ; overlayable register banks
                             26 ;--------------------------------------------------------
                             27 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                      28 	.ds 8
                             29 ;--------------------------------------------------------
                             30 ; internal ram data
                             31 ;--------------------------------------------------------
                             32 	.area DSEG    (DATA)
                             33 ;--------------------------------------------------------
                             34 ; overlayable items in internal ram 
                             35 ;--------------------------------------------------------
                             36 ;--------------------------------------------------------
                             37 ; indirectly addressable internal ram data
                             38 ;--------------------------------------------------------
                             39 	.area ISEG    (DATA)
                             40 ;--------------------------------------------------------
                             41 ; absolute internal ram data
                             42 ;--------------------------------------------------------
                             43 	.area IABS    (ABS,DATA)
                             44 	.area IABS    (ABS,DATA)
                             45 ;--------------------------------------------------------
                             46 ; bit data
                             47 ;--------------------------------------------------------
                             48 	.area BSEG    (BIT)
                             49 ;--------------------------------------------------------
                             50 ; paged external ram data
                             51 ;--------------------------------------------------------
                             52 	.area PSEG    (PAG,XDATA)
                             53 ;--------------------------------------------------------
                             54 ; external ram data
                             55 ;--------------------------------------------------------
                             56 	.area XSEG    (XDATA)
                             57 ;--------------------------------------------------------
                             58 ; absolute external ram data
                             59 ;--------------------------------------------------------
                             60 	.area XABS    (ABS,XDATA)
                             61 ;--------------------------------------------------------
                             62 ; external initialized ram data
                             63 ;--------------------------------------------------------
                             64 	.area XISEG   (XDATA)
                             65 	.area HOME    (CODE)
                             66 	.area GSINIT0 (CODE)
                             67 	.area GSINIT1 (CODE)
                             68 	.area GSINIT2 (CODE)
                             69 	.area GSINIT3 (CODE)
                             70 	.area GSINIT4 (CODE)
                             71 	.area GSINIT5 (CODE)
                             72 	.area GSINIT  (CODE)
                             73 	.area GSFINAL (CODE)
                             74 	.area CSEG    (CODE)
                             75 ;--------------------------------------------------------
                             76 ; global & static initialisations
                             77 ;--------------------------------------------------------
                             78 	.area HOME    (CODE)
                             79 	.area GSINIT  (CODE)
                             80 	.area GSFINAL (CODE)
                             81 	.area GSINIT  (CODE)
                             82 ;--------------------------------------------------------
                             83 ; Home
                             84 ;--------------------------------------------------------
                             85 	.area HOME    (CODE)
                             86 	.area HOME    (CODE)
                             87 ;--------------------------------------------------------
                             88 ; code
                             89 ;--------------------------------------------------------
                             90 	.area CSEG    (CODE)
                             91 ;------------------------------------------------------------
                             92 ;Allocation info for local variables in function '_delay_us'
                             93 ;------------------------------------------------------------
                    0000     94 	G$_delay_us$0$0 ==.
                    0000     95 	C$time_delay.c$7$0$0 ==.
                             96 ;	time_delay.c:7: void _delay_us(unsigned char delay) //opoznienie czasowy us.
                             97 ;	-----------------------------------------
                             98 ;	 function _delay_us
                             99 ;	-----------------------------------------
   0848                     100 __delay_us:
                    0007    101 	ar7 = 0x07
                    0006    102 	ar6 = 0x06
                    0005    103 	ar5 = 0x05
                    0004    104 	ar4 = 0x04
                    0003    105 	ar3 = 0x03
                    0002    106 	ar2 = 0x02
                    0001    107 	ar1 = 0x01
                    0000    108 	ar0 = 0x00
                    0000    109 	C$time_delay.c$9$1$4 ==.
                            110 ;	time_delay.c:9: unsigned char t=delay*3; //
   0848 E5 82               111 	mov	a,dpl
   084A 75 F0 03            112 	mov	b,#0x03
   084D A4                  113 	mul	ab
   084E FF                  114 	mov	r7,a
                    0007    115 	C$time_delay.c$11$1$4 ==.
                            116 ;	time_delay.c:11: while(t!=0) // operacja trwa 4 takty 12 to 1us
   084F                     117 00101$:
   084F EF                  118 	mov	a,r7
   0850 60 03               119 	jz	00104$
                    000A    120 	C$time_delay.c$12$1$4 ==.
                            121 ;	time_delay.c:12: t--;
   0852 1F                  122 	dec	r7
   0853 80 FA               123 	sjmp	00101$
   0855                     124 00104$:
                    000D    125 	C$time_delay.c$13$1$4 ==.
                    000D    126 	XG$_delay_us$0$0 ==.
   0855 22                  127 	ret
                            128 ;------------------------------------------------------------
                            129 ;Allocation info for local variables in function '_delay_ms'
                            130 ;------------------------------------------------------------
                    000E    131 	G$_delay_ms$0$0 ==.
                    000E    132 	C$time_delay.c$15$1$4 ==.
                            133 ;	time_delay.c:15: void _delay_ms(unsigned char delay)
                            134 ;	-----------------------------------------
                            135 ;	 function _delay_ms
                            136 ;	-----------------------------------------
   0856                     137 __delay_ms:
                    000E    138 	C$time_delay.c$18$1$6 ==.
                            139 ;	time_delay.c:18: _delay_us(250);
   0856 75 82 FA            140 	mov	dpl,#0xFA
   0859 12 08 48            141 	lcall	__delay_us
                    0014    142 	C$time_delay.c$19$1$6 ==.
                            143 ;	time_delay.c:19: _delay_us(250);
   085C 75 82 FA            144 	mov	dpl,#0xFA
   085F 12 08 48            145 	lcall	__delay_us
                    001A    146 	C$time_delay.c$20$1$6 ==.
                            147 ;	time_delay.c:20: _delay_us(250);
   0862 75 82 FA            148 	mov	dpl,#0xFA
   0865 12 08 48            149 	lcall	__delay_us
                    0020    150 	C$time_delay.c$21$1$6 ==.
                            151 ;	time_delay.c:21: _delay_us(250);
   0868 75 82 FA            152 	mov	dpl,#0xFA
   086B 12 08 48            153 	lcall	__delay_us
                    0026    154 	C$time_delay.c$22$1$6 ==.
                    0026    155 	XG$_delay_ms$0$0 ==.
   086E 22                  156 	ret
                            157 	.area CSEG    (CODE)
                            158 	.area CONST   (CODE)
                            159 	.area XINIT   (CODE)
                            160 	.area CABS    (ABS,CODE)
