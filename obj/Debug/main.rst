                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : free open source ANSI-C Compiler
                              3 ; Version 3.2.0 #8008 (Jul  6 2012) (MINGW32)
                              4 ; This file was generated Fri Feb 08 23:32:47 2013
                              5 ;--------------------------------------------------------
                              6 	.module main
                              7 	.optsdcc -mmcs51 --model-medium
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _crc_tab16
                             13 	.globl _main
                             14 	.globl _IntO_int
                             15 	.globl __delay_us
                             16 	.globl _LCD_Initalize
                             17 	.globl _LCD_Clear
                             18 	.globl _LCD_WriteText
                             19 	.globl _powf
                             20 	.globl _sprintf
                             21 	.globl _SPR0
                             22 	.globl _SPR1
                             23 	.globl _CPHA
                             24 	.globl _CPOL
                             25 	.globl _SPIM
                             26 	.globl _SPE
                             27 	.globl _WCOL
                             28 	.globl _ISPI
                             29 	.globl _CS0
                             30 	.globl _CS1
                             31 	.globl _CS2
                             32 	.globl _CS3
                             33 	.globl _SCONV
                             34 	.globl _CCONV
                             35 	.globl _DMA
                             36 	.globl _ADCI
                             37 	.globl _B_7
                             38 	.globl _B_6
                             39 	.globl _B_5
                             40 	.globl _B_4
                             41 	.globl _B_3
                             42 	.globl _B_2
                             43 	.globl _B_1
                             44 	.globl _B_0
                             45 	.globl _I2CI
                             46 	.globl _I2CTX
                             47 	.globl _I2CRS
                             48 	.globl _I2CM
                             49 	.globl _I2CID0
                             50 	.globl _I2CID1
                             51 	.globl _I2CGC
                             52 	.globl _I2CSI
                             53 	.globl _MDI
                             54 	.globl _MCO
                             55 	.globl _MDE
                             56 	.globl _MDO
                             57 	.globl _ACC_7
                             58 	.globl _ACC_6
                             59 	.globl _ACC_5
                             60 	.globl _ACC_4
                             61 	.globl _ACC_3
                             62 	.globl _ACC_2
                             63 	.globl _ACC_1
                             64 	.globl _ACC_0
                             65 	.globl _P
                             66 	.globl _F1
                             67 	.globl _OV
                             68 	.globl _RS0
                             69 	.globl _RS1
                             70 	.globl _F0
                             71 	.globl _AC
                             72 	.globl _CY
                             73 	.globl _CAP2
                             74 	.globl _CNT2
                             75 	.globl _TR2
                             76 	.globl _EXEN2
                             77 	.globl _TCLK
                             78 	.globl _RCLK
                             79 	.globl _EXF2
                             80 	.globl _TF2
                             81 	.globl _WDWR
                             82 	.globl _WDE
                             83 	.globl _WDS
                             84 	.globl _WDIR
                             85 	.globl _PRE0
                             86 	.globl _PRE1
                             87 	.globl _PRE2
                             88 	.globl _PRE3
                             89 	.globl _PX0
                             90 	.globl _PT0
                             91 	.globl _PX1
                             92 	.globl _PT1
                             93 	.globl _PS
                             94 	.globl _PT2
                             95 	.globl _PADC
                             96 	.globl _PSI
                             97 	.globl _EX0
                             98 	.globl _ET0
                             99 	.globl _EX1
                            100 	.globl _ET1
                            101 	.globl _ES
                            102 	.globl _ET2
                            103 	.globl _EADC
                            104 	.globl _EA
                            105 	.globl _RI
                            106 	.globl _TI
                            107 	.globl _RB8
                            108 	.globl _TB8
                            109 	.globl _REN
                            110 	.globl _SM2
                            111 	.globl _SM1
                            112 	.globl _SM0
                            113 	.globl _RD
                            114 	.globl _WR
                            115 	.globl _T1
                            116 	.globl _T0
                            117 	.globl _INT1
                            118 	.globl _INT0
                            119 	.globl _TXD
                            120 	.globl _RXD
                            121 	.globl _P3_7
                            122 	.globl _P3_6
                            123 	.globl _P3_5
                            124 	.globl _P3_4
                            125 	.globl _P3_3
                            126 	.globl _P3_2
                            127 	.globl _P3_1
                            128 	.globl _P3_0
                            129 	.globl _P2_7
                            130 	.globl _P2_6
                            131 	.globl _P2_5
                            132 	.globl _P2_4
                            133 	.globl _P2_3
                            134 	.globl _P2_2
                            135 	.globl _P2_1
                            136 	.globl _P2_0
                            137 	.globl _T2
                            138 	.globl _T2EX
                            139 	.globl _P1_7
                            140 	.globl _P1_6
                            141 	.globl _P1_5
                            142 	.globl _P1_4
                            143 	.globl _P1_3
                            144 	.globl _P1_2
                            145 	.globl _P1_1
                            146 	.globl _P1_0
                            147 	.globl _P0_7
                            148 	.globl _P0_6
                            149 	.globl _P0_5
                            150 	.globl _P0_4
                            151 	.globl _P0_3
                            152 	.globl _P0_2
                            153 	.globl _P0_1
                            154 	.globl _P0_0
                            155 	.globl _IT0
                            156 	.globl _IE0
                            157 	.globl _IT1
                            158 	.globl _IE1
                            159 	.globl _TR0
                            160 	.globl _TF0
                            161 	.globl _TR1
                            162 	.globl _TF1
                            163 	.globl _DACCON
                            164 	.globl _DAC1H
                            165 	.globl _DAC1L
                            166 	.globl _DAC0H
                            167 	.globl _DAC0L
                            168 	.globl _SPICON
                            169 	.globl _SPIDAT
                            170 	.globl _ADCGAINH
                            171 	.globl _ADCGAINL
                            172 	.globl _ADCOFSH
                            173 	.globl _ADCOFSL
                            174 	.globl _ADCDATAH
                            175 	.globl _ADCDATAL
                            176 	.globl _ADCCON3
                            177 	.globl _ADCCON2
                            178 	.globl _ADCCON1
                            179 	.globl _B
                            180 	.globl _I2CCON
                            181 	.globl _ACC
                            182 	.globl _PSMCON
                            183 	.globl _PLLCON
                            184 	.globl _DMAP
                            185 	.globl _DMAH
                            186 	.globl _DMAL
                            187 	.globl _PSW
                            188 	.globl _TH2
                            189 	.globl _TL2
                            190 	.globl _RCAP2H
                            191 	.globl _RCAP2L
                            192 	.globl _T2CON
                            193 	.globl _CHIPID
                            194 	.globl _WDCON
                            195 	.globl _EADRH
                            196 	.globl _EADRL
                            197 	.globl _EDATA4
                            198 	.globl _EDATA3
                            199 	.globl _EDATA2
                            200 	.globl _EDATA1
                            201 	.globl _ECON
                            202 	.globl _IP
                            203 	.globl _PWM1H
                            204 	.globl _PWM1L
                            205 	.globl _PWM0H
                            206 	.globl _PWM0L
                            207 	.globl _PWMCON
                            208 	.globl _IEIP2
                            209 	.globl _IE
                            210 	.globl _INTVAL
                            211 	.globl _HOUR
                            212 	.globl _MIN
                            213 	.globl _SEC
                            214 	.globl _HTHSEC
                            215 	.globl _TIMECON
                            216 	.globl _T3CON
                            217 	.globl _T3FD
                            218 	.globl _SBUF
                            219 	.globl _SCON
                            220 	.globl _I2CDAT
                            221 	.globl _I2CADD3
                            222 	.globl _I2CADD2
                            223 	.globl _I2CADD1
                            224 	.globl _I2CADD
                            225 	.globl _P3
                            226 	.globl _P2
                            227 	.globl _P1
                            228 	.globl _P0
                            229 	.globl _TH1
                            230 	.globl _TH0
                            231 	.globl _TL1
                            232 	.globl _TL0
                            233 	.globl _TMOD
                            234 	.globl _TCON
                            235 	.globl _PCON
                            236 	.globl _DPCON
                            237 	.globl _DPP
                            238 	.globl _DPH
                            239 	.globl _DPL
                            240 	.globl _SPH
                            241 	.globl _SP
                            242 	.globl _CFG842
                            243 	.globl _CFG841
                            244 	.globl _update_crc_16_PARM_2
                            245 	.globl _putchar
                            246 	.globl _update_crc_16
                            247 	.globl _Obl_przeplywM
                            248 	.globl _Obl_czestotliwosc
                            249 	.globl _Obl_przeplywO
                            250 	.globl _Obl_gestosc
                            251 ;--------------------------------------------------------
                            252 ; special function registers
                            253 ;--------------------------------------------------------
                            254 	.area RSEG    (ABS,DATA)
   0000                     255 	.org 0x0000
                    00AF    256 G$CFG841$0$0 == 0x00af
                    00AF    257 _CFG841	=	0x00af
                    00AF    258 G$CFG842$0$0 == 0x00af
                    00AF    259 _CFG842	=	0x00af
                    0081    260 G$SP$0$0 == 0x0081
                    0081    261 _SP	=	0x0081
                    00B7    262 G$SPH$0$0 == 0x00b7
                    00B7    263 _SPH	=	0x00b7
                    0082    264 G$DPL$0$0 == 0x0082
                    0082    265 _DPL	=	0x0082
                    0083    266 G$DPH$0$0 == 0x0083
                    0083    267 _DPH	=	0x0083
                    0084    268 G$DPP$0$0 == 0x0084
                    0084    269 _DPP	=	0x0084
                    00A7    270 G$DPCON$0$0 == 0x00a7
                    00A7    271 _DPCON	=	0x00a7
                    0087    272 G$PCON$0$0 == 0x0087
                    0087    273 _PCON	=	0x0087
                    0088    274 G$TCON$0$0 == 0x0088
                    0088    275 _TCON	=	0x0088
                    0089    276 G$TMOD$0$0 == 0x0089
                    0089    277 _TMOD	=	0x0089
                    008A    278 G$TL0$0$0 == 0x008a
                    008A    279 _TL0	=	0x008a
                    008B    280 G$TL1$0$0 == 0x008b
                    008B    281 _TL1	=	0x008b
                    008C    282 G$TH0$0$0 == 0x008c
                    008C    283 _TH0	=	0x008c
                    008D    284 G$TH1$0$0 == 0x008d
                    008D    285 _TH1	=	0x008d
                    0080    286 G$P0$0$0 == 0x0080
                    0080    287 _P0	=	0x0080
                    0090    288 G$P1$0$0 == 0x0090
                    0090    289 _P1	=	0x0090
                    00A0    290 G$P2$0$0 == 0x00a0
                    00A0    291 _P2	=	0x00a0
                    00B0    292 G$P3$0$0 == 0x00b0
                    00B0    293 _P3	=	0x00b0
                    009B    294 G$I2CADD$0$0 == 0x009b
                    009B    295 _I2CADD	=	0x009b
                    0091    296 G$I2CADD1$0$0 == 0x0091
                    0091    297 _I2CADD1	=	0x0091
                    0092    298 G$I2CADD2$0$0 == 0x0092
                    0092    299 _I2CADD2	=	0x0092
                    0093    300 G$I2CADD3$0$0 == 0x0093
                    0093    301 _I2CADD3	=	0x0093
                    009A    302 G$I2CDAT$0$0 == 0x009a
                    009A    303 _I2CDAT	=	0x009a
                    0098    304 G$SCON$0$0 == 0x0098
                    0098    305 _SCON	=	0x0098
                    0099    306 G$SBUF$0$0 == 0x0099
                    0099    307 _SBUF	=	0x0099
                    009D    308 G$T3FD$0$0 == 0x009d
                    009D    309 _T3FD	=	0x009d
                    009E    310 G$T3CON$0$0 == 0x009e
                    009E    311 _T3CON	=	0x009e
                    00A1    312 G$TIMECON$0$0 == 0x00a1
                    00A1    313 _TIMECON	=	0x00a1
                    00A2    314 G$HTHSEC$0$0 == 0x00a2
                    00A2    315 _HTHSEC	=	0x00a2
                    00A3    316 G$SEC$0$0 == 0x00a3
                    00A3    317 _SEC	=	0x00a3
                    00A4    318 G$MIN$0$0 == 0x00a4
                    00A4    319 _MIN	=	0x00a4
                    00A5    320 G$HOUR$0$0 == 0x00a5
                    00A5    321 _HOUR	=	0x00a5
                    00A6    322 G$INTVAL$0$0 == 0x00a6
                    00A6    323 _INTVAL	=	0x00a6
                    00A8    324 G$IE$0$0 == 0x00a8
                    00A8    325 _IE	=	0x00a8
                    00A9    326 G$IEIP2$0$0 == 0x00a9
                    00A9    327 _IEIP2	=	0x00a9
                    00AE    328 G$PWMCON$0$0 == 0x00ae
                    00AE    329 _PWMCON	=	0x00ae
                    00B1    330 G$PWM0L$0$0 == 0x00b1
                    00B1    331 _PWM0L	=	0x00b1
                    00B2    332 G$PWM0H$0$0 == 0x00b2
                    00B2    333 _PWM0H	=	0x00b2
                    00B3    334 G$PWM1L$0$0 == 0x00b3
                    00B3    335 _PWM1L	=	0x00b3
                    00B4    336 G$PWM1H$0$0 == 0x00b4
                    00B4    337 _PWM1H	=	0x00b4
                    00B8    338 G$IP$0$0 == 0x00b8
                    00B8    339 _IP	=	0x00b8
                    00B9    340 G$ECON$0$0 == 0x00b9
                    00B9    341 _ECON	=	0x00b9
                    00BC    342 G$EDATA1$0$0 == 0x00bc
                    00BC    343 _EDATA1	=	0x00bc
                    00BD    344 G$EDATA2$0$0 == 0x00bd
                    00BD    345 _EDATA2	=	0x00bd
                    00BE    346 G$EDATA3$0$0 == 0x00be
                    00BE    347 _EDATA3	=	0x00be
                    00BF    348 G$EDATA4$0$0 == 0x00bf
                    00BF    349 _EDATA4	=	0x00bf
                    00C6    350 G$EADRL$0$0 == 0x00c6
                    00C6    351 _EADRL	=	0x00c6
                    00C7    352 G$EADRH$0$0 == 0x00c7
                    00C7    353 _EADRH	=	0x00c7
                    00C0    354 G$WDCON$0$0 == 0x00c0
                    00C0    355 _WDCON	=	0x00c0
                    00C2    356 G$CHIPID$0$0 == 0x00c2
                    00C2    357 _CHIPID	=	0x00c2
                    00C8    358 G$T2CON$0$0 == 0x00c8
                    00C8    359 _T2CON	=	0x00c8
                    00CA    360 G$RCAP2L$0$0 == 0x00ca
                    00CA    361 _RCAP2L	=	0x00ca
                    00CB    362 G$RCAP2H$0$0 == 0x00cb
                    00CB    363 _RCAP2H	=	0x00cb
                    00CC    364 G$TL2$0$0 == 0x00cc
                    00CC    365 _TL2	=	0x00cc
                    00CD    366 G$TH2$0$0 == 0x00cd
                    00CD    367 _TH2	=	0x00cd
                    00D0    368 G$PSW$0$0 == 0x00d0
                    00D0    369 _PSW	=	0x00d0
                    00D2    370 G$DMAL$0$0 == 0x00d2
                    00D2    371 _DMAL	=	0x00d2
                    00D3    372 G$DMAH$0$0 == 0x00d3
                    00D3    373 _DMAH	=	0x00d3
                    00D4    374 G$DMAP$0$0 == 0x00d4
                    00D4    375 _DMAP	=	0x00d4
                    00D7    376 G$PLLCON$0$0 == 0x00d7
                    00D7    377 _PLLCON	=	0x00d7
                    00DF    378 G$PSMCON$0$0 == 0x00df
                    00DF    379 _PSMCON	=	0x00df
                    00E0    380 G$ACC$0$0 == 0x00e0
                    00E0    381 _ACC	=	0x00e0
                    00E8    382 G$I2CCON$0$0 == 0x00e8
                    00E8    383 _I2CCON	=	0x00e8
                    00F0    384 G$B$0$0 == 0x00f0
                    00F0    385 _B	=	0x00f0
                    00EF    386 G$ADCCON1$0$0 == 0x00ef
                    00EF    387 _ADCCON1	=	0x00ef
                    00D8    388 G$ADCCON2$0$0 == 0x00d8
                    00D8    389 _ADCCON2	=	0x00d8
                    00F5    390 G$ADCCON3$0$0 == 0x00f5
                    00F5    391 _ADCCON3	=	0x00f5
                    00D9    392 G$ADCDATAL$0$0 == 0x00d9
                    00D9    393 _ADCDATAL	=	0x00d9
                    00DA    394 G$ADCDATAH$0$0 == 0x00da
                    00DA    395 _ADCDATAH	=	0x00da
                    00F1    396 G$ADCOFSL$0$0 == 0x00f1
                    00F1    397 _ADCOFSL	=	0x00f1
                    00F2    398 G$ADCOFSH$0$0 == 0x00f2
                    00F2    399 _ADCOFSH	=	0x00f2
                    00F3    400 G$ADCGAINL$0$0 == 0x00f3
                    00F3    401 _ADCGAINL	=	0x00f3
                    00F4    402 G$ADCGAINH$0$0 == 0x00f4
                    00F4    403 _ADCGAINH	=	0x00f4
                    00F7    404 G$SPIDAT$0$0 == 0x00f7
                    00F7    405 _SPIDAT	=	0x00f7
                    00F8    406 G$SPICON$0$0 == 0x00f8
                    00F8    407 _SPICON	=	0x00f8
                    00F9    408 G$DAC0L$0$0 == 0x00f9
                    00F9    409 _DAC0L	=	0x00f9
                    00FA    410 G$DAC0H$0$0 == 0x00fa
                    00FA    411 _DAC0H	=	0x00fa
                    00FB    412 G$DAC1L$0$0 == 0x00fb
                    00FB    413 _DAC1L	=	0x00fb
                    00FC    414 G$DAC1H$0$0 == 0x00fc
                    00FC    415 _DAC1H	=	0x00fc
                    00FD    416 G$DACCON$0$0 == 0x00fd
                    00FD    417 _DACCON	=	0x00fd
                            418 ;--------------------------------------------------------
                            419 ; special function bits
                            420 ;--------------------------------------------------------
                            421 	.area RSEG    (ABS,DATA)
   0000                     422 	.org 0x0000
                    008F    423 G$TF1$0$0 == 0x008f
                    008F    424 _TF1	=	0x008f
                    008E    425 G$TR1$0$0 == 0x008e
                    008E    426 _TR1	=	0x008e
                    008D    427 G$TF0$0$0 == 0x008d
                    008D    428 _TF0	=	0x008d
                    008C    429 G$TR0$0$0 == 0x008c
                    008C    430 _TR0	=	0x008c
                    008B    431 G$IE1$0$0 == 0x008b
                    008B    432 _IE1	=	0x008b
                    008A    433 G$IT1$0$0 == 0x008a
                    008A    434 _IT1	=	0x008a
                    0089    435 G$IE0$0$0 == 0x0089
                    0089    436 _IE0	=	0x0089
                    0088    437 G$IT0$0$0 == 0x0088
                    0088    438 _IT0	=	0x0088
                    0080    439 G$P0_0$0$0 == 0x0080
                    0080    440 _P0_0	=	0x0080
                    0081    441 G$P0_1$0$0 == 0x0081
                    0081    442 _P0_1	=	0x0081
                    0082    443 G$P0_2$0$0 == 0x0082
                    0082    444 _P0_2	=	0x0082
                    0083    445 G$P0_3$0$0 == 0x0083
                    0083    446 _P0_3	=	0x0083
                    0084    447 G$P0_4$0$0 == 0x0084
                    0084    448 _P0_4	=	0x0084
                    0085    449 G$P0_5$0$0 == 0x0085
                    0085    450 _P0_5	=	0x0085
                    0086    451 G$P0_6$0$0 == 0x0086
                    0086    452 _P0_6	=	0x0086
                    0087    453 G$P0_7$0$0 == 0x0087
                    0087    454 _P0_7	=	0x0087
                    0090    455 G$P1_0$0$0 == 0x0090
                    0090    456 _P1_0	=	0x0090
                    0091    457 G$P1_1$0$0 == 0x0091
                    0091    458 _P1_1	=	0x0091
                    0092    459 G$P1_2$0$0 == 0x0092
                    0092    460 _P1_2	=	0x0092
                    0093    461 G$P1_3$0$0 == 0x0093
                    0093    462 _P1_3	=	0x0093
                    0094    463 G$P1_4$0$0 == 0x0094
                    0094    464 _P1_4	=	0x0094
                    0095    465 G$P1_5$0$0 == 0x0095
                    0095    466 _P1_5	=	0x0095
                    0096    467 G$P1_6$0$0 == 0x0096
                    0096    468 _P1_6	=	0x0096
                    0097    469 G$P1_7$0$0 == 0x0097
                    0097    470 _P1_7	=	0x0097
                    0091    471 G$T2EX$0$0 == 0x0091
                    0091    472 _T2EX	=	0x0091
                    0090    473 G$T2$0$0 == 0x0090
                    0090    474 _T2	=	0x0090
                    00A0    475 G$P2_0$0$0 == 0x00a0
                    00A0    476 _P2_0	=	0x00a0
                    00A1    477 G$P2_1$0$0 == 0x00a1
                    00A1    478 _P2_1	=	0x00a1
                    00A2    479 G$P2_2$0$0 == 0x00a2
                    00A2    480 _P2_2	=	0x00a2
                    00A3    481 G$P2_3$0$0 == 0x00a3
                    00A3    482 _P2_3	=	0x00a3
                    00A4    483 G$P2_4$0$0 == 0x00a4
                    00A4    484 _P2_4	=	0x00a4
                    00A5    485 G$P2_5$0$0 == 0x00a5
                    00A5    486 _P2_5	=	0x00a5
                    00A6    487 G$P2_6$0$0 == 0x00a6
                    00A6    488 _P2_6	=	0x00a6
                    00A7    489 G$P2_7$0$0 == 0x00a7
                    00A7    490 _P2_7	=	0x00a7
                    00B0    491 G$P3_0$0$0 == 0x00b0
                    00B0    492 _P3_0	=	0x00b0
                    00B1    493 G$P3_1$0$0 == 0x00b1
                    00B1    494 _P3_1	=	0x00b1
                    00B2    495 G$P3_2$0$0 == 0x00b2
                    00B2    496 _P3_2	=	0x00b2
                    00B3    497 G$P3_3$0$0 == 0x00b3
                    00B3    498 _P3_3	=	0x00b3
                    00B4    499 G$P3_4$0$0 == 0x00b4
                    00B4    500 _P3_4	=	0x00b4
                    00B5    501 G$P3_5$0$0 == 0x00b5
                    00B5    502 _P3_5	=	0x00b5
                    00B6    503 G$P3_6$0$0 == 0x00b6
                    00B6    504 _P3_6	=	0x00b6
                    00B7    505 G$P3_7$0$0 == 0x00b7
                    00B7    506 _P3_7	=	0x00b7
                    00B0    507 G$RXD$0$0 == 0x00b0
                    00B0    508 _RXD	=	0x00b0
                    00B1    509 G$TXD$0$0 == 0x00b1
                    00B1    510 _TXD	=	0x00b1
                    00B2    511 G$INT0$0$0 == 0x00b2
                    00B2    512 _INT0	=	0x00b2
                    00B3    513 G$INT1$0$0 == 0x00b3
                    00B3    514 _INT1	=	0x00b3
                    00B4    515 G$T0$0$0 == 0x00b4
                    00B4    516 _T0	=	0x00b4
                    00B5    517 G$T1$0$0 == 0x00b5
                    00B5    518 _T1	=	0x00b5
                    00B6    519 G$WR$0$0 == 0x00b6
                    00B6    520 _WR	=	0x00b6
                    00B7    521 G$RD$0$0 == 0x00b7
                    00B7    522 _RD	=	0x00b7
                    009F    523 G$SM0$0$0 == 0x009f
                    009F    524 _SM0	=	0x009f
                    009E    525 G$SM1$0$0 == 0x009e
                    009E    526 _SM1	=	0x009e
                    009D    527 G$SM2$0$0 == 0x009d
                    009D    528 _SM2	=	0x009d
                    009C    529 G$REN$0$0 == 0x009c
                    009C    530 _REN	=	0x009c
                    009B    531 G$TB8$0$0 == 0x009b
                    009B    532 _TB8	=	0x009b
                    009A    533 G$RB8$0$0 == 0x009a
                    009A    534 _RB8	=	0x009a
                    0099    535 G$TI$0$0 == 0x0099
                    0099    536 _TI	=	0x0099
                    0098    537 G$RI$0$0 == 0x0098
                    0098    538 _RI	=	0x0098
                    00AF    539 G$EA$0$0 == 0x00af
                    00AF    540 _EA	=	0x00af
                    00AE    541 G$EADC$0$0 == 0x00ae
                    00AE    542 _EADC	=	0x00ae
                    00AD    543 G$ET2$0$0 == 0x00ad
                    00AD    544 _ET2	=	0x00ad
                    00AC    545 G$ES$0$0 == 0x00ac
                    00AC    546 _ES	=	0x00ac
                    00AB    547 G$ET1$0$0 == 0x00ab
                    00AB    548 _ET1	=	0x00ab
                    00AA    549 G$EX1$0$0 == 0x00aa
                    00AA    550 _EX1	=	0x00aa
                    00A9    551 G$ET0$0$0 == 0x00a9
                    00A9    552 _ET0	=	0x00a9
                    00A8    553 G$EX0$0$0 == 0x00a8
                    00A8    554 _EX0	=	0x00a8
                    00BF    555 G$PSI$0$0 == 0x00bf
                    00BF    556 _PSI	=	0x00bf
                    00BE    557 G$PADC$0$0 == 0x00be
                    00BE    558 _PADC	=	0x00be
                    00BD    559 G$PT2$0$0 == 0x00bd
                    00BD    560 _PT2	=	0x00bd
                    00BC    561 G$PS$0$0 == 0x00bc
                    00BC    562 _PS	=	0x00bc
                    00BB    563 G$PT1$0$0 == 0x00bb
                    00BB    564 _PT1	=	0x00bb
                    00BA    565 G$PX1$0$0 == 0x00ba
                    00BA    566 _PX1	=	0x00ba
                    00B9    567 G$PT0$0$0 == 0x00b9
                    00B9    568 _PT0	=	0x00b9
                    00B8    569 G$PX0$0$0 == 0x00b8
                    00B8    570 _PX0	=	0x00b8
                    00C7    571 G$PRE3$0$0 == 0x00c7
                    00C7    572 _PRE3	=	0x00c7
                    00C6    573 G$PRE2$0$0 == 0x00c6
                    00C6    574 _PRE2	=	0x00c6
                    00C5    575 G$PRE1$0$0 == 0x00c5
                    00C5    576 _PRE1	=	0x00c5
                    00C4    577 G$PRE0$0$0 == 0x00c4
                    00C4    578 _PRE0	=	0x00c4
                    00C3    579 G$WDIR$0$0 == 0x00c3
                    00C3    580 _WDIR	=	0x00c3
                    00C2    581 G$WDS$0$0 == 0x00c2
                    00C2    582 _WDS	=	0x00c2
                    00C1    583 G$WDE$0$0 == 0x00c1
                    00C1    584 _WDE	=	0x00c1
                    00C0    585 G$WDWR$0$0 == 0x00c0
                    00C0    586 _WDWR	=	0x00c0
                    00CF    587 G$TF2$0$0 == 0x00cf
                    00CF    588 _TF2	=	0x00cf
                    00CE    589 G$EXF2$0$0 == 0x00ce
                    00CE    590 _EXF2	=	0x00ce
                    00CD    591 G$RCLK$0$0 == 0x00cd
                    00CD    592 _RCLK	=	0x00cd
                    00CC    593 G$TCLK$0$0 == 0x00cc
                    00CC    594 _TCLK	=	0x00cc
                    00CB    595 G$EXEN2$0$0 == 0x00cb
                    00CB    596 _EXEN2	=	0x00cb
                    00CA    597 G$TR2$0$0 == 0x00ca
                    00CA    598 _TR2	=	0x00ca
                    00C9    599 G$CNT2$0$0 == 0x00c9
                    00C9    600 _CNT2	=	0x00c9
                    00C8    601 G$CAP2$0$0 == 0x00c8
                    00C8    602 _CAP2	=	0x00c8
                    00D7    603 G$CY$0$0 == 0x00d7
                    00D7    604 _CY	=	0x00d7
                    00D6    605 G$AC$0$0 == 0x00d6
                    00D6    606 _AC	=	0x00d6
                    00D5    607 G$F0$0$0 == 0x00d5
                    00D5    608 _F0	=	0x00d5
                    00D4    609 G$RS1$0$0 == 0x00d4
                    00D4    610 _RS1	=	0x00d4
                    00D3    611 G$RS0$0$0 == 0x00d3
                    00D3    612 _RS0	=	0x00d3
                    00D2    613 G$OV$0$0 == 0x00d2
                    00D2    614 _OV	=	0x00d2
                    00D1    615 G$F1$0$0 == 0x00d1
                    00D1    616 _F1	=	0x00d1
                    00D0    617 G$P$0$0 == 0x00d0
                    00D0    618 _P	=	0x00d0
                    00E0    619 G$ACC_0$0$0 == 0x00e0
                    00E0    620 _ACC_0	=	0x00e0
                    00E1    621 G$ACC_1$0$0 == 0x00e1
                    00E1    622 _ACC_1	=	0x00e1
                    00E2    623 G$ACC_2$0$0 == 0x00e2
                    00E2    624 _ACC_2	=	0x00e2
                    00E3    625 G$ACC_3$0$0 == 0x00e3
                    00E3    626 _ACC_3	=	0x00e3
                    00E4    627 G$ACC_4$0$0 == 0x00e4
                    00E4    628 _ACC_4	=	0x00e4
                    00E5    629 G$ACC_5$0$0 == 0x00e5
                    00E5    630 _ACC_5	=	0x00e5
                    00E6    631 G$ACC_6$0$0 == 0x00e6
                    00E6    632 _ACC_6	=	0x00e6
                    00E7    633 G$ACC_7$0$0 == 0x00e7
                    00E7    634 _ACC_7	=	0x00e7
                    00EF    635 G$MDO$0$0 == 0x00ef
                    00EF    636 _MDO	=	0x00ef
                    00EE    637 G$MDE$0$0 == 0x00ee
                    00EE    638 _MDE	=	0x00ee
                    00ED    639 G$MCO$0$0 == 0x00ed
                    00ED    640 _MCO	=	0x00ed
                    00EC    641 G$MDI$0$0 == 0x00ec
                    00EC    642 _MDI	=	0x00ec
                    00EF    643 G$I2CSI$0$0 == 0x00ef
                    00EF    644 _I2CSI	=	0x00ef
                    00EE    645 G$I2CGC$0$0 == 0x00ee
                    00EE    646 _I2CGC	=	0x00ee
                    00ED    647 G$I2CID1$0$0 == 0x00ed
                    00ED    648 _I2CID1	=	0x00ed
                    00EC    649 G$I2CID0$0$0 == 0x00ec
                    00EC    650 _I2CID0	=	0x00ec
                    00EB    651 G$I2CM$0$0 == 0x00eb
                    00EB    652 _I2CM	=	0x00eb
                    00EA    653 G$I2CRS$0$0 == 0x00ea
                    00EA    654 _I2CRS	=	0x00ea
                    00E9    655 G$I2CTX$0$0 == 0x00e9
                    00E9    656 _I2CTX	=	0x00e9
                    00E8    657 G$I2CI$0$0 == 0x00e8
                    00E8    658 _I2CI	=	0x00e8
                    00F0    659 G$B_0$0$0 == 0x00f0
                    00F0    660 _B_0	=	0x00f0
                    00F1    661 G$B_1$0$0 == 0x00f1
                    00F1    662 _B_1	=	0x00f1
                    00F2    663 G$B_2$0$0 == 0x00f2
                    00F2    664 _B_2	=	0x00f2
                    00F3    665 G$B_3$0$0 == 0x00f3
                    00F3    666 _B_3	=	0x00f3
                    00F4    667 G$B_4$0$0 == 0x00f4
                    00F4    668 _B_4	=	0x00f4
                    00F5    669 G$B_5$0$0 == 0x00f5
                    00F5    670 _B_5	=	0x00f5
                    00F6    671 G$B_6$0$0 == 0x00f6
                    00F6    672 _B_6	=	0x00f6
                    00F7    673 G$B_7$0$0 == 0x00f7
                    00F7    674 _B_7	=	0x00f7
                    00DF    675 G$ADCI$0$0 == 0x00df
                    00DF    676 _ADCI	=	0x00df
                    00DE    677 G$DMA$0$0 == 0x00de
                    00DE    678 _DMA	=	0x00de
                    00DD    679 G$CCONV$0$0 == 0x00dd
                    00DD    680 _CCONV	=	0x00dd
                    00DC    681 G$SCONV$0$0 == 0x00dc
                    00DC    682 _SCONV	=	0x00dc
                    00DB    683 G$CS3$0$0 == 0x00db
                    00DB    684 _CS3	=	0x00db
                    00DA    685 G$CS2$0$0 == 0x00da
                    00DA    686 _CS2	=	0x00da
                    00D9    687 G$CS1$0$0 == 0x00d9
                    00D9    688 _CS1	=	0x00d9
                    00D8    689 G$CS0$0$0 == 0x00d8
                    00D8    690 _CS0	=	0x00d8
                    00FF    691 G$ISPI$0$0 == 0x00ff
                    00FF    692 _ISPI	=	0x00ff
                    00FE    693 G$WCOL$0$0 == 0x00fe
                    00FE    694 _WCOL	=	0x00fe
                    00FD    695 G$SPE$0$0 == 0x00fd
                    00FD    696 _SPE	=	0x00fd
                    00FC    697 G$SPIM$0$0 == 0x00fc
                    00FC    698 _SPIM	=	0x00fc
                    00FB    699 G$CPOL$0$0 == 0x00fb
                    00FB    700 _CPOL	=	0x00fb
                    00FA    701 G$CPHA$0$0 == 0x00fa
                    00FA    702 _CPHA	=	0x00fa
                    00F9    703 G$SPR1$0$0 == 0x00f9
                    00F9    704 _SPR1	=	0x00f9
                    00F8    705 G$SPR0$0$0 == 0x00f8
                    00F8    706 _SPR0	=	0x00f8
                            707 ;--------------------------------------------------------
                            708 ; overlayable register banks
                            709 ;--------------------------------------------------------
                            710 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     711 	.ds 8
                            712 ;--------------------------------------------------------
                            713 ; internal ram data
                            714 ;--------------------------------------------------------
                            715 	.area DSEG    (DATA)
                    0000    716 Lmain.main$sloc0$1$0==.
   0008                     717 _main_sloc0_1_0:
   0008                     718 	.ds 4
                    0004    719 Lmain.Obl_przeplywM$sloc0$1$0==.
   000C                     720 _Obl_przeplywM_sloc0_1_0:
   000C                     721 	.ds 4
                            722 ;--------------------------------------------------------
                            723 ; overlayable items in internal ram 
                            724 ;--------------------------------------------------------
                            725 ;--------------------------------------------------------
                            726 ; Stack segment in internal ram 
                            727 ;--------------------------------------------------------
                            728 	.area	SSEG	(DATA)
   0027                     729 __start__stack:
   0027                     730 	.ds	1
                            731 
                            732 ;--------------------------------------------------------
                            733 ; indirectly addressable internal ram data
                            734 ;--------------------------------------------------------
                            735 	.area ISEG    (DATA)
                            736 ;--------------------------------------------------------
                            737 ; absolute internal ram data
                            738 ;--------------------------------------------------------
                            739 	.area IABS    (ABS,DATA)
                            740 	.area IABS    (ABS,DATA)
                            741 ;--------------------------------------------------------
                            742 ; bit data
                            743 ;--------------------------------------------------------
                            744 	.area BSEG    (BIT)
                    0000    745 Fmain$NoweWyniki$0$0==.
   0004                     746 _NoweWyniki:
   0004                     747 	.ds 1
                    0001    748 Fmain$modbus$0$0==.
   0005                     749 _modbus:
   0005                     750 	.ds 1
                            751 ;--------------------------------------------------------
                            752 ; paged external ram data
                            753 ;--------------------------------------------------------
                            754 	.area PSEG    (PAG,XDATA)
                    0000    755 Fmain$buffer$0$0==.
   0001                     756 _buffer:
   0001                     757 	.ds 20
                    0014    758 Fmain$wiadomosc$0$0==.
   0015                     759 _wiadomosc:
   0015                     760 	.ds 12
                    0020    761 Fmain$str$0$0==.
   0021                     762 _str:
   0021                     763 	.ds 10
                    002A    764 Fmain$WynikADC$0$0==.
   002B                     765 _WynikADC:
   002B                     766 	.ds 2
                    002C    767 Fmain$CzasT2$0$0==.
   002D                     768 _CzasT2:
   002D                     769 	.ds 2
                    002E    770 Fmain$WynikOkres$0$0==.
   002F                     771 _WynikOkres:
   002F                     772 	.ds 2
                    0030    773 Fmain$WynikFaza$0$0==.
   0031                     774 _WynikFaza:
   0031                     775 	.ds 2
                    0032    776 Fmain$n$0$0==.
   0033                     777 _n:
   0033                     778 	.ds 2
                    0034    779 Fmain$sr_przeplM$0$0==.
   0035                     780 _sr_przeplM:
   0035                     781 	.ds 4
                    0038    782 Fmain$sr_przeplO$0$0==.
   0039                     783 _sr_przeplO:
   0039                     784 	.ds 4
                    003C    785 Fmain$calk_przeplM$0$0==.
   003D                     786 _calk_przeplM:
   003D                     787 	.ds 4
                    0040    788 Fmain$calk_przeplO$0$0==.
   0041                     789 _calk_przeplO:
   0041                     790 	.ds 4
                    0044    791 Fmain$crc_modbus$0$0==.
   0045                     792 _crc_modbus:
   0045                     793 	.ds 2
                    0046    794 Fmain$zapytanie_master$0$0==.
   0047                     795 _zapytanie_master:
   0047                     796 	.ds 8
                    004E    797 Fmain$Znak$0$0==.
   004F                     798 _Znak:
   004F                     799 	.ds 1
                    004F    800 Fmain$literka$0$0==.
   0050                     801 _literka:
   0050                     802 	.ds 1
                    0050    803 Lmain.update_crc_16$c$1$72==.
   0051                     804 _update_crc_16_PARM_2:
   0051                     805 	.ds 1
                            806 ;--------------------------------------------------------
                            807 ; external ram data
                            808 ;--------------------------------------------------------
                            809 	.area XSEG    (XDATA)
                            810 ;--------------------------------------------------------
                            811 ; absolute external ram data
                            812 ;--------------------------------------------------------
                            813 	.area XABS    (ABS,XDATA)
                            814 ;--------------------------------------------------------
                            815 ; external initialized ram data
                            816 ;--------------------------------------------------------
                            817 	.area XISEG   (XDATA)
                            818 	.area HOME    (CODE)
                            819 	.area GSINIT0 (CODE)
                            820 	.area GSINIT1 (CODE)
                            821 	.area GSINIT2 (CODE)
                            822 	.area GSINIT3 (CODE)
                            823 	.area GSINIT4 (CODE)
                            824 	.area GSINIT5 (CODE)
                            825 	.area GSINIT  (CODE)
                            826 	.area GSFINAL (CODE)
                            827 	.area CSEG    (CODE)
                            828 ;--------------------------------------------------------
                            829 ; interrupt vector 
                            830 ;--------------------------------------------------------
                            831 	.area HOME    (CODE)
   0000                     832 __interrupt_vect:
   0000 02 00 0B            833 	ljmp	__sdcc_gsinit_startup
   0003 02 02 05            834 	ljmp	_IntO_int
                            835 ;--------------------------------------------------------
                            836 ; global & static initialisations
                            837 ;--------------------------------------------------------
                            838 	.area HOME    (CODE)
                            839 	.area GSINIT  (CODE)
                            840 	.area GSFINAL (CODE)
                            841 	.area GSINIT  (CODE)
                            842 	.globl __sdcc_gsinit_startup
                            843 	.globl __sdcc_program_startup
                            844 	.globl __start__stack
                            845 	.globl __mcs51_genXINIT
                            846 	.globl __mcs51_genXRAMCLEAR
                            847 	.globl __mcs51_genRAMCLEAR
                    0000    848 	C$main.c$21$1$81 ==.
                            849 ;	main.c:21: static bool NoweWyniki=0;	// Znacznik "nowe wyniki"  1 wyniki 0 czekaj
   0064 C2 04               850 	clr	_NoweWyniki
                    0002    851 	C$main.c$31$1$81 ==.
                            852 ;	main.c:31: static bool modbus=0; //modbus uruchamia modbusa
   0066 C2 05               853 	clr	_modbus
                    0004    854 	C$main.c$22$1$81 ==.
                            855 ;	main.c:22: static int n=0;             // Liczba do średniej arytmetycznej
   0068 78 33               856 	mov	r0,#_n
   006A E4                  857 	clr	a
   006B F2                  858 	movx	@r0,a
   006C 08                  859 	inc	r0
   006D F2                  860 	movx	@r0,a
                    000A    861 	C$main.c$23$1$81 ==.
                            862 ;	main.c:23: static float sr_przeplM=0;     //
   006E 78 35               863 	mov	r0,#_sr_przeplM
   0070 E4                  864 	clr	a
   0071 F2                  865 	movx	@r0,a
   0072 08                  866 	inc	r0
   0073 F2                  867 	movx	@r0,a
   0074 08                  868 	inc	r0
   0075 F2                  869 	movx	@r0,a
   0076 08                  870 	inc	r0
   0077 F2                  871 	movx	@r0,a
                    0014    872 	C$main.c$24$1$81 ==.
                            873 ;	main.c:24: static float sr_przeplO=0;     // Średni przepływ objętościowy
   0078 78 39               874 	mov	r0,#_sr_przeplO
   007A E4                  875 	clr	a
   007B F2                  876 	movx	@r0,a
   007C 08                  877 	inc	r0
   007D F2                  878 	movx	@r0,a
   007E 08                  879 	inc	r0
   007F F2                  880 	movx	@r0,a
   0080 08                  881 	inc	r0
   0081 F2                  882 	movx	@r0,a
                    001E    883 	C$main.c$25$1$81 ==.
                            884 ;	main.c:25: static float calk_przeplM=0;
   0082 78 3D               885 	mov	r0,#_calk_przeplM
   0084 E4                  886 	clr	a
   0085 F2                  887 	movx	@r0,a
   0086 08                  888 	inc	r0
   0087 F2                  889 	movx	@r0,a
   0088 08                  890 	inc	r0
   0089 F2                  891 	movx	@r0,a
   008A 08                  892 	inc	r0
   008B F2                  893 	movx	@r0,a
                    0028    894 	C$main.c$26$1$81 ==.
                            895 ;	main.c:26: static float calk_przeplO=0;   // Całkoity
   008C 78 41               896 	mov	r0,#_calk_przeplO
   008E E4                  897 	clr	a
   008F F2                  898 	movx	@r0,a
   0090 08                  899 	inc	r0
   0091 F2                  900 	movx	@r0,a
   0092 08                  901 	inc	r0
   0093 F2                  902 	movx	@r0,a
   0094 08                  903 	inc	r0
   0095 F2                  904 	movx	@r0,a
                            905 	.area GSFINAL (CODE)
   0096 02 00 06            906 	ljmp	__sdcc_program_startup
                            907 ;--------------------------------------------------------
                            908 ; Home
                            909 ;--------------------------------------------------------
                            910 	.area HOME    (CODE)
                            911 	.area HOME    (CODE)
   0006                     912 __sdcc_program_startup:
   0006 12 02 2F            913 	lcall	_main
                            914 ;	return from main will lock up
   0009 80 FE               915 	sjmp .
                            916 ;--------------------------------------------------------
                            917 ; code
                            918 ;--------------------------------------------------------
                            919 	.area CSEG    (CODE)
                            920 ;------------------------------------------------------------
                            921 ;Allocation info for local variables in function 'IntO_int'
                            922 ;------------------------------------------------------------
                    0000    923 	G$IntO_int$0$0 ==.
                    0000    924 	C$main.c$42$0$0 ==.
                            925 ;	main.c:42: void IntO_int (void) __interrupt (0) 	// przerwanie z Int0
                            926 ;	-----------------------------------------
                            927 ;	 function IntO_int
                            928 ;	-----------------------------------------
   0205                     929 _IntO_int:
                    0007    930 	ar7 = 0x07
                    0006    931 	ar6 = 0x06
                    0005    932 	ar5 = 0x05
                    0004    933 	ar4 = 0x04
                    0003    934 	ar3 = 0x03
                    0002    935 	ar2 = 0x02
                    0001    936 	ar1 = 0x01
                    0000    937 	ar0 = 0x00
   0205 C0 E0               938 	push	acc
   0207 C0 00               939 	push	ar0
   0209 C0 D0               940 	push	psw
   020B 75 D0 00            941 	mov	psw,#0x00
                    0009    942 	C$main.c$48$1$54 ==.
                            943 ;	main.c:48: __endasm;								/**Koniec kodu assemblera**/
   020E 85 8A 31            944 	mov _WynikFaza,TL0
   0211 85 8C 32            945 	mov _WynikFaza+1,TH0
                    000F    946 	C$main.c$49$1$54 ==.
                            947 ;	main.c:49: TH0 = 0;			  	// Zeruj czêœæ doln¹ licznika T0
   0214 75 8C 00            948 	mov	_TH0,#0x00
                    0012    949 	C$main.c$50$1$54 ==.
                            950 ;	main.c:50: TL0 = 0;				// Zeruj czêœæ górn¹ licznika T0
   0217 75 8A 00            951 	mov	_TL0,#0x00
                    0015    952 	C$main.c$56$1$54 ==.
                            953 ;	main.c:56: __endasm;								/**Koniec kodu assemblera**/
   021A 85 CA 2D            954 	mov _CzasT2,RCAP2L
   021D 85 CB 2E            955 	mov _CzasT2+1,RCAP2H
                    001B    956 	C$main.c$57$1$54 ==.
                            957 ;	main.c:57: WynikOkres =CzasT2-WynikOkres;  	// Oblicz okres jako przyrost czasu
   0220 78 2F               958 	mov	r0,#_WynikOkres
   0222 E4                  959 	clr	a
   0223 F2                  960 	movx	@r0,a
   0224 08                  961 	inc	r0
   0225 F2                  962 	movx	@r0,a
                    0021    963 	C$main.c$58$1$54 ==.
                            964 ;	main.c:58: NoweWyniki = 1;			// Ustaw znacznik  "nowe wyniki"
   0226 D2 04               965 	setb	_NoweWyniki
   0228 D0 D0               966 	pop	psw
   022A D0 00               967 	pop	ar0
   022C D0 E0               968 	pop	acc
                    0029    969 	C$main.c$59$1$54 ==.
                    0029    970 	XG$IntO_int$0$0 ==.
   022E 32                  971 	reti
                            972 ;	eliminated unneeded push/pop ar1
                            973 ;	eliminated unneeded push/pop dpl
                            974 ;	eliminated unneeded push/pop dph
                            975 ;	eliminated unneeded push/pop b
                            976 ;------------------------------------------------------------
                            977 ;Allocation info for local variables in function 'main'
                            978 ;------------------------------------------------------------
                            979 ;sloc0                     Allocated with name '_main_sloc0_1_0'
                            980 ;------------------------------------------------------------
                    002A    981 	G$main$0$0 ==.
                    002A    982 	C$main.c$61$1$54 ==.
                            983 ;	main.c:61: void main(void)
                            984 ;	-----------------------------------------
                            985 ;	 function main
                            986 ;	-----------------------------------------
   022F                     987 _main:
                    002A    988 	C$main.c$65$1$56 ==.
                            989 ;	main.c:65: P1=0b00000001;		// p1.0  Wejscie analogowe reszta cyfrowe ( 0 cyfrowe, 1 analogowe)
   022F 75 90 01            990 	mov	_P1,#0x01
                    002D    991 	C$main.c$67$1$56 ==.
                            992 ;	main.c:67: T3CON =0x82;			// T3BAUDEN(0x80) + DIV ( DIV=log(Fosc/16/BaudRate)/log(2)) 115200
   0232 75 9E 82            993 	mov	_T3CON,#0x82
                    0030    994 	C$main.c$68$1$56 ==.
                            995 ;	main.c:68: T3FD =40;			// T3FD (2*Fosc/2^(DIV-1)/BaudRate)-64
   0235 75 9D 28            996 	mov	_T3FD,#0x28
                    0033    997 	C$main.c$70$1$56 ==.
                            998 ;	main.c:70: SCON = SM1 | REN;
   0238 A2 9E               999 	mov	c,_SM1
   023A E4                 1000 	clr	a
   023B 33                 1001 	rlc	a
   023C FF                 1002 	mov	r7,a
   023D A2 9C              1003 	mov	c,_REN
   023F E4                 1004 	clr	a
   0240 33                 1005 	rlc	a
   0241 4F                 1006 	orl	a,r7
   0242 F5 98              1007 	mov	_SCON,a
                    003F   1008 	C$main.c$71$1$56 ==.
                           1009 ;	main.c:71: TI=1;					//Flaga transmisji portu szeregowego
   0244 D2 99              1010 	setb	_TI
                    0041   1011 	C$main.c$73$1$56 ==.
                           1012 ;	main.c:73: ADCCON1 =0xB4;		// Tryb paracy przetwornika ADC (ADC Clock 12MHz/3 )
   0246 75 EF B4           1013 	mov	_ADCCON1,#0xB4
                    0044   1014 	C$main.c$74$1$56 ==.
                           1015 ;	main.c:74: ADCCON2 =0x00;			// Ustawienie kanalu przetwornika KANAL 0
   0249 75 D8 00           1016 	mov	_ADCCON2,#0x00
                    0047   1017 	C$main.c$75$1$56 ==.
                           1018 ;	main.c:75: SCONV =0;			// Gotowosc do konwersji
   024C C2 DC              1019 	clr	_SCONV
                    0049   1020 	C$main.c$77$1$56 ==.
                           1021 ;	main.c:77: TH0 =0;			//Zeruj starsz¹ czêœæ licznika T0
   024E 75 8C 00           1022 	mov	_TH0,#0x00
                    004C   1023 	C$main.c$78$1$56 ==.
                           1024 ;	main.c:78: TL0 =0;			//Zeruj m³odsz¹ czêœæ licznika T0
   0251 75 8A 00           1025 	mov	_TL0,#0x00
                    004F   1026 	C$main.c$79$1$56 ==.
                           1027 ;	main.c:79: TMOD =0b00001001;		// T1: wyl,  T0: Gate0=1, C/T0=0 tryb0=01
   0254 75 89 09           1028 	mov	_TMOD,#0x09
                    0052   1029 	C$main.c$80$1$56 ==.
                           1030 ;	main.c:80: TR0 =1;			// T0 start zliczania
   0257 D2 8C              1031 	setb	_TR0
                    0054   1032 	C$main.c$82$1$56 ==.
                           1033 ;	main.c:82: TH2 = 0;			// Zeruj czêœæ górną licznika T2
   0259 75 CD 00           1034 	mov	_TH2,#0x00
                    0057   1035 	C$main.c$83$1$56 ==.
                           1036 ;	main.c:83: TL2 = 0;			// Zeruj czêœæ dolną licznika T2
   025C 75 CC 00           1037 	mov	_TL2,#0x00
                    005A   1038 	C$main.c$84$1$56 ==.
                           1039 ;	main.c:84: CAP2 = 1;			// T2 praca z zatraskiwaniem wartosci licznika
   025F D2 C8              1040 	setb	_CAP2
                    005C   1041 	C$main.c$85$1$56 ==.
                           1042 ;	main.c:85: CNT2 = 0;			// Czasomierz (zliczanie impulsow 12 MHz)
   0261 C2 C9              1043 	clr	_CNT2
                    005E   1044 	C$main.c$86$1$56 ==.
                           1045 ;	main.c:86: EXEN2 = 1;			// Aktywacja wejscia T2EX (P1.1)
   0263 D2 CB              1046 	setb	_EXEN2
                    0060   1047 	C$main.c$87$1$56 ==.
                           1048 ;	main.c:87: TR2 = 1;			// Sterowanie zliczaniem (1 start zliczania)
   0265 D2 CA              1049 	setb	_TR2
                    0062   1050 	C$main.c$89$1$56 ==.
                           1051 ;	main.c:89: IT0 =1;			// I0 zglaszane zboczem opadajacym
   0267 D2 88              1052 	setb	_IT0
                    0064   1053 	C$main.c$90$1$56 ==.
                           1054 ;	main.c:90: EX0 =1;			// Odblokuj przerwanie od INT0
   0269 D2 A8              1055 	setb	_EX0
                    0066   1056 	C$main.c$91$1$56 ==.
                           1057 ;	main.c:91: EA =1;			// Odblokuj niezablokowane przerwania
   026B D2 AF              1058 	setb	_EA
                    0068   1059 	C$main.c$93$1$56 ==.
                           1060 ;	main.c:93: CFG842 = EXTCLK; // Okblokowuje źródło zegara zewnętrznego 32,768kHz
   026D 75 AF 10           1061 	mov	_CFG842,#0x10
                    006B   1062 	C$main.c$94$1$56 ==.
                           1063 ;	main.c:94: TIMECON = TCEN | ITS0; // Inicjalizacja TIC, interwal 1/128s
   0270 75 A1 11           1064 	mov	_TIMECON,#0x11
                    006E   1065 	C$main.c$95$1$56 ==.
                           1066 ;	main.c:95: INTVAL=127; // Ustawia bit TII rejestru CFG842 co sekunde
   0273 75 A6 7F           1067 	mov	_INTVAL,#0x7F
                    0071   1068 	C$main.c$96$1$56 ==.
                           1069 ;	main.c:96: LCD_Initalize(); // Inicjalizacja LCD
   0276 12 01 82           1070 	lcall	_LCD_Initalize
                    0074   1071 	C$main.c$97$1$56 ==.
                           1072 ;	main.c:97: LCD_Clear();
   0279 12 01 68           1073 	lcall	_LCD_Clear
                    0077   1074 	C$main.c$99$1$56 ==.
                           1075 ;	main.c:99: while(1)			//***********************Pêtla g³ówna programu**************************
   027C                    1076 00124$:
                    0077   1077 	C$main.c$101$2$57 ==.
                           1078 ;	main.c:101: if (NoweWyniki)		// jesli nowe wyniki to wyslij
   027C 20 04 03           1079 	jb	_NoweWyniki,00192$
   027F 02 03 F3           1080 	ljmp	00111$
   0282                    1081 00192$:
                    007D   1082 	C$main.c$103$3$58 ==.
                           1083 ;	main.c:103: crc_modbus=0xffff; // CRC dla modbus musi zaczynac się od ffff
   0282 78 45              1084 	mov	r0,#_crc_modbus
   0284 74 FF              1085 	mov	a,#0xFF
   0286 F2                 1086 	movx	@r0,a
   0287 08                 1087 	inc	r0
   0288 74 FF              1088 	mov	a,#0xFF
   028A F2                 1089 	movx	@r0,a
                    0086   1090 	C$main.c$104$3$58 ==.
                           1091 ;	main.c:104: SCONV =1;     	      	// Inicjuj pomiar w kanale 0
   028B D2 DC              1092 	setb	_SCONV
                    0088   1093 	C$main.c$105$3$58 ==.
                           1094 ;	main.c:105: NoweWyniki =0;		    // Kasuj znacznik "nowe wyniki"
   028D C2 04              1095 	clr	_NoweWyniki
                    008A   1096 	C$main.c$109$3$58 ==.
                           1097 ;	main.c:109: if (modbus==1)
   028F 30 05 6A           1098 	jnb	_modbus,00102$
                    008D   1099 	C$main.c$111$5$60 ==.
                           1100 ;	main.c:111: for (i=0; i<8; i++){
   0292 7E 00              1101 	mov	r6,#0x00
   0294 7F 00              1102 	mov	r7,#0x00
   0296                    1103 00126$:
   0296 C3                 1104 	clr	c
   0297 EE                 1105 	mov	a,r6
   0298 94 08              1106 	subb	a,#0x08
   029A EF                 1107 	mov	a,r7
   029B 64 80              1108 	xrl	a,#0x80
   029D 94 80              1109 	subb	a,#0x80
   029F 50 1D              1110 	jnc	00129$
                    009C   1111 	C$main.c$112$5$60 ==.
                           1112 ;	main.c:112: zapytanie_master[i]=getchar();
   02A1 EE                 1113 	mov	a,r6
   02A2 24 47              1114 	add	a,#_zapytanie_master
   02A4 F9                 1115 	mov	r1,a
   02A5 C0 07              1116 	push	ar7
   02A7 C0 06              1117 	push	ar6
   02A9 C0 01              1118 	push	ar1
   02AB 12 06 56           1119 	lcall	_getchar
   02AE E5 82              1120 	mov	a,dpl
   02B0 D0 01              1121 	pop	ar1
   02B2 D0 06              1122 	pop	ar6
   02B4 D0 07              1123 	pop	ar7
   02B6 F3                 1124 	movx	@r1,a
                    00B2   1125 	C$main.c$111$4$59 ==.
                           1126 ;	main.c:111: for (i=0; i<8; i++){
   02B7 0E                 1127 	inc	r6
   02B8 BE 00 DB           1128 	cjne	r6,#0x00,00126$
   02BB 0F                 1129 	inc	r7
   02BC 80 D8              1130 	sjmp	00126$
   02BE                    1131 00129$:
                    00B9   1132 	C$main.c$115$1$56 ==.
                           1133 ;	main.c:115: for (i=0; i<8; i++){
   02BE 7E 00              1134 	mov	r6,#0x00
   02C0 7F 00              1135 	mov	r7,#0x00
   02C2                    1136 00130$:
   02C2 C3                 1137 	clr	c
   02C3 EE                 1138 	mov	a,r6
   02C4 94 08              1139 	subb	a,#0x08
   02C6 EF                 1140 	mov	a,r7
   02C7 64 80              1141 	xrl	a,#0x80
   02C9 94 80              1142 	subb	a,#0x80
   02CB 50 2F              1143 	jnc	00102$
                    00C8   1144 	C$main.c$116$5$61 ==.
                           1145 ;	main.c:116: crc_modbus=update_crc_16(crc_modbus,zapytanie_master[i]);
   02CD EE                 1146 	mov	a,r6
   02CE 24 47              1147 	add	a,#_zapytanie_master
   02D0 F9                 1148 	mov	r1,a
   02D1 78 51              1149 	mov	r0,#_update_crc_16_PARM_2
   02D3 E3                 1150 	movx	a,@r1
   02D4 F2                 1151 	movx	@r0,a
   02D5 78 45              1152 	mov	r0,#_crc_modbus
   02D7 E2                 1153 	movx	a,@r0
   02D8 F5 82              1154 	mov	dpl,a
   02DA 08                 1155 	inc	r0
   02DB E2                 1156 	movx	a,@r0
   02DC F5 83              1157 	mov	dph,a
   02DE C0 07              1158 	push	ar7
   02E0 C0 06              1159 	push	ar6
   02E2 12 06 5F           1160 	lcall	_update_crc_16
   02E5 E5 82              1161 	mov	a,dpl
   02E7 85 83 F0           1162 	mov	b,dph
   02EA D0 06              1163 	pop	ar6
   02EC D0 07              1164 	pop	ar7
   02EE 78 45              1165 	mov	r0,#_crc_modbus
   02F0 F2                 1166 	movx	@r0,a
   02F1 08                 1167 	inc	r0
   02F2 E5 F0              1168 	mov	a,b
   02F4 F2                 1169 	movx	@r0,a
                    00F0   1170 	C$main.c$115$4$59 ==.
                           1171 ;	main.c:115: for (i=0; i<8; i++){
   02F5 0E                 1172 	inc	r6
   02F6 BE 00 C9           1173 	cjne	r6,#0x00,00130$
   02F9 0F                 1174 	inc	r7
   02FA 80 C6              1175 	sjmp	00130$
   02FC                    1176 00102$:
                    00F7   1177 	C$main.c$120$3$58 ==.
                           1178 ;	main.c:120: if (crc_modbus==0 && (zapytanie_master[0]==0x11) && (zapytanie_master[1]==0x03)){
   02FC 78 45              1179 	mov	r0,#_crc_modbus
   02FE E2                 1180 	movx	a,@r0
   02FF F5 F0              1181 	mov	b,a
   0301 08                 1182 	inc	r0
   0302 E2                 1183 	movx	a,@r0
   0303 45 F0              1184 	orl	a,b
   0305 60 03              1185 	jz	00198$
   0307 02 03 F3           1186 	ljmp	00111$
   030A                    1187 00198$:
   030A 78 47              1188 	mov	r0,#_zapytanie_master
   030C E2                 1189 	movx	a,@r0
   030D FF                 1190 	mov	r7,a
   030E BF 11 02           1191 	cjne	r7,#0x11,00199$
   0311 80 03              1192 	sjmp	00200$
   0313                    1193 00199$:
   0313 02 03 F3           1194 	ljmp	00111$
   0316                    1195 00200$:
   0316 78 48              1196 	mov	r0,#(_zapytanie_master + 0x0001)
   0318 E2                 1197 	movx	a,@r0
   0319 FF                 1198 	mov	r7,a
   031A BF 03 02           1199 	cjne	r7,#0x03,00201$
   031D 80 03              1200 	sjmp	00202$
   031F                    1201 00201$:
   031F 02 03 F3           1202 	ljmp	00111$
   0322                    1203 00202$:
                    011D   1204 	C$main.c$122$4$62 ==.
                           1205 ;	main.c:122: crc_modbus=0xffff; // CRC dla modbus musi zaczynac się od ffff
   0322 78 45              1206 	mov	r0,#_crc_modbus
   0324 74 FF              1207 	mov	a,#0xFF
   0326 F2                 1208 	movx	@r0,a
   0327 08                 1209 	inc	r0
   0328 74 FF              1210 	mov	a,#0xFF
   032A F2                 1211 	movx	@r0,a
                    0126   1212 	C$main.c$123$4$62 ==.
                           1213 ;	main.c:123: wiadomosc[0]=0x11;		// Adres SLAVE 17
   032B 78 15              1214 	mov	r0,#_wiadomosc
   032D 74 11              1215 	mov	a,#0x11
   032F F2                 1216 	movx	@r0,a
                    012B   1217 	C$main.c$124$4$62 ==.
                           1218 ;	main.c:124: wiadomosc[1]=0x03;		// Funkcja - czystanie stanu rejestrów
   0330 78 16              1219 	mov	r0,#(_wiadomosc + 0x0001)
   0332 74 03              1220 	mov	a,#0x03
   0334 F2                 1221 	movx	@r0,a
                    0130   1222 	C$main.c$125$4$62 ==.
                           1223 ;	main.c:125: wiadomosc[3]=0x06;      // Liczba bajtów do wysłania
   0335 78 18              1224 	mov	r0,#(_wiadomosc + 0x0003)
   0337 74 06              1225 	mov	a,#0x06
   0339 F2                 1226 	movx	@r0,a
                    0135   1227 	C$main.c$126$4$62 ==.
                           1228 ;	main.c:126: wiadomosc[4]=WynikOkres>>8;	// Starsza czesc Okresu
   033A 78 30              1229 	mov	r0,#(_WynikOkres + 1)
   033C E2                 1230 	movx	a,@r0
   033D FF                 1231 	mov	r7,a
   033E 78 19              1232 	mov	r0,#(_wiadomosc + 0x0004)
   0340 EF                 1233 	mov	a,r7
   0341 F2                 1234 	movx	@r0,a
                    013D   1235 	C$main.c$127$4$62 ==.
                           1236 ;	main.c:127: wiadomosc[5]=WynikOkres;	// Mlodsza czesc Okresu
   0342 78 2F              1237 	mov	r0,#_WynikOkres
   0344 E2                 1238 	movx	a,@r0
   0345 FF                 1239 	mov	r7,a
   0346 78 1A              1240 	mov	r0,#(_wiadomosc + 0x0005)
   0348 EF                 1241 	mov	a,r7
   0349 F2                 1242 	movx	@r0,a
                    0145   1243 	C$main.c$128$4$62 ==.
                           1244 ;	main.c:128: wiadomosc[6]=WynikFaza>>8;	// Starsza cesc Fazy
   034A 78 32              1245 	mov	r0,#(_WynikFaza + 1)
   034C E2                 1246 	movx	a,@r0
   034D FF                 1247 	mov	r7,a
   034E 78 1B              1248 	mov	r0,#(_wiadomosc + 0x0006)
   0350 EF                 1249 	mov	a,r7
   0351 F2                 1250 	movx	@r0,a
                    014D   1251 	C$main.c$129$4$62 ==.
                           1252 ;	main.c:129: wiadomosc[7]=WynikFaza;	// Mlodsza czesc Fazy
   0352 78 31              1253 	mov	r0,#_WynikFaza
   0354 E2                 1254 	movx	a,@r0
   0355 FF                 1255 	mov	r7,a
   0356 78 1C              1256 	mov	r0,#(_wiadomosc + 0x0007)
   0358 EF                 1257 	mov	a,r7
   0359 F2                 1258 	movx	@r0,a
                    0155   1259 	C$main.c$130$4$62 ==.
                           1260 ;	main.c:130: while (SCONV);		    // Czekaj na koniec konwersji
   035A                    1261 00103$:
   035A 20 DC FD           1262 	jb	_SCONV,00103$
                    0158   1263 	C$main.c$131$4$62 ==.
                           1264 ;	main.c:131: wiadomosc[8]=ADCDATAH;	// Wyslij czesc starsza Temperatury
   035D 78 1D              1265 	mov	r0,#(_wiadomosc + 0x0008)
   035F E5 DA              1266 	mov	a,_ADCDATAH
   0361 F2                 1267 	movx	@r0,a
                    015D   1268 	C$main.c$132$4$62 ==.
                           1269 ;	main.c:132: wiadomosc[9]=ADCDATAL;	// Wyslij czesc mlodsza Temperatury
   0362 78 1E              1270 	mov	r0,#(_wiadomosc + 0x0009)
   0364 E5 D9              1271 	mov	a,_ADCDATAL
   0366 F2                 1272 	movx	@r0,a
                    0162   1273 	C$main.c$133$4$62 ==.
                           1274 ;	main.c:133: _delay_us(200);
   0367 75 82 C8           1275 	mov	dpl,#0xC8
   036A 12 08 48           1276 	lcall	__delay_us
                    0168   1277 	C$main.c$134$4$62 ==.
                           1278 ;	main.c:134: _delay_us(147);// Czas 4 znaków
   036D 75 82 93           1279 	mov	dpl,#0x93
   0370 12 08 48           1280 	lcall	__delay_us
                    016E   1281 	C$main.c$135$1$56 ==.
                           1282 ;	main.c:135: for (i=0; i<=9; i++){
   0373 7E 00              1283 	mov	r6,#0x00
   0375 7F 00              1284 	mov	r7,#0x00
   0377                    1285 00134$:
   0377 C3                 1286 	clr	c
   0378 74 09              1287 	mov	a,#0x09
   037A 9E                 1288 	subb	a,r6
   037B E4                 1289 	clr	a
   037C 64 80              1290 	xrl	a,#0x80
   037E 8F F0              1291 	mov	b,r7
   0380 63 F0 80           1292 	xrl	b,#0x80
   0383 95 F0              1293 	subb	a,b
   0385 40 2F              1294 	jc	00137$
                    0182   1295 	C$main.c$136$5$63 ==.
                           1296 ;	main.c:136: crc_modbus=update_crc_16(crc_modbus,wiadomosc[i]);
   0387 EE                 1297 	mov	a,r6
   0388 24 15              1298 	add	a,#_wiadomosc
   038A F9                 1299 	mov	r1,a
   038B 78 51              1300 	mov	r0,#_update_crc_16_PARM_2
   038D E3                 1301 	movx	a,@r1
   038E F2                 1302 	movx	@r0,a
   038F 78 45              1303 	mov	r0,#_crc_modbus
   0391 E2                 1304 	movx	a,@r0
   0392 F5 82              1305 	mov	dpl,a
   0394 08                 1306 	inc	r0
   0395 E2                 1307 	movx	a,@r0
   0396 F5 83              1308 	mov	dph,a
   0398 C0 07              1309 	push	ar7
   039A C0 06              1310 	push	ar6
   039C 12 06 5F           1311 	lcall	_update_crc_16
   039F E5 82              1312 	mov	a,dpl
   03A1 85 83 F0           1313 	mov	b,dph
   03A4 D0 06              1314 	pop	ar6
   03A6 D0 07              1315 	pop	ar7
   03A8 78 45              1316 	mov	r0,#_crc_modbus
   03AA F2                 1317 	movx	@r0,a
   03AB 08                 1318 	inc	r0
   03AC E5 F0              1319 	mov	a,b
   03AE F2                 1320 	movx	@r0,a
                    01AA   1321 	C$main.c$135$4$62 ==.
                           1322 ;	main.c:135: for (i=0; i<=9; i++){
   03AF 0E                 1323 	inc	r6
   03B0 BE 00 C4           1324 	cjne	r6,#0x00,00134$
   03B3 0F                 1325 	inc	r7
   03B4 80 C1              1326 	sjmp	00134$
   03B6                    1327 00137$:
                    01B1   1328 	C$main.c$138$4$62 ==.
                           1329 ;	main.c:138: wiadomosc[10]=crc_modbus;       //Część mlodsza
   03B6 78 45              1330 	mov	r0,#_crc_modbus
   03B8 E2                 1331 	movx	a,@r0
   03B9 FF                 1332 	mov	r7,a
   03BA 78 1F              1333 	mov	r0,#(_wiadomosc + 0x000a)
   03BC EF                 1334 	mov	a,r7
   03BD F2                 1335 	movx	@r0,a
                    01B9   1336 	C$main.c$139$4$62 ==.
                           1337 ;	main.c:139: wiadomosc[11]=crc_modbus>>8;    //Część starsza
   03BE 78 46              1338 	mov	r0,#(_crc_modbus + 1)
   03C0 E2                 1339 	movx	a,@r0
   03C1 FF                 1340 	mov	r7,a
   03C2 78 20              1341 	mov	r0,#(_wiadomosc + 0x000b)
   03C4 EF                 1342 	mov	a,r7
   03C5 F2                 1343 	movx	@r0,a
                    01C1   1344 	C$main.c$141$1$56 ==.
                           1345 ;	main.c:141: for (i=0; i<=11; i++){
   03C6 7E 00              1346 	mov	r6,#0x00
   03C8 7F 00              1347 	mov	r7,#0x00
   03CA                    1348 00138$:
   03CA C3                 1349 	clr	c
   03CB 74 0B              1350 	mov	a,#0x0B
   03CD 9E                 1351 	subb	a,r6
   03CE E4                 1352 	clr	a
   03CF 64 80              1353 	xrl	a,#0x80
   03D1 8F F0              1354 	mov	b,r7
   03D3 63 F0 80           1355 	xrl	b,#0x80
   03D6 95 F0              1356 	subb	a,b
   03D8 40 19              1357 	jc	00111$
                    01D5   1358 	C$main.c$142$5$64 ==.
                           1359 ;	main.c:142: putchar(wiadomosc[i]);
   03DA EE                 1360 	mov	a,r6
   03DB 24 15              1361 	add	a,#_wiadomosc
   03DD F9                 1362 	mov	r1,a
   03DE E3                 1363 	movx	a,@r1
   03DF F5 82              1364 	mov	dpl,a
   03E1 C0 07              1365 	push	ar7
   03E3 C0 06              1366 	push	ar6
   03E5 12 06 4C           1367 	lcall	_putchar
   03E8 D0 06              1368 	pop	ar6
   03EA D0 07              1369 	pop	ar7
                    01E7   1370 	C$main.c$141$4$62 ==.
                           1371 ;	main.c:141: for (i=0; i<=11; i++){
   03EC 0E                 1372 	inc	r6
   03ED BE 00 DA           1373 	cjne	r6,#0x00,00138$
   03F0 0F                 1374 	inc	r7
   03F1 80 D7              1375 	sjmp	00138$
   03F3                    1376 00111$:
                    01EE   1377 	C$main.c$149$2$57 ==.
                           1378 ;	main.c:149: if(TIMECON&TII) // Sprawdza czy upłynęła sekunda
   03F3 E5 A1              1379 	mov	a,_TIMECON
   03F5 20 E2 03           1380 	jb	acc.2,00208$
   03F8 02 05 76           1381 	ljmp	00113$
   03FB                    1382 00208$:
                    01F6   1383 	C$main.c$151$3$65 ==.
                           1384 ;	main.c:151: TIMECON = ~TII; // Zeruje bit, odliczanie czasu od nowa
   03FB 75 A1 FB           1385 	mov	_TIMECON,#0xFB
                    01F9   1386 	C$main.c$152$1$56 ==.
                           1387 ;	main.c:152: sr_przeplM = sr_przeplM/(float)n; // Uśrednianie
   03FE 78 33              1388 	mov	r0,#_n
   0400 E2                 1389 	movx	a,@r0
   0401 F5 82              1390 	mov	dpl,a
   0403 08                 1391 	inc	r0
   0404 E2                 1392 	movx	a,@r0
   0405 F5 83              1393 	mov	dph,a
   0407 12 0B CB           1394 	lcall	___sint2fs
   040A 85 82 08           1395 	mov	_main_sloc0_1_0,dpl
   040D 85 83 09           1396 	mov	(_main_sloc0_1_0 + 1),dph
   0410 85 F0 0A           1397 	mov	(_main_sloc0_1_0 + 2),b
   0413 F5 0B              1398 	mov	(_main_sloc0_1_0 + 3),a
   0415 C0 08              1399 	push	_main_sloc0_1_0
   0417 C0 09              1400 	push	(_main_sloc0_1_0 + 1)
   0419 C0 0A              1401 	push	(_main_sloc0_1_0 + 2)
   041B C0 0B              1402 	push	(_main_sloc0_1_0 + 3)
   041D 78 35              1403 	mov	r0,#_sr_przeplM
   041F E2                 1404 	movx	a,@r0
   0420 F5 82              1405 	mov	dpl,a
   0422 08                 1406 	inc	r0
   0423 E2                 1407 	movx	a,@r0
   0424 F5 83              1408 	mov	dph,a
   0426 08                 1409 	inc	r0
   0427 E2                 1410 	movx	a,@r0
   0428 F5 F0              1411 	mov	b,a
   042A 08                 1412 	inc	r0
   042B E2                 1413 	movx	a,@r0
   042C 12 13 78           1414 	lcall	___fsdiv
   042F AA 82              1415 	mov	r2,dpl
   0431 AB 83              1416 	mov	r3,dph
   0433 AE F0              1417 	mov	r6,b
   0435 FF                 1418 	mov	r7,a
   0436 E5 81              1419 	mov	a,sp
   0438 24 FC              1420 	add	a,#0xfc
   043A F5 81              1421 	mov	sp,a
   043C 78 35              1422 	mov	r0,#_sr_przeplM
   043E EA                 1423 	mov	a,r2
   043F F2                 1424 	movx	@r0,a
   0440 08                 1425 	inc	r0
   0441 EB                 1426 	mov	a,r3
   0442 F2                 1427 	movx	@r0,a
   0443 08                 1428 	inc	r0
   0444 EE                 1429 	mov	a,r6
   0445 F2                 1430 	movx	@r0,a
   0446 08                 1431 	inc	r0
   0447 EF                 1432 	mov	a,r7
   0448 F2                 1433 	movx	@r0,a
                    0244   1434 	C$main.c$153$1$56 ==.
                           1435 ;	main.c:153: sr_przeplO = sr_przeplO/(float)n;
   0449 C0 08              1436 	push	_main_sloc0_1_0
   044B C0 09              1437 	push	(_main_sloc0_1_0 + 1)
   044D C0 0A              1438 	push	(_main_sloc0_1_0 + 2)
   044F C0 0B              1439 	push	(_main_sloc0_1_0 + 3)
   0451 78 39              1440 	mov	r0,#_sr_przeplO
   0453 E2                 1441 	movx	a,@r0
   0454 F5 82              1442 	mov	dpl,a
   0456 08                 1443 	inc	r0
   0457 E2                 1444 	movx	a,@r0
   0458 F5 83              1445 	mov	dph,a
   045A 08                 1446 	inc	r0
   045B E2                 1447 	movx	a,@r0
   045C F5 F0              1448 	mov	b,a
   045E 08                 1449 	inc	r0
   045F E2                 1450 	movx	a,@r0
   0460 12 13 78           1451 	lcall	___fsdiv
   0463 AC 82              1452 	mov	r4,dpl
   0465 AD 83              1453 	mov	r5,dph
   0467 AE F0              1454 	mov	r6,b
   0469 FF                 1455 	mov	r7,a
   046A E5 81              1456 	mov	a,sp
   046C 24 FC              1457 	add	a,#0xfc
   046E F5 81              1458 	mov	sp,a
   0470 78 39              1459 	mov	r0,#_sr_przeplO
   0472 EC                 1460 	mov	a,r4
   0473 F2                 1461 	movx	@r0,a
   0474 08                 1462 	inc	r0
   0475 ED                 1463 	mov	a,r5
   0476 F2                 1464 	movx	@r0,a
   0477 08                 1465 	inc	r0
   0478 EE                 1466 	mov	a,r6
   0479 F2                 1467 	movx	@r0,a
   047A 08                 1468 	inc	r0
   047B EF                 1469 	mov	a,r7
   047C F2                 1470 	movx	@r0,a
                    0278   1471 	C$main.c$154$3$65 ==.
                           1472 ;	main.c:154: n=0;
   047D 78 33              1473 	mov	r0,#_n
   047F E4                 1474 	clr	a
   0480 F2                 1475 	movx	@r0,a
   0481 08                 1476 	inc	r0
   0482 F2                 1477 	movx	@r0,a
                    027E   1478 	C$main.c$156$1$56 ==.
                           1479 ;	main.c:156: calk_przeplM+=sr_przeplM; //całkowanie przepływu
   0483 78 35              1480 	mov	r0,#_sr_przeplM
   0485 E2                 1481 	movx	a,@r0
   0486 C0 E0              1482 	push	acc
   0488 08                 1483 	inc	r0
   0489 E2                 1484 	movx	a,@r0
   048A C0 E0              1485 	push	acc
   048C 08                 1486 	inc	r0
   048D E2                 1487 	movx	a,@r0
   048E C0 E0              1488 	push	acc
   0490 08                 1489 	inc	r0
   0491 E2                 1490 	movx	a,@r0
   0492 C0 E0              1491 	push	acc
   0494 78 3D              1492 	mov	r0,#_calk_przeplM
   0496 E2                 1493 	movx	a,@r0
   0497 F5 82              1494 	mov	dpl,a
   0499 08                 1495 	inc	r0
   049A E2                 1496 	movx	a,@r0
   049B F5 83              1497 	mov	dph,a
   049D 08                 1498 	inc	r0
   049E E2                 1499 	movx	a,@r0
   049F F5 F0              1500 	mov	b,a
   04A1 08                 1501 	inc	r0
   04A2 E2                 1502 	movx	a,@r0
   04A3 12 0A 5D           1503 	lcall	___fsadd
   04A6 AC 82              1504 	mov	r4,dpl
   04A8 AD 83              1505 	mov	r5,dph
   04AA AE F0              1506 	mov	r6,b
   04AC FF                 1507 	mov	r7,a
   04AD E5 81              1508 	mov	a,sp
   04AF 24 FC              1509 	add	a,#0xfc
   04B1 F5 81              1510 	mov	sp,a
   04B3 78 3D              1511 	mov	r0,#_calk_przeplM
   04B5 EC                 1512 	mov	a,r4
   04B6 F2                 1513 	movx	@r0,a
   04B7 08                 1514 	inc	r0
   04B8 ED                 1515 	mov	a,r5
   04B9 F2                 1516 	movx	@r0,a
   04BA 08                 1517 	inc	r0
   04BB EE                 1518 	mov	a,r6
   04BC F2                 1519 	movx	@r0,a
   04BD 08                 1520 	inc	r0
   04BE EF                 1521 	mov	a,r7
   04BF F2                 1522 	movx	@r0,a
                    02BB   1523 	C$main.c$157$1$56 ==.
                           1524 ;	main.c:157: calk_przeplO+=sr_przeplO;
   04C0 78 39              1525 	mov	r0,#_sr_przeplO
   04C2 E2                 1526 	movx	a,@r0
   04C3 C0 E0              1527 	push	acc
   04C5 08                 1528 	inc	r0
   04C6 E2                 1529 	movx	a,@r0
   04C7 C0 E0              1530 	push	acc
   04C9 08                 1531 	inc	r0
   04CA E2                 1532 	movx	a,@r0
   04CB C0 E0              1533 	push	acc
   04CD 08                 1534 	inc	r0
   04CE E2                 1535 	movx	a,@r0
   04CF C0 E0              1536 	push	acc
   04D1 78 41              1537 	mov	r0,#_calk_przeplO
   04D3 E2                 1538 	movx	a,@r0
   04D4 F5 82              1539 	mov	dpl,a
   04D6 08                 1540 	inc	r0
   04D7 E2                 1541 	movx	a,@r0
   04D8 F5 83              1542 	mov	dph,a
   04DA 08                 1543 	inc	r0
   04DB E2                 1544 	movx	a,@r0
   04DC F5 F0              1545 	mov	b,a
   04DE 08                 1546 	inc	r0
   04DF E2                 1547 	movx	a,@r0
   04E0 12 0A 5D           1548 	lcall	___fsadd
   04E3 AC 82              1549 	mov	r4,dpl
   04E5 AD 83              1550 	mov	r5,dph
   04E7 AE F0              1551 	mov	r6,b
   04E9 FF                 1552 	mov	r7,a
   04EA E5 81              1553 	mov	a,sp
   04EC 24 FC              1554 	add	a,#0xfc
   04EE F5 81              1555 	mov	sp,a
   04F0 78 41              1556 	mov	r0,#_calk_przeplO
   04F2 EC                 1557 	mov	a,r4
   04F3 F2                 1558 	movx	@r0,a
   04F4 08                 1559 	inc	r0
   04F5 ED                 1560 	mov	a,r5
   04F6 F2                 1561 	movx	@r0,a
   04F7 08                 1562 	inc	r0
   04F8 EE                 1563 	mov	a,r6
   04F9 F2                 1564 	movx	@r0,a
   04FA 08                 1565 	inc	r0
   04FB EF                 1566 	mov	a,r7
   04FC F2                 1567 	movx	@r0,a
                    02F8   1568 	C$main.c$159$3$65 ==.
                           1569 ;	main.c:159: sprintf(str,"fi_m %f ", sr_przeplM);
   04FD 78 35              1570 	mov	r0,#_sr_przeplM
   04FF E2                 1571 	movx	a,@r0
   0500 C0 E0              1572 	push	acc
   0502 08                 1573 	inc	r0
   0503 E2                 1574 	movx	a,@r0
   0504 C0 E0              1575 	push	acc
   0506 08                 1576 	inc	r0
   0507 E2                 1577 	movx	a,@r0
   0508 C0 E0              1578 	push	acc
   050A 08                 1579 	inc	r0
   050B E2                 1580 	movx	a,@r0
   050C C0 E0              1581 	push	acc
   050E 74 61              1582 	mov	a,#__str_0
   0510 C0 E0              1583 	push	acc
   0512 74 1A              1584 	mov	a,#(__str_0 >> 8)
   0514 C0 E0              1585 	push	acc
   0516 74 80              1586 	mov	a,#0x80
   0518 C0 E0              1587 	push	acc
   051A 74 21              1588 	mov	a,#_str
   051C C0 E0              1589 	push	acc
   051E 74 00              1590 	mov	a,#(_str >> 8)
   0520 C0 E0              1591 	push	acc
   0522 74 60              1592 	mov	a,#0x60
   0524 C0 E0              1593 	push	acc
   0526 12 0B 78           1594 	lcall	_sprintf
   0529 E5 81              1595 	mov	a,sp
   052B 24 F6              1596 	add	a,#0xf6
   052D F5 81              1597 	mov	sp,a
                    032A   1598 	C$main.c$160$3$65 ==.
                           1599 ;	main.c:160: LCD_WriteText(str);
   052F 90 00 21           1600 	mov	dptr,#_str
   0532 75 F0 60           1601 	mov	b,#0x60
   0535 12 01 2B           1602 	lcall	_LCD_WriteText
                    0333   1603 	C$main.c$161$3$65 ==.
                           1604 ;	main.c:161: sprintf(str,"fi_m %f ", sr_przeplO);
   0538 78 39              1605 	mov	r0,#_sr_przeplO
   053A E2                 1606 	movx	a,@r0
   053B C0 E0              1607 	push	acc
   053D 08                 1608 	inc	r0
   053E E2                 1609 	movx	a,@r0
   053F C0 E0              1610 	push	acc
   0541 08                 1611 	inc	r0
   0542 E2                 1612 	movx	a,@r0
   0543 C0 E0              1613 	push	acc
   0545 08                 1614 	inc	r0
   0546 E2                 1615 	movx	a,@r0
   0547 C0 E0              1616 	push	acc
   0549 74 61              1617 	mov	a,#__str_0
   054B C0 E0              1618 	push	acc
   054D 74 1A              1619 	mov	a,#(__str_0 >> 8)
   054F C0 E0              1620 	push	acc
   0551 74 80              1621 	mov	a,#0x80
   0553 C0 E0              1622 	push	acc
   0555 74 21              1623 	mov	a,#_str
   0557 C0 E0              1624 	push	acc
   0559 74 00              1625 	mov	a,#(_str >> 8)
   055B C0 E0              1626 	push	acc
   055D 74 60              1627 	mov	a,#0x60
   055F C0 E0              1628 	push	acc
   0561 12 0B 78           1629 	lcall	_sprintf
   0564 E5 81              1630 	mov	a,sp
   0566 24 F6              1631 	add	a,#0xf6
   0568 F5 81              1632 	mov	sp,a
                    0365   1633 	C$main.c$162$3$65 ==.
                           1634 ;	main.c:162: LCD_WriteText(str);
   056A 90 00 21           1635 	mov	dptr,#_str
   056D 75 F0 60           1636 	mov	b,#0x60
   0570 12 01 2B           1637 	lcall	_LCD_WriteText
   0573 02 05 FD           1638 	ljmp	00114$
   0576                    1639 00113$:
                    0371   1640 	C$main.c$167$3$66 ==.
                           1641 ;	main.c:167: n++;    //dzielnik do średniej arytmetycznej
   0576 78 33              1642 	mov	r0,#_n
   0578 E2                 1643 	movx	a,@r0
   0579 24 01              1644 	add	a,#0x01
   057B F2                 1645 	movx	@r0,a
   057C 08                 1646 	inc	r0
   057D E2                 1647 	movx	a,@r0
   057E 34 00              1648 	addc	a,#0x00
   0580 F2                 1649 	movx	@r0,a
                    037C   1650 	C$main.c$168$3$66 ==.
                           1651 ;	main.c:168: sr_przeplM+=Obl_przeplywM();    //
   0581 12 06 98           1652 	lcall	_Obl_przeplywM
   0584 AC 82              1653 	mov	r4,dpl
   0586 AD 83              1654 	mov	r5,dph
   0588 AE F0              1655 	mov	r6,b
   058A FF                 1656 	mov	r7,a
   058B C0 04              1657 	push	ar4
   058D C0 05              1658 	push	ar5
   058F C0 06              1659 	push	ar6
   0591 C0 07              1660 	push	ar7
   0593 78 35              1661 	mov	r0,#_sr_przeplM
   0595 E2                 1662 	movx	a,@r0
   0596 F5 82              1663 	mov	dpl,a
   0598 08                 1664 	inc	r0
   0599 E2                 1665 	movx	a,@r0
   059A F5 83              1666 	mov	dph,a
   059C 08                 1667 	inc	r0
   059D E2                 1668 	movx	a,@r0
   059E F5 F0              1669 	mov	b,a
   05A0 08                 1670 	inc	r0
   05A1 E2                 1671 	movx	a,@r0
   05A2 12 0A 5D           1672 	lcall	___fsadd
   05A5 AC 82              1673 	mov	r4,dpl
   05A7 AD 83              1674 	mov	r5,dph
   05A9 AE F0              1675 	mov	r6,b
   05AB FF                 1676 	mov	r7,a
   05AC E5 81              1677 	mov	a,sp
   05AE 24 FC              1678 	add	a,#0xfc
   05B0 F5 81              1679 	mov	sp,a
   05B2 78 35              1680 	mov	r0,#_sr_przeplM
   05B4 EC                 1681 	mov	a,r4
   05B5 F2                 1682 	movx	@r0,a
   05B6 08                 1683 	inc	r0
   05B7 ED                 1684 	mov	a,r5
   05B8 F2                 1685 	movx	@r0,a
   05B9 08                 1686 	inc	r0
   05BA EE                 1687 	mov	a,r6
   05BB F2                 1688 	movx	@r0,a
   05BC 08                 1689 	inc	r0
   05BD EF                 1690 	mov	a,r7
   05BE F2                 1691 	movx	@r0,a
                    03BA   1692 	C$main.c$169$3$66 ==.
                           1693 ;	main.c:169: sr_przeplO+=Obl_przeplywO();    //
   05BF 12 07 63           1694 	lcall	_Obl_przeplywO
   05C2 AC 82              1695 	mov	r4,dpl
   05C4 AD 83              1696 	mov	r5,dph
   05C6 AE F0              1697 	mov	r6,b
   05C8 FF                 1698 	mov	r7,a
   05C9 C0 04              1699 	push	ar4
   05CB C0 05              1700 	push	ar5
   05CD C0 06              1701 	push	ar6
   05CF C0 07              1702 	push	ar7
   05D1 78 39              1703 	mov	r0,#_sr_przeplO
   05D3 E2                 1704 	movx	a,@r0
   05D4 F5 82              1705 	mov	dpl,a
   05D6 08                 1706 	inc	r0
   05D7 E2                 1707 	movx	a,@r0
   05D8 F5 83              1708 	mov	dph,a
   05DA 08                 1709 	inc	r0
   05DB E2                 1710 	movx	a,@r0
   05DC F5 F0              1711 	mov	b,a
   05DE 08                 1712 	inc	r0
   05DF E2                 1713 	movx	a,@r0
   05E0 12 0A 5D           1714 	lcall	___fsadd
   05E3 AC 82              1715 	mov	r4,dpl
   05E5 AD 83              1716 	mov	r5,dph
   05E7 AE F0              1717 	mov	r6,b
   05E9 FF                 1718 	mov	r7,a
   05EA E5 81              1719 	mov	a,sp
   05EC 24 FC              1720 	add	a,#0xfc
   05EE F5 81              1721 	mov	sp,a
   05F0 78 39              1722 	mov	r0,#_sr_przeplO
   05F2 EC                 1723 	mov	a,r4
   05F3 F2                 1724 	movx	@r0,a
   05F4 08                 1725 	inc	r0
   05F5 ED                 1726 	mov	a,r5
   05F6 F2                 1727 	movx	@r0,a
   05F7 08                 1728 	inc	r0
   05F8 EE                 1729 	mov	a,r6
   05F9 F2                 1730 	movx	@r0,a
   05FA 08                 1731 	inc	r0
   05FB EF                 1732 	mov	a,r7
   05FC F2                 1733 	movx	@r0,a
   05FD                    1734 00114$:
                    03F8   1735 	C$main.c$172$2$57 ==.
                           1736 ;	main.c:172: if(!RI) continue;				// Brak znaku skok na poczatek petli (czekaj na odbior znaku)
                    03F8   1737 	C$main.c$173$2$57 ==.
                           1738 ;	main.c:173: RI =0;				// Zeruj flage odebranego znaku
   05FD 10 98 03           1739 	jbc	_RI,00209$
   0600 02 02 7C           1740 	ljmp	00124$
   0603                    1741 00209$:
                    03FE   1742 	C$main.c$174$2$57 ==.
                           1743 ;	main.c:174: Znak =SBUF;		//Wysy³a znak do bufora nadawczego
   0603 AF 99              1744 	mov	r7,_SBUF
   0605 78 4F              1745 	mov	r0,#_Znak
   0607 EF                 1746 	mov	a,r7
   0608 F2                 1747 	movx	@r0,a
                    0404   1748 	C$main.c$175$2$57 ==.
                           1749 ;	main.c:175: switch (Znak)
   0609 8F 06              1750 	mov	ar6,r7
   060B BE 41 02           1751 	cjne	r6,#0x41,00210$
   060E 80 0F              1752 	sjmp	00117$
   0610                    1753 00210$:
   0610 BE 42 02           1754 	cjne	r6,#0x42,00211$
   0613 80 13              1755 	sjmp	00118$
   0615                    1756 00211$:
   0615 BE 4D 02           1757 	cjne	r6,#0x4D,00212$
   0618 80 1F              1758 	sjmp	00119$
   061A                    1759 00212$:
                    0415   1760 	C$main.c$177$3$67 ==.
                           1761 ;	main.c:177: case 'A':			// Rzadanie potwierdzenia komunikacji
   061A BE 6D 26           1762 	cjne	r6,#0x6D,00121$
   061D 80 1F              1763 	sjmp	00120$
   061F                    1764 00117$:
                    041A   1765 	C$main.c$178$3$67 ==.
                           1766 ;	main.c:178: putchar('E');		// Potwierdzenie komunikacji
   061F 75 82 45           1767 	mov	dpl,#0x45
   0622 12 06 4C           1768 	lcall	_putchar
                    0420   1769 	C$main.c$179$3$67 ==.
                           1770 ;	main.c:179: break;
   0625 02 02 7C           1771 	ljmp	00124$
                    0423   1772 	C$main.c$180$3$67 ==.
                           1773 ;	main.c:180: case 'B':
   0628                    1774 00118$:
                    0423   1775 	C$main.c$181$3$67 ==.
                           1776 ;	main.c:181: putchar('e');
   0628 75 82 65           1777 	mov	dpl,#0x65
   062B 12 06 4C           1778 	lcall	_putchar
                    0429   1779 	C$main.c$182$3$67 ==.
                           1780 ;	main.c:182: literka=getchar();
   062E 12 06 56           1781 	lcall	_getchar
   0631 E5 82              1782 	mov	a,dpl
   0633 78 50              1783 	mov	r0,#_literka
   0635 F2                 1784 	movx	@r0,a
                    0431   1785 	C$main.c$183$3$67 ==.
                           1786 ;	main.c:183: break;
   0636 02 02 7C           1787 	ljmp	00124$
                    0434   1788 	C$main.c$184$3$67 ==.
                           1789 ;	main.c:184: case 'M':
   0639                    1790 00119$:
                    0434   1791 	C$main.c$185$3$67 ==.
                           1792 ;	main.c:185: modbus=1;
   0639 D2 05              1793 	setb	_modbus
                    0436   1794 	C$main.c$186$3$67 ==.
                           1795 ;	main.c:186: break;
   063B 02 02 7C           1796 	ljmp	00124$
                    0439   1797 	C$main.c$187$3$67 ==.
                           1798 ;	main.c:187: case 'm':
   063E                    1799 00120$:
                    0439   1800 	C$main.c$188$3$67 ==.
                           1801 ;	main.c:188: modbus=0;
   063E C2 05              1802 	clr	_modbus
                    043B   1803 	C$main.c$189$3$67 ==.
                           1804 ;	main.c:189: break;
   0640 02 02 7C           1805 	ljmp	00124$
                    043E   1806 	C$main.c$190$3$67 ==.
                           1807 ;	main.c:190: default:			// Odeslij nierozpoznany znak
   0643                    1808 00121$:
                    043E   1809 	C$main.c$191$3$67 ==.
                           1810 ;	main.c:191: putchar(Znak);  // Odsy³a nierozpoznany znak
   0643 8F 82              1811 	mov	dpl,r7
   0645 12 06 4C           1812 	lcall	_putchar
                    0443   1813 	C$main.c$193$1$56 ==.
                           1814 ;	main.c:193: }
   0648 02 02 7C           1815 	ljmp	00124$
                    0446   1816 	C$main.c$195$1$56 ==.
                    0446   1817 	XG$main$0$0 ==.
   064B 22                 1818 	ret
                           1819 ;------------------------------------------------------------
                           1820 ;Allocation info for local variables in function 'putchar'
                           1821 ;------------------------------------------------------------
                    0447   1822 	G$putchar$0$0 ==.
                    0447   1823 	C$main.c$199$1$56 ==.
                           1824 ;	main.c:199: void putchar(unsigned char V)
                           1825 ;	-----------------------------------------
                           1826 ;	 function putchar
                           1827 ;	-----------------------------------------
   064C                    1828 _putchar:
   064C AF 82              1829 	mov	r7,dpl
                    0449   1830 	C$main.c$201$1$69 ==.
                           1831 ;	main.c:201: while(! TI);				// Czekaj na ewentualny koniec wyslania
   064E                    1832 00101$:
                    0449   1833 	C$main.c$202$1$69 ==.
                           1834 ;	main.c:202: TI =0;				// Zeruj flage wyslania
   064E 10 99 02           1835 	jbc	_TI,00110$
   0651 80 FB              1836 	sjmp	00101$
   0653                    1837 00110$:
                    044E   1838 	C$main.c$203$1$69 ==.
                           1839 ;	main.c:203: SBUF=V;				// Wyslij znak do bufora nadawczego
   0653 8F 99              1840 	mov	_SBUF,r7
                    0450   1841 	C$main.c$204$1$69 ==.
                    0450   1842 	XG$putchar$0$0 ==.
   0655 22                 1843 	ret
                           1844 ;------------------------------------------------------------
                           1845 ;Allocation info for local variables in function 'getchar'
                           1846 ;------------------------------------------------------------
                    0451   1847 	G$getchar$0$0 ==.
                    0451   1848 	C$main.c$206$1$69 ==.
                           1849 ;	main.c:206: extern char getchar(void)
                           1850 ;	-----------------------------------------
                           1851 ;	 function getchar
                           1852 ;	-----------------------------------------
   0656                    1853 _getchar:
                    0451   1854 	C$main.c$209$1$71 ==.
                           1855 ;	main.c:209: while(!RI); //czekaj aż odbierze
   0656                    1856 00101$:
                    0451   1857 	C$main.c$210$1$71 ==.
                           1858 ;	main.c:210: RI=0;
   0656 10 98 02           1859 	jbc	_RI,00110$
   0659 80 FB              1860 	sjmp	00101$
   065B                    1861 00110$:
                    0456   1862 	C$main.c$211$1$71 ==.
                           1863 ;	main.c:211: znak1 = SBUF;
   065B 85 99 82           1864 	mov	dpl,_SBUF
                    0459   1865 	C$main.c$212$1$71 ==.
                           1866 ;	main.c:212: return znak1;
                    0459   1867 	C$main.c$213$1$71 ==.
                    0459   1868 	XG$getchar$0$0 ==.
   065E 22                 1869 	ret
                           1870 ;------------------------------------------------------------
                           1871 ;Allocation info for local variables in function 'update_crc_16'
                           1872 ;------------------------------------------------------------
                    045A   1873 	G$update_crc_16$0$0 ==.
                    045A   1874 	C$main.c$225$1$71 ==.
                           1875 ;	main.c:225: unsigned short update_crc_16( unsigned short crc, char c )
                           1876 ;	-----------------------------------------
                           1877 ;	 function update_crc_16
                           1878 ;	-----------------------------------------
   065F                    1879 _update_crc_16:
   065F AE 82              1880 	mov	r6,dpl
   0661 AF 83              1881 	mov	r7,dph
                    045E   1882 	C$main.c$229$1$73 ==.
                           1883 ;	main.c:229: short_c = 0x00ff & (unsigned short) c;
   0663 78 51              1884 	mov	r0,#_update_crc_16_PARM_2
   0665 E2                 1885 	movx	a,@r0
   0666 FC                 1886 	mov	r4,a
   0667 E2                 1887 	movx	a,@r0
   0668 33                 1888 	rlc	a
   0669 95 E0              1889 	subb	a,acc
   066B 7D 00              1890 	mov	r5,#0x00
                    0468   1891 	C$main.c$231$1$73 ==.
                           1892 ;	main.c:231: tmp =  crc       ^ short_c;
   066D EE                 1893 	mov	a,r6
   066E 62 04              1894 	xrl	ar4,a
   0670 EF                 1895 	mov	a,r7
   0671 62 05              1896 	xrl	ar5,a
                    046E   1897 	C$main.c$232$1$73 ==.
                           1898 ;	main.c:232: crc = (crc >> 8) ^ crc_tab16[ tmp & 0xff ];
   0673 8F 02              1899 	mov	ar2,r7
   0675 E4                 1900 	clr	a
   0676 FB                 1901 	mov	r3,a
   0677 CC                 1902 	xch	a,r4
   0678 25 E0              1903 	add	a,acc
   067A CC                 1904 	xch	a,r4
   067B 33                 1905 	rlc	a
   067C FD                 1906 	mov	r5,a
   067D EC                 1907 	mov	a,r4
   067E 24 61              1908 	add	a,#_crc_tab16
   0680 F5 82              1909 	mov	dpl,a
   0682 ED                 1910 	mov	a,r5
   0683 34 18              1911 	addc	a,#(_crc_tab16 >> 8)
   0685 F5 83              1912 	mov	dph,a
   0687 E4                 1913 	clr	a
   0688 93                 1914 	movc	a,@a+dptr
   0689 FC                 1915 	mov	r4,a
   068A A3                 1916 	inc	dptr
   068B E4                 1917 	clr	a
   068C 93                 1918 	movc	a,@a+dptr
   068D FD                 1919 	mov	r5,a
   068E EC                 1920 	mov	a,r4
   068F 6A                 1921 	xrl	a,r2
   0690 FE                 1922 	mov	r6,a
   0691 ED                 1923 	mov	a,r5
   0692 6B                 1924 	xrl	a,r3
                    048E   1925 	C$main.c$234$1$73 ==.
                           1926 ;	main.c:234: return crc;
                    048E   1927 	C$main.c$235$1$73 ==.
                    048E   1928 	XG$update_crc_16$0$0 ==.
   0693 8E 82              1929 	mov	dpl,r6
   0695 F5 83              1930 	mov	dph,a
   0697 22                 1931 	ret
                           1932 ;------------------------------------------------------------
                           1933 ;Allocation info for local variables in function 'Obl_przeplywM'
                           1934 ;------------------------------------------------------------
                           1935 ;sloc0                     Allocated with name '_Obl_przeplywM_sloc0_1_0'
                           1936 ;------------------------------------------------------------
                    0493   1937 	G$Obl_przeplywM$0$0 ==.
                    0493   1938 	C$main.c$237$1$73 ==.
                           1939 ;	main.c:237: float Obl_przeplywM(void)
                           1940 ;	-----------------------------------------
                           1941 ;	 function Obl_przeplywM
                           1942 ;	-----------------------------------------
   0698                    1943 _Obl_przeplywM:
                    0493   1944 	C$main.c$240$1$75 ==.
                           1945 ;	main.c:240: fiM = (float)WynikFaza;
   0698 78 31              1946 	mov	r0,#_WynikFaza
   069A E2                 1947 	movx	a,@r0
   069B F5 82              1948 	mov	dpl,a
   069D 08                 1949 	inc	r0
   069E E2                 1950 	movx	a,@r0
   069F F5 83              1951 	mov	dph,a
   06A1 12 0B D8           1952 	lcall	___uint2fs
   06A4 AC 82              1953 	mov	r4,dpl
   06A6 AD 83              1954 	mov	r5,dph
   06A8 AE F0              1955 	mov	r6,b
   06AA FF                 1956 	mov	r7,a
                    04A6   1957 	C$main.c$241$1$75 ==.
                           1958 ;	main.c:241: fiM = (0.8096*fiM)-127.26;
   06AB C0 04              1959 	push	ar4
   06AD C0 05              1960 	push	ar5
   06AF C0 06              1961 	push	ar6
   06B1 C0 07              1962 	push	ar7
   06B3 90 41 F2           1963 	mov	dptr,#0x41F2
   06B6 75 F0 4F           1964 	mov	b,#0x4F
   06B9 74 3F              1965 	mov	a,#0x3F
   06BB 12 08 7A           1966 	lcall	___fsmul
   06BE 85 82 0C           1967 	mov	_Obl_przeplywM_sloc0_1_0,dpl
   06C1 85 83 0D           1968 	mov	(_Obl_przeplywM_sloc0_1_0 + 1),dph
   06C4 85 F0 0E           1969 	mov	(_Obl_przeplywM_sloc0_1_0 + 2),b
   06C7 F5 0F              1970 	mov	(_Obl_przeplywM_sloc0_1_0 + 3),a
   06C9 E5 81              1971 	mov	a,sp
   06CB 24 FC              1972 	add	a,#0xfc
   06CD F5 81              1973 	mov	sp,a
   06CF 74 1F              1974 	mov	a,#0x1F
   06D1 C0 E0              1975 	push	acc
   06D3 74 85              1976 	mov	a,#0x85
   06D5 C0 E0              1977 	push	acc
   06D7 74 FE              1978 	mov	a,#0xFE
   06D9 C0 E0              1979 	push	acc
   06DB 74 42              1980 	mov	a,#0x42
   06DD C0 E0              1981 	push	acc
   06DF 85 0C 82           1982 	mov	dpl,_Obl_przeplywM_sloc0_1_0
   06E2 85 0D 83           1983 	mov	dph,(_Obl_przeplywM_sloc0_1_0 + 1)
   06E5 85 0E F0           1984 	mov	b,(_Obl_przeplywM_sloc0_1_0 + 2)
   06E8 E5 0F              1985 	mov	a,(_Obl_przeplywM_sloc0_1_0 + 3)
   06EA 12 08 6F           1986 	lcall	___fssub
   06ED AC 82              1987 	mov	r4,dpl
   06EF AD 83              1988 	mov	r5,dph
   06F1 AE F0              1989 	mov	r6,b
   06F3 FF                 1990 	mov	r7,a
   06F4 E5 81              1991 	mov	a,sp
   06F6 24 FC              1992 	add	a,#0xfc
   06F8 F5 81              1993 	mov	sp,a
                    04F5   1994 	C$main.c$242$1$75 ==.
                           1995 ;	main.c:242: return fiM;
   06FA 8C 82              1996 	mov	dpl,r4
   06FC 8D 83              1997 	mov	dph,r5
   06FE 8E F0              1998 	mov	b,r6
   0700 EF                 1999 	mov	a,r7
                    04FC   2000 	C$main.c$243$1$75 ==.
                    04FC   2001 	XG$Obl_przeplywM$0$0 ==.
   0701 22                 2002 	ret
                           2003 ;------------------------------------------------------------
                           2004 ;Allocation info for local variables in function 'Obl_czestotliwosc'
                           2005 ;------------------------------------------------------------
                    04FD   2006 	G$Obl_czestotliwosc$0$0 ==.
                    04FD   2007 	C$main.c$245$1$75 ==.
                           2008 ;	main.c:245: float Obl_czestotliwosc(void)
                           2009 ;	-----------------------------------------
                           2010 ;	 function Obl_czestotliwosc
                           2011 ;	-----------------------------------------
   0702                    2012 _Obl_czestotliwosc:
                    04FD   2013 	C$main.c$248$1$77 ==.
                           2014 ;	main.c:248: f = (float)WynikOkres/12000000.0;
   0702 78 2F              2015 	mov	r0,#_WynikOkres
   0704 E2                 2016 	movx	a,@r0
   0705 F5 82              2017 	mov	dpl,a
   0707 08                 2018 	inc	r0
   0708 E2                 2019 	movx	a,@r0
   0709 F5 83              2020 	mov	dph,a
   070B 12 0B D8           2021 	lcall	___uint2fs
   070E AC 82              2022 	mov	r4,dpl
   0710 AD 83              2023 	mov	r5,dph
   0712 AE F0              2024 	mov	r6,b
   0714 FF                 2025 	mov	r7,a
   0715 E4                 2026 	clr	a
   0716 C0 E0              2027 	push	acc
   0718 74 1B              2028 	mov	a,#0x1B
   071A C0 E0              2029 	push	acc
   071C 74 37              2030 	mov	a,#0x37
   071E C0 E0              2031 	push	acc
   0720 74 4B              2032 	mov	a,#0x4B
   0722 C0 E0              2033 	push	acc
   0724 8C 82              2034 	mov	dpl,r4
   0726 8D 83              2035 	mov	dph,r5
   0728 8E F0              2036 	mov	b,r6
   072A EF                 2037 	mov	a,r7
   072B 12 13 78           2038 	lcall	___fsdiv
   072E AC 82              2039 	mov	r4,dpl
   0730 AD 83              2040 	mov	r5,dph
   0732 AE F0              2041 	mov	r6,b
   0734 FF                 2042 	mov	r7,a
   0735 E5 81              2043 	mov	a,sp
   0737 24 FC              2044 	add	a,#0xfc
   0739 F5 81              2045 	mov	sp,a
                    0536   2046 	C$main.c$249$1$77 ==.
                           2047 ;	main.c:249: f = 1/f;
   073B C0 04              2048 	push	ar4
   073D C0 05              2049 	push	ar5
   073F C0 06              2050 	push	ar6
   0741 C0 07              2051 	push	ar7
   0743 90 00 00           2052 	mov	dptr,#0x0000
   0746 75 F0 80           2053 	mov	b,#0x80
   0749 74 3F              2054 	mov	a,#0x3F
   074B 12 13 78           2055 	lcall	___fsdiv
   074E AC 82              2056 	mov	r4,dpl
   0750 AD 83              2057 	mov	r5,dph
   0752 AE F0              2058 	mov	r6,b
   0754 FF                 2059 	mov	r7,a
   0755 E5 81              2060 	mov	a,sp
   0757 24 FC              2061 	add	a,#0xfc
   0759 F5 81              2062 	mov	sp,a
                    0556   2063 	C$main.c$250$1$77 ==.
                           2064 ;	main.c:250: return f;
   075B 8C 82              2065 	mov	dpl,r4
   075D 8D 83              2066 	mov	dph,r5
   075F 8E F0              2067 	mov	b,r6
   0761 EF                 2068 	mov	a,r7
                    055D   2069 	C$main.c$251$1$77 ==.
                    055D   2070 	XG$Obl_czestotliwosc$0$0 ==.
   0762 22                 2071 	ret
                           2072 ;------------------------------------------------------------
                           2073 ;Allocation info for local variables in function 'Obl_przeplywO'
                           2074 ;------------------------------------------------------------
                    055E   2075 	G$Obl_przeplywO$0$0 ==.
                    055E   2076 	C$main.c$253$1$77 ==.
                           2077 ;	main.c:253: float Obl_przeplywO(void)
                           2078 ;	-----------------------------------------
                           2079 ;	 function Obl_przeplywO
                           2080 ;	-----------------------------------------
   0763                    2081 _Obl_przeplywO:
                    055E   2082 	C$main.c$256$1$79 ==.
                           2083 ;	main.c:256: fiO = Obl_przeplywM()/Obl_gestosc();
   0763 12 06 98           2084 	lcall	_Obl_przeplywM
   0766 AC 82              2085 	mov	r4,dpl
   0768 AD 83              2086 	mov	r5,dph
   076A AE F0              2087 	mov	r6,b
   076C FF                 2088 	mov	r7,a
   076D C0 07              2089 	push	ar7
   076F C0 06              2090 	push	ar6
   0771 C0 05              2091 	push	ar5
   0773 C0 04              2092 	push	ar4
   0775 12 07 AE           2093 	lcall	_Obl_gestosc
   0778 A8 82              2094 	mov	r0,dpl
   077A A9 83              2095 	mov	r1,dph
   077C AA F0              2096 	mov	r2,b
   077E FB                 2097 	mov	r3,a
   077F D0 04              2098 	pop	ar4
   0781 D0 05              2099 	pop	ar5
   0783 D0 06              2100 	pop	ar6
   0785 D0 07              2101 	pop	ar7
   0787 C0 00              2102 	push	ar0
   0789 C0 01              2103 	push	ar1
   078B C0 02              2104 	push	ar2
   078D C0 03              2105 	push	ar3
   078F 8C 82              2106 	mov	dpl,r4
   0791 8D 83              2107 	mov	dph,r5
   0793 8E F0              2108 	mov	b,r6
   0795 EF                 2109 	mov	a,r7
   0796 12 13 78           2110 	lcall	___fsdiv
   0799 AC 82              2111 	mov	r4,dpl
   079B AD 83              2112 	mov	r5,dph
   079D AE F0              2113 	mov	r6,b
   079F FF                 2114 	mov	r7,a
   07A0 E5 81              2115 	mov	a,sp
   07A2 24 FC              2116 	add	a,#0xfc
   07A4 F5 81              2117 	mov	sp,a
                    05A1   2118 	C$main.c$257$1$79 ==.
                           2119 ;	main.c:257: return fiO;
   07A6 8C 82              2120 	mov	dpl,r4
   07A8 8D 83              2121 	mov	dph,r5
   07AA 8E F0              2122 	mov	b,r6
   07AC EF                 2123 	mov	a,r7
                    05A8   2124 	C$main.c$258$1$79 ==.
                    05A8   2125 	XG$Obl_przeplywO$0$0 ==.
   07AD 22                 2126 	ret
                           2127 ;------------------------------------------------------------
                           2128 ;Allocation info for local variables in function 'Obl_gestosc'
                           2129 ;------------------------------------------------------------
                    05A9   2130 	G$Obl_gestosc$0$0 ==.
                    05A9   2131 	C$main.c$260$1$79 ==.
                           2132 ;	main.c:260: float Obl_gestosc(void)
                           2133 ;	-----------------------------------------
                           2134 ;	 function Obl_gestosc
                           2135 ;	-----------------------------------------
   07AE                    2136 _Obl_gestosc:
                    05A9   2137 	C$main.c$263$1$81 ==.
                           2138 ;	main.c:263: rho = (powf(81.92543/Obl_czestotliwosc(),2)-1)/0.084090608;
   07AE 12 07 02           2139 	lcall	_Obl_czestotliwosc
   07B1 AC 82              2140 	mov	r4,dpl
   07B3 AD 83              2141 	mov	r5,dph
   07B5 AE F0              2142 	mov	r6,b
   07B7 FF                 2143 	mov	r7,a
   07B8 C0 04              2144 	push	ar4
   07BA C0 05              2145 	push	ar5
   07BC C0 06              2146 	push	ar6
   07BE C0 07              2147 	push	ar7
   07C0 90 D9 D2           2148 	mov	dptr,#0xD9D2
   07C3 75 F0 A3           2149 	mov	b,#0xA3
   07C6 74 42              2150 	mov	a,#0x42
   07C8 12 13 78           2151 	lcall	___fsdiv
   07CB AC 82              2152 	mov	r4,dpl
   07CD AD 83              2153 	mov	r5,dph
   07CF AE F0              2154 	mov	r6,b
   07D1 FF                 2155 	mov	r7,a
   07D2 E5 81              2156 	mov	a,sp
   07D4 24 FC              2157 	add	a,#0xfc
   07D6 F5 81              2158 	mov	sp,a
   07D8 78 52              2159 	mov	r0,#_powf_PARM_2
   07DA E4                 2160 	clr	a
   07DB F2                 2161 	movx	@r0,a
   07DC 08                 2162 	inc	r0
   07DD F2                 2163 	movx	@r0,a
   07DE 08                 2164 	inc	r0
   07DF F2                 2165 	movx	@r0,a
   07E0 08                 2166 	inc	r0
   07E1 74 40              2167 	mov	a,#0x40
   07E3 F2                 2168 	movx	@r0,a
   07E4 8C 82              2169 	mov	dpl,r4
   07E6 8D 83              2170 	mov	dph,r5
   07E8 8E F0              2171 	mov	b,r6
   07EA EF                 2172 	mov	a,r7
   07EB 12 09 7E           2173 	lcall	_powf
   07EE AC 82              2174 	mov	r4,dpl
   07F0 AD 83              2175 	mov	r5,dph
   07F2 AE F0              2176 	mov	r6,b
   07F4 FF                 2177 	mov	r7,a
   07F5 E4                 2178 	clr	a
   07F6 C0 E0              2179 	push	acc
   07F8 C0 E0              2180 	push	acc
   07FA 74 80              2181 	mov	a,#0x80
   07FC C0 E0              2182 	push	acc
   07FE 74 3F              2183 	mov	a,#0x3F
   0800 C0 E0              2184 	push	acc
   0802 8C 82              2185 	mov	dpl,r4
   0804 8D 83              2186 	mov	dph,r5
   0806 8E F0              2187 	mov	b,r6
   0808 EF                 2188 	mov	a,r7
   0809 12 08 6F           2189 	lcall	___fssub
   080C AC 82              2190 	mov	r4,dpl
   080E AD 83              2191 	mov	r5,dph
   0810 AE F0              2192 	mov	r6,b
   0812 FF                 2193 	mov	r7,a
   0813 E5 81              2194 	mov	a,sp
   0815 24 FC              2195 	add	a,#0xfc
   0817 F5 81              2196 	mov	sp,a
   0819 74 B2              2197 	mov	a,#0xB2
   081B C0 E0              2198 	push	acc
   081D 74 37              2199 	mov	a,#0x37
   081F C0 E0              2200 	push	acc
   0821 74 AC              2201 	mov	a,#0xAC
   0823 C0 E0              2202 	push	acc
   0825 74 3D              2203 	mov	a,#0x3D
   0827 C0 E0              2204 	push	acc
   0829 8C 82              2205 	mov	dpl,r4
   082B 8D 83              2206 	mov	dph,r5
   082D 8E F0              2207 	mov	b,r6
   082F EF                 2208 	mov	a,r7
   0830 12 13 78           2209 	lcall	___fsdiv
   0833 AC 82              2210 	mov	r4,dpl
   0835 AD 83              2211 	mov	r5,dph
   0837 AE F0              2212 	mov	r6,b
   0839 FF                 2213 	mov	r7,a
   083A E5 81              2214 	mov	a,sp
   083C 24 FC              2215 	add	a,#0xfc
   083E F5 81              2216 	mov	sp,a
                    063B   2217 	C$main.c$264$1$81 ==.
                           2218 ;	main.c:264: return rho;
   0840 8C 82              2219 	mov	dpl,r4
   0842 8D 83              2220 	mov	dph,r5
   0844 8E F0              2221 	mov	b,r6
   0846 EF                 2222 	mov	a,r7
                    0642   2223 	C$main.c$265$1$81 ==.
                    0642   2224 	XG$Obl_gestosc$0$0 ==.
   0847 22                 2225 	ret
                           2226 	.area CSEG    (CODE)
                           2227 	.area CONST   (CODE)
                    0000   2228 G$crc_tab16$0$0 == .
   1861                    2229 _crc_tab16:
   1861 00 00              2230 	.byte #0x00,#0x00	; 0
   1863 C1 C0              2231 	.byte #0xC1,#0xC0	; 49345
   1865 81 C1              2232 	.byte #0x81,#0xC1	; 49537
   1867 40 01              2233 	.byte #0x40,#0x01	; 320
   1869 01 C3              2234 	.byte #0x01,#0xC3	; 49921
   186B C0 03              2235 	.byte #0xC0,#0x03	; 960
   186D 80 02              2236 	.byte #0x80,#0x02	; 640
   186F 41 C2              2237 	.byte #0x41,#0xC2	; 49729
   1871 01 C6              2238 	.byte #0x01,#0xC6	; 50689
   1873 C0 06              2239 	.byte #0xC0,#0x06	; 1728
   1875 80 07              2240 	.byte #0x80,#0x07	; 1920
   1877 41 C7              2241 	.byte #0x41,#0xC7	; 51009
   1879 00 05              2242 	.byte #0x00,#0x05	; 1280
   187B C1 C5              2243 	.byte #0xC1,#0xC5	; 50625
   187D 81 C4              2244 	.byte #0x81,#0xC4	; 50305
   187F 40 04              2245 	.byte #0x40,#0x04	; 1088
   1881 01 CC              2246 	.byte #0x01,#0xCC	; 52225
   1883 C0 0C              2247 	.byte #0xC0,#0x0C	; 3264
   1885 80 0D              2248 	.byte #0x80,#0x0D	; 3456
   1887 41 CD              2249 	.byte #0x41,#0xCD	; 52545
   1889 00 0F              2250 	.byte #0x00,#0x0F	; 3840
   188B C1 CF              2251 	.byte #0xC1,#0xCF	; 53185
   188D 81 CE              2252 	.byte #0x81,#0xCE	; 52865
   188F 40 0E              2253 	.byte #0x40,#0x0E	; 3648
   1891 00 0A              2254 	.byte #0x00,#0x0A	; 2560
   1893 C1 CA              2255 	.byte #0xC1,#0xCA	; 51905
   1895 81 CB              2256 	.byte #0x81,#0xCB	; 52097
   1897 40 0B              2257 	.byte #0x40,#0x0B	; 2880
   1899 01 C9              2258 	.byte #0x01,#0xC9	; 51457
   189B C0 09              2259 	.byte #0xC0,#0x09	; 2496
   189D 80 08              2260 	.byte #0x80,#0x08	; 2176
   189F 41 C8              2261 	.byte #0x41,#0xC8	; 51265
   18A1 01 D8              2262 	.byte #0x01,#0xD8	; 55297
   18A3 C0 18              2263 	.byte #0xC0,#0x18	; 6336
   18A5 80 19              2264 	.byte #0x80,#0x19	; 6528
   18A7 41 D9              2265 	.byte #0x41,#0xD9	; 55617
   18A9 00 1B              2266 	.byte #0x00,#0x1B	; 6912
   18AB C1 DB              2267 	.byte #0xC1,#0xDB	; 56257
   18AD 81 DA              2268 	.byte #0x81,#0xDA	; 55937
   18AF 40 1A              2269 	.byte #0x40,#0x1A	; 6720
   18B1 00 1E              2270 	.byte #0x00,#0x1E	; 7680
   18B3 C1 DE              2271 	.byte #0xC1,#0xDE	; 57025
   18B5 81 DF              2272 	.byte #0x81,#0xDF	; 57217
   18B7 40 1F              2273 	.byte #0x40,#0x1F	; 8000
   18B9 01 DD              2274 	.byte #0x01,#0xDD	; 56577
   18BB C0 1D              2275 	.byte #0xC0,#0x1D	; 7616
   18BD 80 1C              2276 	.byte #0x80,#0x1C	; 7296
   18BF 41 DC              2277 	.byte #0x41,#0xDC	; 56385
   18C1 00 14              2278 	.byte #0x00,#0x14	; 5120
   18C3 C1 D4              2279 	.byte #0xC1,#0xD4	; 54465
   18C5 81 D5              2280 	.byte #0x81,#0xD5	; 54657
   18C7 40 15              2281 	.byte #0x40,#0x15	; 5440
   18C9 01 D7              2282 	.byte #0x01,#0xD7	; 55041
   18CB C0 17              2283 	.byte #0xC0,#0x17	; 6080
   18CD 80 16              2284 	.byte #0x80,#0x16	; 5760
   18CF 41 D6              2285 	.byte #0x41,#0xD6	; 54849
   18D1 01 D2              2286 	.byte #0x01,#0xD2	; 53761
   18D3 C0 12              2287 	.byte #0xC0,#0x12	; 4800
   18D5 80 13              2288 	.byte #0x80,#0x13	; 4992
   18D7 41 D3              2289 	.byte #0x41,#0xD3	; 54081
   18D9 00 11              2290 	.byte #0x00,#0x11	; 4352
   18DB C1 D1              2291 	.byte #0xC1,#0xD1	; 53697
   18DD 81 D0              2292 	.byte #0x81,#0xD0	; 53377
   18DF 40 10              2293 	.byte #0x40,#0x10	; 4160
   18E1 01 F0              2294 	.byte #0x01,#0xF0	; 61441
   18E3 C0 30              2295 	.byte #0xC0,#0x30	; 12480
   18E5 80 31              2296 	.byte #0x80,#0x31	; 12672
   18E7 41 F1              2297 	.byte #0x41,#0xF1	; 61761
   18E9 00 33              2298 	.byte #0x00,#0x33	; 13056
   18EB C1 F3              2299 	.byte #0xC1,#0xF3	; 62401
   18ED 81 F2              2300 	.byte #0x81,#0xF2	; 62081
   18EF 40 32              2301 	.byte #0x40,#0x32	; 12864
   18F1 00 36              2302 	.byte #0x00,#0x36	; 13824
   18F3 C1 F6              2303 	.byte #0xC1,#0xF6	; 63169
   18F5 81 F7              2304 	.byte #0x81,#0xF7	; 63361
   18F7 40 37              2305 	.byte #0x40,#0x37	; 14144
   18F9 01 F5              2306 	.byte #0x01,#0xF5	; 62721
   18FB C0 35              2307 	.byte #0xC0,#0x35	; 13760
   18FD 80 34              2308 	.byte #0x80,#0x34	; 13440
   18FF 41 F4              2309 	.byte #0x41,#0xF4	; 62529
   1901 00 3C              2310 	.byte #0x00,#0x3C	; 15360
   1903 C1 FC              2311 	.byte #0xC1,#0xFC	; 64705
   1905 81 FD              2312 	.byte #0x81,#0xFD	; 64897
   1907 40 3D              2313 	.byte #0x40,#0x3D	; 15680
   1909 01 FF              2314 	.byte #0x01,#0xFF	; 65281
   190B C0 3F              2315 	.byte #0xC0,#0x3F	; 16320
   190D 80 3E              2316 	.byte #0x80,#0x3E	; 16000
   190F 41 FE              2317 	.byte #0x41,#0xFE	; 65089
   1911 01 FA              2318 	.byte #0x01,#0xFA	; 64001
   1913 C0 3A              2319 	.byte #0xC0,#0x3A	; 15040
   1915 80 3B              2320 	.byte #0x80,#0x3B	; 15232
   1917 41 FB              2321 	.byte #0x41,#0xFB	; 64321
   1919 00 39              2322 	.byte #0x00,#0x39	; 14592
   191B C1 F9              2323 	.byte #0xC1,#0xF9	; 63937
   191D 81 F8              2324 	.byte #0x81,#0xF8	; 63617
   191F 40 38              2325 	.byte #0x40,#0x38	; 14400
   1921 00 28              2326 	.byte #0x00,#0x28	; 10240
   1923 C1 E8              2327 	.byte #0xC1,#0xE8	; 59585
   1925 81 E9              2328 	.byte #0x81,#0xE9	; 59777
   1927 40 29              2329 	.byte #0x40,#0x29	; 10560
   1929 01 EB              2330 	.byte #0x01,#0xEB	; 60161
   192B C0 2B              2331 	.byte #0xC0,#0x2B	; 11200
   192D 80 2A              2332 	.byte #0x80,#0x2A	; 10880
   192F 41 EA              2333 	.byte #0x41,#0xEA	; 59969
   1931 01 EE              2334 	.byte #0x01,#0xEE	; 60929
   1933 C0 2E              2335 	.byte #0xC0,#0x2E	; 11968
   1935 80 2F              2336 	.byte #0x80,#0x2F	; 12160
   1937 41 EF              2337 	.byte #0x41,#0xEF	; 61249
   1939 00 2D              2338 	.byte #0x00,#0x2D	; 11520
   193B C1 ED              2339 	.byte #0xC1,#0xED	; 60865
   193D 81 EC              2340 	.byte #0x81,#0xEC	; 60545
   193F 40 2C              2341 	.byte #0x40,#0x2C	; 11328
   1941 01 E4              2342 	.byte #0x01,#0xE4	; 58369
   1943 C0 24              2343 	.byte #0xC0,#0x24	; 9408
   1945 80 25              2344 	.byte #0x80,#0x25	; 9600
   1947 41 E5              2345 	.byte #0x41,#0xE5	; 58689
   1949 00 27              2346 	.byte #0x00,#0x27	; 9984
   194B C1 E7              2347 	.byte #0xC1,#0xE7	; 59329
   194D 81 E6              2348 	.byte #0x81,#0xE6	; 59009
   194F 40 26              2349 	.byte #0x40,#0x26	; 9792
   1951 00 22              2350 	.byte #0x00,#0x22	; 8704
   1953 C1 E2              2351 	.byte #0xC1,#0xE2	; 58049
   1955 81 E3              2352 	.byte #0x81,#0xE3	; 58241
   1957 40 23              2353 	.byte #0x40,#0x23	; 9024
   1959 01 E1              2354 	.byte #0x01,#0xE1	; 57601
   195B C0 21              2355 	.byte #0xC0,#0x21	; 8640
   195D 80 20              2356 	.byte #0x80,#0x20	; 8320
   195F 41 E0              2357 	.byte #0x41,#0xE0	; 57409
   1961 01 A0              2358 	.byte #0x01,#0xA0	; 40961
   1963 C0 60              2359 	.byte #0xC0,#0x60	; 24768
   1965 80 61              2360 	.byte #0x80,#0x61	; 24960
   1967 41 A1              2361 	.byte #0x41,#0xA1	; 41281
   1969 00 63              2362 	.byte #0x00,#0x63	; 25344
   196B C1 A3              2363 	.byte #0xC1,#0xA3	; 41921
   196D 81 A2              2364 	.byte #0x81,#0xA2	; 41601
   196F 40 62              2365 	.byte #0x40,#0x62	; 25152
   1971 00 66              2366 	.byte #0x00,#0x66	; 26112
   1973 C1 A6              2367 	.byte #0xC1,#0xA6	; 42689
   1975 81 A7              2368 	.byte #0x81,#0xA7	; 42881
   1977 40 67              2369 	.byte #0x40,#0x67	; 26432
   1979 01 A5              2370 	.byte #0x01,#0xA5	; 42241
   197B C0 65              2371 	.byte #0xC0,#0x65	; 26048
   197D 80 64              2372 	.byte #0x80,#0x64	; 25728
   197F 41 A4              2373 	.byte #0x41,#0xA4	; 42049
   1981 00 6C              2374 	.byte #0x00,#0x6C	; 27648
   1983 C1 AC              2375 	.byte #0xC1,#0xAC	; 44225
   1985 81 AD              2376 	.byte #0x81,#0xAD	; 44417
   1987 40 6D              2377 	.byte #0x40,#0x6D	; 27968
   1989 01 AF              2378 	.byte #0x01,#0xAF	; 44801
   198B C0 6F              2379 	.byte #0xC0,#0x6F	; 28608
   198D 80 6E              2380 	.byte #0x80,#0x6E	; 28288
   198F 41 AE              2381 	.byte #0x41,#0xAE	; 44609
   1991 01 AA              2382 	.byte #0x01,#0xAA	; 43521
   1993 C0 6A              2383 	.byte #0xC0,#0x6A	; 27328
   1995 80 6B              2384 	.byte #0x80,#0x6B	; 27520
   1997 41 AB              2385 	.byte #0x41,#0xAB	; 43841
   1999 00 69              2386 	.byte #0x00,#0x69	; 26880
   199B C1 A9              2387 	.byte #0xC1,#0xA9	; 43457
   199D 81 A8              2388 	.byte #0x81,#0xA8	; 43137
   199F 40 68              2389 	.byte #0x40,#0x68	; 26688
   19A1 00 78              2390 	.byte #0x00,#0x78	; 30720
   19A3 C1 B8              2391 	.byte #0xC1,#0xB8	; 47297
   19A5 81 B9              2392 	.byte #0x81,#0xB9	; 47489
   19A7 40 79              2393 	.byte #0x40,#0x79	; 31040
   19A9 01 BB              2394 	.byte #0x01,#0xBB	; 47873
   19AB C0 7B              2395 	.byte #0xC0,#0x7B	; 31680
   19AD 80 7A              2396 	.byte #0x80,#0x7A	; 31360
   19AF 41 BA              2397 	.byte #0x41,#0xBA	; 47681
   19B1 01 BE              2398 	.byte #0x01,#0xBE	; 48641
   19B3 C0 7E              2399 	.byte #0xC0,#0x7E	; 32448
   19B5 80 7F              2400 	.byte #0x80,#0x7F	; 32640
   19B7 41 BF              2401 	.byte #0x41,#0xBF	; 48961
   19B9 00 7D              2402 	.byte #0x00,#0x7D	; 32000
   19BB C1 BD              2403 	.byte #0xC1,#0xBD	; 48577
   19BD 81 BC              2404 	.byte #0x81,#0xBC	; 48257
   19BF 40 7C              2405 	.byte #0x40,#0x7C	; 31808
   19C1 01 B4              2406 	.byte #0x01,#0xB4	; 46081
   19C3 C0 74              2407 	.byte #0xC0,#0x74	; 29888
   19C5 80 75              2408 	.byte #0x80,#0x75	; 30080
   19C7 41 B5              2409 	.byte #0x41,#0xB5	; 46401
   19C9 00 77              2410 	.byte #0x00,#0x77	; 30464
   19CB C1 B7              2411 	.byte #0xC1,#0xB7	; 47041
   19CD 81 B6              2412 	.byte #0x81,#0xB6	; 46721
   19CF 40 76              2413 	.byte #0x40,#0x76	; 30272
   19D1 00 72              2414 	.byte #0x00,#0x72	; 29184
   19D3 C1 B2              2415 	.byte #0xC1,#0xB2	; 45761
   19D5 81 B3              2416 	.byte #0x81,#0xB3	; 45953
   19D7 40 73              2417 	.byte #0x40,#0x73	; 29504
   19D9 01 B1              2418 	.byte #0x01,#0xB1	; 45313
   19DB C0 71              2419 	.byte #0xC0,#0x71	; 29120
   19DD 80 70              2420 	.byte #0x80,#0x70	; 28800
   19DF 41 B0              2421 	.byte #0x41,#0xB0	; 45121
   19E1 00 50              2422 	.byte #0x00,#0x50	; 20480
   19E3 C1 90              2423 	.byte #0xC1,#0x90	; 37057
   19E5 81 91              2424 	.byte #0x81,#0x91	; 37249
   19E7 40 51              2425 	.byte #0x40,#0x51	; 20800
   19E9 01 93              2426 	.byte #0x01,#0x93	; 37633
   19EB C0 53              2427 	.byte #0xC0,#0x53	; 21440
   19ED 80 52              2428 	.byte #0x80,#0x52	; 21120
   19EF 41 92              2429 	.byte #0x41,#0x92	; 37441
   19F1 01 96              2430 	.byte #0x01,#0x96	; 38401
   19F3 C0 56              2431 	.byte #0xC0,#0x56	; 22208
   19F5 80 57              2432 	.byte #0x80,#0x57	; 22400
   19F7 41 97              2433 	.byte #0x41,#0x97	; 38721
   19F9 00 55              2434 	.byte #0x00,#0x55	; 21760
   19FB C1 95              2435 	.byte #0xC1,#0x95	; 38337
   19FD 81 94              2436 	.byte #0x81,#0x94	; 38017
   19FF 40 54              2437 	.byte #0x40,#0x54	; 21568
   1A01 01 9C              2438 	.byte #0x01,#0x9C	; 39937
   1A03 C0 5C              2439 	.byte #0xC0,#0x5C	; 23744
   1A05 80 5D              2440 	.byte #0x80,#0x5D	; 23936
   1A07 41 9D              2441 	.byte #0x41,#0x9D	; 40257
   1A09 00 5F              2442 	.byte #0x00,#0x5F	; 24320
   1A0B C1 9F              2443 	.byte #0xC1,#0x9F	; 40897
   1A0D 81 9E              2444 	.byte #0x81,#0x9E	; 40577
   1A0F 40 5E              2445 	.byte #0x40,#0x5E	; 24128
   1A11 00 5A              2446 	.byte #0x00,#0x5A	; 23040
   1A13 C1 9A              2447 	.byte #0xC1,#0x9A	; 39617
   1A15 81 9B              2448 	.byte #0x81,#0x9B	; 39809
   1A17 40 5B              2449 	.byte #0x40,#0x5B	; 23360
   1A19 01 99              2450 	.byte #0x01,#0x99	; 39169
   1A1B C0 59              2451 	.byte #0xC0,#0x59	; 22976
   1A1D 80 58              2452 	.byte #0x80,#0x58	; 22656
   1A1F 41 98              2453 	.byte #0x41,#0x98	; 38977
   1A21 01 88              2454 	.byte #0x01,#0x88	; 34817
   1A23 C0 48              2455 	.byte #0xC0,#0x48	; 18624
   1A25 80 49              2456 	.byte #0x80,#0x49	; 18816
   1A27 41 89              2457 	.byte #0x41,#0x89	; 35137
   1A29 00 4B              2458 	.byte #0x00,#0x4B	; 19200
   1A2B C1 8B              2459 	.byte #0xC1,#0x8B	; 35777
   1A2D 81 8A              2460 	.byte #0x81,#0x8A	; 35457
   1A2F 40 4A              2461 	.byte #0x40,#0x4A	; 19008
   1A31 00 4E              2462 	.byte #0x00,#0x4E	; 19968
   1A33 C1 8E              2463 	.byte #0xC1,#0x8E	; 36545
   1A35 81 8F              2464 	.byte #0x81,#0x8F	; 36737
   1A37 40 4F              2465 	.byte #0x40,#0x4F	; 20288
   1A39 01 8D              2466 	.byte #0x01,#0x8D	; 36097
   1A3B C0 4D              2467 	.byte #0xC0,#0x4D	; 19904
   1A3D 80 4C              2468 	.byte #0x80,#0x4C	; 19584
   1A3F 41 8C              2469 	.byte #0x41,#0x8C	; 35905
   1A41 00 44              2470 	.byte #0x00,#0x44	; 17408
   1A43 C1 84              2471 	.byte #0xC1,#0x84	; 33985
   1A45 81 85              2472 	.byte #0x81,#0x85	; 34177
   1A47 40 45              2473 	.byte #0x40,#0x45	; 17728
   1A49 01 87              2474 	.byte #0x01,#0x87	; 34561
   1A4B C0 47              2475 	.byte #0xC0,#0x47	; 18368
   1A4D 80 46              2476 	.byte #0x80,#0x46	; 18048
   1A4F 41 86              2477 	.byte #0x41,#0x86	; 34369
   1A51 01 82              2478 	.byte #0x01,#0x82	; 33281
   1A53 C0 42              2479 	.byte #0xC0,#0x42	; 17088
   1A55 80 43              2480 	.byte #0x80,#0x43	; 17280
   1A57 41 83              2481 	.byte #0x41,#0x83	; 33601
   1A59 00 41              2482 	.byte #0x00,#0x41	; 16640
   1A5B C1 81              2483 	.byte #0xC1,#0x81	; 33217
   1A5D 81 80              2484 	.byte #0x81,#0x80	; 32897
   1A5F 40 40              2485 	.byte #0x40,#0x40	; 16448
                    0200   2486 Fmain$_str_0$0$0 == .
   1A61                    2487 __str_0:
   1A61 66 69 5F 6D 20 25  2488 	.ascii "fi_m %f "
        66 20
   1A69 00                 2489 	.db 0x00
                           2490 	.area XINIT   (CODE)
                           2491 	.area CABS    (ABS,CODE)
