;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.2.0 #8008 (Jul  6 2012) (MINGW32)
; This file was generated Fri Feb 08 23:32:46 2013
;--------------------------------------------------------
	.module HD44780
	.optsdcc -mmcs51 --model-medium
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl __LCD_Write
	.globl __LCD_OutNibble
	.globl __delay_ms
	.globl __delay_us
	.globl _SPR0
	.globl _SPR1
	.globl _CPHA
	.globl _CPOL
	.globl _SPIM
	.globl _SPE
	.globl _WCOL
	.globl _ISPI
	.globl _CS0
	.globl _CS1
	.globl _CS2
	.globl _CS3
	.globl _SCONV
	.globl _CCONV
	.globl _DMA
	.globl _ADCI
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _I2CI
	.globl _I2CTX
	.globl _I2CRS
	.globl _I2CM
	.globl _I2CID0
	.globl _I2CID1
	.globl _I2CGC
	.globl _I2CSI
	.globl _MDI
	.globl _MCO
	.globl _MDE
	.globl _MDO
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _CAP2
	.globl _CNT2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK
	.globl _RCLK
	.globl _EXF2
	.globl _TF2
	.globl _WDWR
	.globl _WDE
	.globl _WDS
	.globl _WDIR
	.globl _PRE0
	.globl _PRE1
	.globl _PRE2
	.globl _PRE3
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _PADC
	.globl _PSI
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES
	.globl _ET2
	.globl _EADC
	.globl _EA
	.globl _RI
	.globl _TI
	.globl _RB8
	.globl _TB8
	.globl _REN
	.globl _SM2
	.globl _SM1
	.globl _SM0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _T2
	.globl _T2EX
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl _DACCON
	.globl _DAC1H
	.globl _DAC1L
	.globl _DAC0H
	.globl _DAC0L
	.globl _SPICON
	.globl _SPIDAT
	.globl _ADCGAINH
	.globl _ADCGAINL
	.globl _ADCOFSH
	.globl _ADCOFSL
	.globl _ADCDATAH
	.globl _ADCDATAL
	.globl _ADCCON3
	.globl _ADCCON2
	.globl _ADCCON1
	.globl _B
	.globl _I2CCON
	.globl _ACC
	.globl _PSMCON
	.globl _PLLCON
	.globl _DMAP
	.globl _DMAH
	.globl _DMAL
	.globl _PSW
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _CHIPID
	.globl _WDCON
	.globl _EADRH
	.globl _EADRL
	.globl _EDATA4
	.globl _EDATA3
	.globl _EDATA2
	.globl _EDATA1
	.globl _ECON
	.globl _IP
	.globl _PWM1H
	.globl _PWM1L
	.globl _PWM0H
	.globl _PWM0L
	.globl _PWMCON
	.globl _IEIP2
	.globl _IE
	.globl _INTVAL
	.globl _HOUR
	.globl _MIN
	.globl _SEC
	.globl _HTHSEC
	.globl _TIMECON
	.globl _T3CON
	.globl _T3FD
	.globl _SBUF
	.globl _SCON
	.globl _I2CDAT
	.globl _I2CADD3
	.globl _I2CADD2
	.globl _I2CADD1
	.globl _I2CADD
	.globl _P3
	.globl _P2
	.globl _P1
	.globl _P0
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPCON
	.globl _DPP
	.globl _DPH
	.globl _DPL
	.globl _SPH
	.globl _SP
	.globl _CFG842
	.globl _CFG841
	.globl _LCD_GoTo_PARM_2
	.globl _LCD_WriteCommand
	.globl _LCD_WriteData
	.globl _LCD_WriteText
	.globl _LCD_GoTo
	.globl _LCD_Clear
	.globl _LCD_Home
	.globl _LCD_Initalize
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$CFG841$0$0 == 0x00af
_CFG841	=	0x00af
G$CFG842$0$0 == 0x00af
_CFG842	=	0x00af
G$SP$0$0 == 0x0081
_SP	=	0x0081
G$SPH$0$0 == 0x00b7
_SPH	=	0x00b7
G$DPL$0$0 == 0x0082
_DPL	=	0x0082
G$DPH$0$0 == 0x0083
_DPH	=	0x0083
G$DPP$0$0 == 0x0084
_DPP	=	0x0084
G$DPCON$0$0 == 0x00a7
_DPCON	=	0x00a7
G$PCON$0$0 == 0x0087
_PCON	=	0x0087
G$TCON$0$0 == 0x0088
_TCON	=	0x0088
G$TMOD$0$0 == 0x0089
_TMOD	=	0x0089
G$TL0$0$0 == 0x008a
_TL0	=	0x008a
G$TL1$0$0 == 0x008b
_TL1	=	0x008b
G$TH0$0$0 == 0x008c
_TH0	=	0x008c
G$TH1$0$0 == 0x008d
_TH1	=	0x008d
G$P0$0$0 == 0x0080
_P0	=	0x0080
G$P1$0$0 == 0x0090
_P1	=	0x0090
G$P2$0$0 == 0x00a0
_P2	=	0x00a0
G$P3$0$0 == 0x00b0
_P3	=	0x00b0
G$I2CADD$0$0 == 0x009b
_I2CADD	=	0x009b
G$I2CADD1$0$0 == 0x0091
_I2CADD1	=	0x0091
G$I2CADD2$0$0 == 0x0092
_I2CADD2	=	0x0092
G$I2CADD3$0$0 == 0x0093
_I2CADD3	=	0x0093
G$I2CDAT$0$0 == 0x009a
_I2CDAT	=	0x009a
G$SCON$0$0 == 0x0098
_SCON	=	0x0098
G$SBUF$0$0 == 0x0099
_SBUF	=	0x0099
G$T3FD$0$0 == 0x009d
_T3FD	=	0x009d
G$T3CON$0$0 == 0x009e
_T3CON	=	0x009e
G$TIMECON$0$0 == 0x00a1
_TIMECON	=	0x00a1
G$HTHSEC$0$0 == 0x00a2
_HTHSEC	=	0x00a2
G$SEC$0$0 == 0x00a3
_SEC	=	0x00a3
G$MIN$0$0 == 0x00a4
_MIN	=	0x00a4
G$HOUR$0$0 == 0x00a5
_HOUR	=	0x00a5
G$INTVAL$0$0 == 0x00a6
_INTVAL	=	0x00a6
G$IE$0$0 == 0x00a8
_IE	=	0x00a8
G$IEIP2$0$0 == 0x00a9
_IEIP2	=	0x00a9
G$PWMCON$0$0 == 0x00ae
_PWMCON	=	0x00ae
G$PWM0L$0$0 == 0x00b1
_PWM0L	=	0x00b1
G$PWM0H$0$0 == 0x00b2
_PWM0H	=	0x00b2
G$PWM1L$0$0 == 0x00b3
_PWM1L	=	0x00b3
G$PWM1H$0$0 == 0x00b4
_PWM1H	=	0x00b4
G$IP$0$0 == 0x00b8
_IP	=	0x00b8
G$ECON$0$0 == 0x00b9
_ECON	=	0x00b9
G$EDATA1$0$0 == 0x00bc
_EDATA1	=	0x00bc
G$EDATA2$0$0 == 0x00bd
_EDATA2	=	0x00bd
G$EDATA3$0$0 == 0x00be
_EDATA3	=	0x00be
G$EDATA4$0$0 == 0x00bf
_EDATA4	=	0x00bf
G$EADRL$0$0 == 0x00c6
_EADRL	=	0x00c6
G$EADRH$0$0 == 0x00c7
_EADRH	=	0x00c7
G$WDCON$0$0 == 0x00c0
_WDCON	=	0x00c0
G$CHIPID$0$0 == 0x00c2
_CHIPID	=	0x00c2
G$T2CON$0$0 == 0x00c8
_T2CON	=	0x00c8
G$RCAP2L$0$0 == 0x00ca
_RCAP2L	=	0x00ca
G$RCAP2H$0$0 == 0x00cb
_RCAP2H	=	0x00cb
G$TL2$0$0 == 0x00cc
_TL2	=	0x00cc
G$TH2$0$0 == 0x00cd
_TH2	=	0x00cd
G$PSW$0$0 == 0x00d0
_PSW	=	0x00d0
G$DMAL$0$0 == 0x00d2
_DMAL	=	0x00d2
G$DMAH$0$0 == 0x00d3
_DMAH	=	0x00d3
G$DMAP$0$0 == 0x00d4
_DMAP	=	0x00d4
G$PLLCON$0$0 == 0x00d7
_PLLCON	=	0x00d7
G$PSMCON$0$0 == 0x00df
_PSMCON	=	0x00df
G$ACC$0$0 == 0x00e0
_ACC	=	0x00e0
G$I2CCON$0$0 == 0x00e8
_I2CCON	=	0x00e8
G$B$0$0 == 0x00f0
_B	=	0x00f0
G$ADCCON1$0$0 == 0x00ef
_ADCCON1	=	0x00ef
G$ADCCON2$0$0 == 0x00d8
_ADCCON2	=	0x00d8
G$ADCCON3$0$0 == 0x00f5
_ADCCON3	=	0x00f5
G$ADCDATAL$0$0 == 0x00d9
_ADCDATAL	=	0x00d9
G$ADCDATAH$0$0 == 0x00da
_ADCDATAH	=	0x00da
G$ADCOFSL$0$0 == 0x00f1
_ADCOFSL	=	0x00f1
G$ADCOFSH$0$0 == 0x00f2
_ADCOFSH	=	0x00f2
G$ADCGAINL$0$0 == 0x00f3
_ADCGAINL	=	0x00f3
G$ADCGAINH$0$0 == 0x00f4
_ADCGAINH	=	0x00f4
G$SPIDAT$0$0 == 0x00f7
_SPIDAT	=	0x00f7
G$SPICON$0$0 == 0x00f8
_SPICON	=	0x00f8
G$DAC0L$0$0 == 0x00f9
_DAC0L	=	0x00f9
G$DAC0H$0$0 == 0x00fa
_DAC0H	=	0x00fa
G$DAC1L$0$0 == 0x00fb
_DAC1L	=	0x00fb
G$DAC1H$0$0 == 0x00fc
_DAC1H	=	0x00fc
G$DACCON$0$0 == 0x00fd
_DACCON	=	0x00fd
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$TF1$0$0 == 0x008f
_TF1	=	0x008f
G$TR1$0$0 == 0x008e
_TR1	=	0x008e
G$TF0$0$0 == 0x008d
_TF0	=	0x008d
G$TR0$0$0 == 0x008c
_TR0	=	0x008c
G$IE1$0$0 == 0x008b
_IE1	=	0x008b
G$IT1$0$0 == 0x008a
_IT1	=	0x008a
G$IE0$0$0 == 0x0089
_IE0	=	0x0089
G$IT0$0$0 == 0x0088
_IT0	=	0x0088
G$P0_0$0$0 == 0x0080
_P0_0	=	0x0080
G$P0_1$0$0 == 0x0081
_P0_1	=	0x0081
G$P0_2$0$0 == 0x0082
_P0_2	=	0x0082
G$P0_3$0$0 == 0x0083
_P0_3	=	0x0083
G$P0_4$0$0 == 0x0084
_P0_4	=	0x0084
G$P0_5$0$0 == 0x0085
_P0_5	=	0x0085
G$P0_6$0$0 == 0x0086
_P0_6	=	0x0086
G$P0_7$0$0 == 0x0087
_P0_7	=	0x0087
G$P1_0$0$0 == 0x0090
_P1_0	=	0x0090
G$P1_1$0$0 == 0x0091
_P1_1	=	0x0091
G$P1_2$0$0 == 0x0092
_P1_2	=	0x0092
G$P1_3$0$0 == 0x0093
_P1_3	=	0x0093
G$P1_4$0$0 == 0x0094
_P1_4	=	0x0094
G$P1_5$0$0 == 0x0095
_P1_5	=	0x0095
G$P1_6$0$0 == 0x0096
_P1_6	=	0x0096
G$P1_7$0$0 == 0x0097
_P1_7	=	0x0097
G$T2EX$0$0 == 0x0091
_T2EX	=	0x0091
G$T2$0$0 == 0x0090
_T2	=	0x0090
G$P2_0$0$0 == 0x00a0
_P2_0	=	0x00a0
G$P2_1$0$0 == 0x00a1
_P2_1	=	0x00a1
G$P2_2$0$0 == 0x00a2
_P2_2	=	0x00a2
G$P2_3$0$0 == 0x00a3
_P2_3	=	0x00a3
G$P2_4$0$0 == 0x00a4
_P2_4	=	0x00a4
G$P2_5$0$0 == 0x00a5
_P2_5	=	0x00a5
G$P2_6$0$0 == 0x00a6
_P2_6	=	0x00a6
G$P2_7$0$0 == 0x00a7
_P2_7	=	0x00a7
G$P3_0$0$0 == 0x00b0
_P3_0	=	0x00b0
G$P3_1$0$0 == 0x00b1
_P3_1	=	0x00b1
G$P3_2$0$0 == 0x00b2
_P3_2	=	0x00b2
G$P3_3$0$0 == 0x00b3
_P3_3	=	0x00b3
G$P3_4$0$0 == 0x00b4
_P3_4	=	0x00b4
G$P3_5$0$0 == 0x00b5
_P3_5	=	0x00b5
G$P3_6$0$0 == 0x00b6
_P3_6	=	0x00b6
G$P3_7$0$0 == 0x00b7
_P3_7	=	0x00b7
G$RXD$0$0 == 0x00b0
_RXD	=	0x00b0
G$TXD$0$0 == 0x00b1
_TXD	=	0x00b1
G$INT0$0$0 == 0x00b2
_INT0	=	0x00b2
G$INT1$0$0 == 0x00b3
_INT1	=	0x00b3
G$T0$0$0 == 0x00b4
_T0	=	0x00b4
G$T1$0$0 == 0x00b5
_T1	=	0x00b5
G$WR$0$0 == 0x00b6
_WR	=	0x00b6
G$RD$0$0 == 0x00b7
_RD	=	0x00b7
G$SM0$0$0 == 0x009f
_SM0	=	0x009f
G$SM1$0$0 == 0x009e
_SM1	=	0x009e
G$SM2$0$0 == 0x009d
_SM2	=	0x009d
G$REN$0$0 == 0x009c
_REN	=	0x009c
G$TB8$0$0 == 0x009b
_TB8	=	0x009b
G$RB8$0$0 == 0x009a
_RB8	=	0x009a
G$TI$0$0 == 0x0099
_TI	=	0x0099
G$RI$0$0 == 0x0098
_RI	=	0x0098
G$EA$0$0 == 0x00af
_EA	=	0x00af
G$EADC$0$0 == 0x00ae
_EADC	=	0x00ae
G$ET2$0$0 == 0x00ad
_ET2	=	0x00ad
G$ES$0$0 == 0x00ac
_ES	=	0x00ac
G$ET1$0$0 == 0x00ab
_ET1	=	0x00ab
G$EX1$0$0 == 0x00aa
_EX1	=	0x00aa
G$ET0$0$0 == 0x00a9
_ET0	=	0x00a9
G$EX0$0$0 == 0x00a8
_EX0	=	0x00a8
G$PSI$0$0 == 0x00bf
_PSI	=	0x00bf
G$PADC$0$0 == 0x00be
_PADC	=	0x00be
G$PT2$0$0 == 0x00bd
_PT2	=	0x00bd
G$PS$0$0 == 0x00bc
_PS	=	0x00bc
G$PT1$0$0 == 0x00bb
_PT1	=	0x00bb
G$PX1$0$0 == 0x00ba
_PX1	=	0x00ba
G$PT0$0$0 == 0x00b9
_PT0	=	0x00b9
G$PX0$0$0 == 0x00b8
_PX0	=	0x00b8
G$PRE3$0$0 == 0x00c7
_PRE3	=	0x00c7
G$PRE2$0$0 == 0x00c6
_PRE2	=	0x00c6
G$PRE1$0$0 == 0x00c5
_PRE1	=	0x00c5
G$PRE0$0$0 == 0x00c4
_PRE0	=	0x00c4
G$WDIR$0$0 == 0x00c3
_WDIR	=	0x00c3
G$WDS$0$0 == 0x00c2
_WDS	=	0x00c2
G$WDE$0$0 == 0x00c1
_WDE	=	0x00c1
G$WDWR$0$0 == 0x00c0
_WDWR	=	0x00c0
G$TF2$0$0 == 0x00cf
_TF2	=	0x00cf
G$EXF2$0$0 == 0x00ce
_EXF2	=	0x00ce
G$RCLK$0$0 == 0x00cd
_RCLK	=	0x00cd
G$TCLK$0$0 == 0x00cc
_TCLK	=	0x00cc
G$EXEN2$0$0 == 0x00cb
_EXEN2	=	0x00cb
G$TR2$0$0 == 0x00ca
_TR2	=	0x00ca
G$CNT2$0$0 == 0x00c9
_CNT2	=	0x00c9
G$CAP2$0$0 == 0x00c8
_CAP2	=	0x00c8
G$CY$0$0 == 0x00d7
_CY	=	0x00d7
G$AC$0$0 == 0x00d6
_AC	=	0x00d6
G$F0$0$0 == 0x00d5
_F0	=	0x00d5
G$RS1$0$0 == 0x00d4
_RS1	=	0x00d4
G$RS0$0$0 == 0x00d3
_RS0	=	0x00d3
G$OV$0$0 == 0x00d2
_OV	=	0x00d2
G$F1$0$0 == 0x00d1
_F1	=	0x00d1
G$P$0$0 == 0x00d0
_P	=	0x00d0
G$ACC_0$0$0 == 0x00e0
_ACC_0	=	0x00e0
G$ACC_1$0$0 == 0x00e1
_ACC_1	=	0x00e1
G$ACC_2$0$0 == 0x00e2
_ACC_2	=	0x00e2
G$ACC_3$0$0 == 0x00e3
_ACC_3	=	0x00e3
G$ACC_4$0$0 == 0x00e4
_ACC_4	=	0x00e4
G$ACC_5$0$0 == 0x00e5
_ACC_5	=	0x00e5
G$ACC_6$0$0 == 0x00e6
_ACC_6	=	0x00e6
G$ACC_7$0$0 == 0x00e7
_ACC_7	=	0x00e7
G$MDO$0$0 == 0x00ef
_MDO	=	0x00ef
G$MDE$0$0 == 0x00ee
_MDE	=	0x00ee
G$MCO$0$0 == 0x00ed
_MCO	=	0x00ed
G$MDI$0$0 == 0x00ec
_MDI	=	0x00ec
G$I2CSI$0$0 == 0x00ef
_I2CSI	=	0x00ef
G$I2CGC$0$0 == 0x00ee
_I2CGC	=	0x00ee
G$I2CID1$0$0 == 0x00ed
_I2CID1	=	0x00ed
G$I2CID0$0$0 == 0x00ec
_I2CID0	=	0x00ec
G$I2CM$0$0 == 0x00eb
_I2CM	=	0x00eb
G$I2CRS$0$0 == 0x00ea
_I2CRS	=	0x00ea
G$I2CTX$0$0 == 0x00e9
_I2CTX	=	0x00e9
G$I2CI$0$0 == 0x00e8
_I2CI	=	0x00e8
G$B_0$0$0 == 0x00f0
_B_0	=	0x00f0
G$B_1$0$0 == 0x00f1
_B_1	=	0x00f1
G$B_2$0$0 == 0x00f2
_B_2	=	0x00f2
G$B_3$0$0 == 0x00f3
_B_3	=	0x00f3
G$B_4$0$0 == 0x00f4
_B_4	=	0x00f4
G$B_5$0$0 == 0x00f5
_B_5	=	0x00f5
G$B_6$0$0 == 0x00f6
_B_6	=	0x00f6
G$B_7$0$0 == 0x00f7
_B_7	=	0x00f7
G$ADCI$0$0 == 0x00df
_ADCI	=	0x00df
G$DMA$0$0 == 0x00de
_DMA	=	0x00de
G$CCONV$0$0 == 0x00dd
_CCONV	=	0x00dd
G$SCONV$0$0 == 0x00dc
_SCONV	=	0x00dc
G$CS3$0$0 == 0x00db
_CS3	=	0x00db
G$CS2$0$0 == 0x00da
_CS2	=	0x00da
G$CS1$0$0 == 0x00d9
_CS1	=	0x00d9
G$CS0$0$0 == 0x00d8
_CS0	=	0x00d8
G$ISPI$0$0 == 0x00ff
_ISPI	=	0x00ff
G$WCOL$0$0 == 0x00fe
_WCOL	=	0x00fe
G$SPE$0$0 == 0x00fd
_SPE	=	0x00fd
G$SPIM$0$0 == 0x00fc
_SPIM	=	0x00fc
G$CPOL$0$0 == 0x00fb
_CPOL	=	0x00fb
G$CPHA$0$0 == 0x00fa
_CPHA	=	0x00fa
G$SPR1$0$0 == 0x00f9
_SPR1	=	0x00f9
G$SPR0$0$0 == 0x00f8
_SPR0	=	0x00f8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
LHD44780._LCD_OutNibble$sloc0$1$0==.
__LCD_OutNibble_sloc0_1_0:
	.ds 1
LHD44780._LCD_Write$sloc0$1$0==.
__LCD_Write_sloc0_1_0:
	.ds 1
LHD44780.LCD_WriteData$sloc0$1$0==.
_LCD_WriteData_sloc0_1_0:
	.ds 1
LHD44780.LCD_Initalize$sloc0$1$0==.
_LCD_Initalize_sloc0_1_0:
	.ds 1
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
LHD44780.LCD_GoTo$y$1$20==.
_LCD_GoTo_PARM_2:
	.ds 1
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function '_LCD_OutNibble'
;------------------------------------------------------------
	G$_LCD_OutNibble$0$0 ==.
	C$HD44780.c$21$0$0 ==.
;	HD44780.c:21: void _LCD_OutNibble(unsigned char nibbleToWrite)
;	-----------------------------------------
;	 function _LCD_OutNibble
;	-----------------------------------------
__LCD_OutNibble:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	C$HD44780.c$23$1$11 ==.
;	HD44780.c:23: if(nibbleToWrite & 0x01)
	mov	a,dpl
	mov	r7,a
	jnb	acc.0,00102$
	C$HD44780.c$24$1$11 ==.
;	HD44780.c:24: LCD_DB4_PORT |= LCD_DB4;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_3
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_3,c
	sjmp	00103$
00102$:
	C$HD44780.c$26$1$11 ==.
;	HD44780.c:26: LCD_DB4_PORT  &= ~LCD_DB4;
	mov	c,_P2_3
	mov	_P2_3,c
00103$:
	C$HD44780.c$28$1$11 ==.
;	HD44780.c:28: if(nibbleToWrite & 0x02)
	mov	a,r7
	jnb	acc.1,00105$
	C$HD44780.c$29$1$11 ==.
;	HD44780.c:29: LCD_DB5_PORT |= LCD_DB5;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_4
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_4,c
	sjmp	00106$
00105$:
	C$HD44780.c$31$1$11 ==.
;	HD44780.c:31: LCD_DB5_PORT  &= ~LCD_DB5;
	mov	c,_P2_4
	mov	_P2_4,c
00106$:
	C$HD44780.c$33$1$11 ==.
;	HD44780.c:33: if(nibbleToWrite & 0x04)
	mov	a,r7
	jnb	acc.2,00108$
	C$HD44780.c$34$1$11 ==.
;	HD44780.c:34: LCD_DB6_PORT |= LCD_DB6;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_5
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_5,c
	sjmp	00109$
00108$:
	C$HD44780.c$36$1$11 ==.
;	HD44780.c:36: LCD_DB6_PORT  &= ~LCD_DB6;
	mov	c,_P2_5
	mov	_P2_5,c
00109$:
	C$HD44780.c$38$1$11 ==.
;	HD44780.c:38: if(nibbleToWrite & 0x08)
	mov	a,r7
	jnb	acc.3,00111$
	C$HD44780.c$39$1$11 ==.
;	HD44780.c:39: LCD_DB7_PORT |= LCD_DB7;
	setb	__LCD_OutNibble_sloc0_1_0
	mov	c,_P2_6
	mov	c,__LCD_OutNibble_sloc0_1_0
	mov	_P2_6,c
	sjmp	00113$
00111$:
	C$HD44780.c$41$1$11 ==.
;	HD44780.c:41: LCD_DB7_PORT  &= ~LCD_DB7;
	mov	c,_P2_6
	mov	_P2_6,c
00113$:
	C$HD44780.c$42$1$11 ==.
	XG$_LCD_OutNibble$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function '_LCD_Write'
;------------------------------------------------------------
	G$_LCD_Write$0$0 ==.
	C$HD44780.c$48$1$11 ==.
;	HD44780.c:48: void _LCD_Write(unsigned char dataToWrite)
;	-----------------------------------------
;	 function _LCD_Write
;	-----------------------------------------
__LCD_Write:
	mov	r7,dpl
	C$HD44780.c$50$1$13 ==.
;	HD44780.c:50: LCD_E_PORT |= LCD_E;
	setb	__LCD_Write_sloc0_1_0
	mov	c,_P2_2
	mov	c,__LCD_Write_sloc0_1_0
	mov	_P2_2,c
	C$HD44780.c$51$1$13 ==.
;	HD44780.c:51: _LCD_OutNibble(dataToWrite >> 4);
	mov	a,r7
	swap	a
	anl	a,#0x0F
	mov	dpl,a
	push	ar7
	lcall	__LCD_OutNibble
	pop	ar7
	C$HD44780.c$52$1$13 ==.
;	HD44780.c:52: LCD_E_PORT &= ~LCD_E;
	mov	c,_P2_2
	mov	_P2_2,c
	C$HD44780.c$53$1$13 ==.
;	HD44780.c:53: LCD_E_PORT |= LCD_E;
	setb	__LCD_Write_sloc0_1_0
	mov	c,_P2_2
	mov	c,__LCD_Write_sloc0_1_0
	mov	_P2_2,c
	C$HD44780.c$54$1$13 ==.
;	HD44780.c:54: _LCD_OutNibble(dataToWrite);
	mov	dpl,r7
	lcall	__LCD_OutNibble
	C$HD44780.c$55$1$13 ==.
;	HD44780.c:55: LCD_E_PORT &= ~LCD_E;
	mov	c,_P2_2
	mov	_P2_2,c
	C$HD44780.c$56$1$13 ==.
;	HD44780.c:56: _delay_us(50);
	mov	dpl,#0x32
	lcall	__delay_us
	C$HD44780.c$57$1$13 ==.
	XG$_LCD_Write$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_WriteCommand'
;------------------------------------------------------------
	G$LCD_WriteCommand$0$0 ==.
	C$HD44780.c$63$1$13 ==.
;	HD44780.c:63: void LCD_WriteCommand(unsigned char commandToWrite)
;	-----------------------------------------
;	 function LCD_WriteCommand
;	-----------------------------------------
_LCD_WriteCommand:
	C$HD44780.c$65$1$15 ==.
;	HD44780.c:65: LCD_RS_PORT &= ~LCD_RS;
	mov	c,_P2_1
	mov	_P2_1,c
	C$HD44780.c$66$1$15 ==.
;	HD44780.c:66: _LCD_Write(commandToWrite);
	lcall	__LCD_Write
	C$HD44780.c$67$1$15 ==.
	XG$LCD_WriteCommand$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_WriteData'
;------------------------------------------------------------
	G$LCD_WriteData$0$0 ==.
	C$HD44780.c$73$1$15 ==.
;	HD44780.c:73: void LCD_WriteData(unsigned char dataToWrite)
;	-----------------------------------------
;	 function LCD_WriteData
;	-----------------------------------------
_LCD_WriteData:
	C$HD44780.c$75$1$17 ==.
;	HD44780.c:75: LCD_RS_PORT |= LCD_RS;
	setb	_LCD_WriteData_sloc0_1_0
	mov	c,_P2_1
	mov	c,_LCD_WriteData_sloc0_1_0
	mov	_P2_1,c
	C$HD44780.c$76$1$17 ==.
;	HD44780.c:76: _LCD_Write(dataToWrite);
	lcall	__LCD_Write
	C$HD44780.c$77$1$17 ==.
	XG$LCD_WriteData$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_WriteText'
;------------------------------------------------------------
	G$LCD_WriteText$0$0 ==.
	C$HD44780.c$83$1$17 ==.
;	HD44780.c:83: void LCD_WriteText(char * text)
;	-----------------------------------------
;	 function LCD_WriteText
;	-----------------------------------------
_LCD_WriteText:
	mov	r5,dpl
	mov	r6,dph
	mov	r7,b
	C$HD44780.c$85$1$19 ==.
;	HD44780.c:85: while(*text)
00101$:
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r4,a
	jz	00104$
	C$HD44780.c$86$1$19 ==.
;	HD44780.c:86: LCD_WriteData(*text++);
	mov	dpl,r4
	inc	r5
	cjne	r5,#0x00,00112$
	inc	r6
00112$:
	push	ar7
	push	ar6
	push	ar5
	lcall	_LCD_WriteData
	pop	ar5
	pop	ar6
	pop	ar7
	sjmp	00101$
00104$:
	C$HD44780.c$87$1$19 ==.
	XG$LCD_WriteText$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_GoTo'
;------------------------------------------------------------
	G$LCD_GoTo$0$0 ==.
	C$HD44780.c$93$1$19 ==.
;	HD44780.c:93: void LCD_GoTo(unsigned char x, unsigned char y)
;	-----------------------------------------
;	 function LCD_GoTo
;	-----------------------------------------
_LCD_GoTo:
	mov	r7,dpl
	C$HD44780.c$95$1$21 ==.
;	HD44780.c:95: LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
	mov	r0,#_LCD_GoTo_PARM_2
	movx	a,@r0
	rr	a
	rr	a
	anl	a,#0xC0
	add	a,r7
	orl	a,#0x80
	mov	dpl,a
	lcall	_LCD_WriteCommand
	C$HD44780.c$96$1$21 ==.
	XG$LCD_GoTo$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_Clear'
;------------------------------------------------------------
	G$LCD_Clear$0$0 ==.
	C$HD44780.c$102$1$21 ==.
;	HD44780.c:102: void LCD_Clear(void)
;	-----------------------------------------
;	 function LCD_Clear
;	-----------------------------------------
_LCD_Clear:
	C$HD44780.c$104$1$23 ==.
;	HD44780.c:104: LCD_WriteCommand(HD44780_CLEAR);
	mov	dpl,#0x01
	lcall	_LCD_WriteCommand
	C$HD44780.c$105$1$23 ==.
;	HD44780.c:105: _delay_ms(2);
	mov	dpl,#0x02
	lcall	__delay_ms
	C$HD44780.c$106$1$23 ==.
	XG$LCD_Clear$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_Home'
;------------------------------------------------------------
	G$LCD_Home$0$0 ==.
	C$HD44780.c$112$1$23 ==.
;	HD44780.c:112: void LCD_Home(void)
;	-----------------------------------------
;	 function LCD_Home
;	-----------------------------------------
_LCD_Home:
	C$HD44780.c$114$1$25 ==.
;	HD44780.c:114: LCD_WriteCommand(HD44780_HOME);
	mov	dpl,#0x02
	lcall	_LCD_WriteCommand
	C$HD44780.c$115$1$25 ==.
;	HD44780.c:115: _delay_ms(2);
	mov	dpl,#0x02
	lcall	__delay_ms
	C$HD44780.c$116$1$25 ==.
	XG$LCD_Home$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LCD_Initalize'
;------------------------------------------------------------
	G$LCD_Initalize$0$0 ==.
	C$HD44780.c$122$1$25 ==.
;	HD44780.c:122: void LCD_Initalize(void)
;	-----------------------------------------
;	 function LCD_Initalize
;	-----------------------------------------
_LCD_Initalize:
	C$HD44780.c$125$1$27 ==.
;	HD44780.c:125: LCD_DB4_DIR |= LCD_DB4; // Konfiguracja kierunku pracy wyprowadze�
	orl	_P2,#0x10
	C$HD44780.c$126$1$27 ==.
;	HD44780.c:126: LCD_DB5_DIR |= LCD_DB5; //
	orl	_P2,#0x20
	C$HD44780.c$127$1$27 ==.
;	HD44780.c:127: LCD_DB6_DIR |= LCD_DB6; //
	orl	_P2,#0x40
	C$HD44780.c$128$1$27 ==.
;	HD44780.c:128: LCD_DB7_DIR |= LCD_DB7; //
	orl	_P2,#0x80
	C$HD44780.c$129$1$27 ==.
;	HD44780.c:129: LCD_E_DIR 	|= LCD_E;   //
	orl	_P2,#0x08
	C$HD44780.c$130$1$27 ==.
;	HD44780.c:130: LCD_RS_DIR 	|= LCD_RS;  //
	orl	_P2,#0x04
	C$HD44780.c$131$1$27 ==.
;	HD44780.c:131: _delay_ms(15); // oczekiwanie na ustalibizowanie si� napiecia zasilajacego
	mov	dpl,#0x0F
	lcall	__delay_ms
	C$HD44780.c$132$1$27 ==.
;	HD44780.c:132: LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
	mov	c,_P2_1
	mov	_P2_1,c
	C$HD44780.c$133$1$27 ==.
;	HD44780.c:133: LCD_E_PORT &= ~LCD_E;  // wyzerowanie linii E
	mov	c,_P2_2
	mov	_P2_2,c
	C$HD44780.c$135$1$27 ==.
;	HD44780.c:135: for(i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
	mov	r7,#0x00
00101$:
	cjne	r7,#0x03,00112$
00112$:
	jnc	00104$
	C$HD44780.c$137$2$28 ==.
;	HD44780.c:137: LCD_E_PORT |= LCD_E; //  E = 1
	setb	_LCD_Initalize_sloc0_1_0
	mov	c,_P2_2
	mov	c,_LCD_Initalize_sloc0_1_0
	mov	_P2_2,c
	C$HD44780.c$138$2$28 ==.
;	HD44780.c:138: _LCD_OutNibble(0x03); // tryb 8-bitowy
	mov	dpl,#0x03
	push	ar7
	lcall	__LCD_OutNibble
	C$HD44780.c$139$2$28 ==.
;	HD44780.c:139: LCD_E_PORT &= ~LCD_E; // E = 0
	mov	c,_P2_2
	mov	_P2_2,c
	C$HD44780.c$140$2$28 ==.
;	HD44780.c:140: _delay_ms(5); // czekaj 5ms
	mov	dpl,#0x05
	lcall	__delay_ms
	pop	ar7
	C$HD44780.c$135$1$27 ==.
;	HD44780.c:135: for(i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
	inc	r7
	sjmp	00101$
00104$:
	C$HD44780.c$143$1$27 ==.
;	HD44780.c:143: LCD_E_PORT |= LCD_E; // E = 1
	setb	_LCD_Initalize_sloc0_1_0
	mov	c,_P2_2
	mov	c,_LCD_Initalize_sloc0_1_0
	mov	_P2_2,c
	C$HD44780.c$144$1$27 ==.
;	HD44780.c:144: _LCD_OutNibble(0x02); // tryb 4-bitowy
	mov	dpl,#0x02
	lcall	__LCD_OutNibble
	C$HD44780.c$145$1$27 ==.
;	HD44780.c:145: LCD_E_PORT &= ~LCD_E; // E = 0
	mov	c,_P2_2
	mov	_P2_2,c
	C$HD44780.c$147$1$27 ==.
;	HD44780.c:147: _delay_ms(1); // czekaj 1ms
	mov	dpl,#0x01
	lcall	__delay_ms
	C$HD44780.c$148$1$27 ==.
;	HD44780.c:148: LCD_WriteCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
	mov	dpl,#0x28
	lcall	_LCD_WriteCommand
	C$HD44780.c$149$1$27 ==.
;	HD44780.c:149: LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wy��czenie wyswietlacza
	mov	dpl,#0x08
	lcall	_LCD_WriteCommand
	C$HD44780.c$150$1$27 ==.
;	HD44780.c:150: LCD_WriteCommand(HD44780_CLEAR); // czyszczenie zawartos�i pamieci DDRAM
	mov	dpl,#0x01
	lcall	_LCD_WriteCommand
	C$HD44780.c$151$1$27 ==.
;	HD44780.c:151: _delay_ms(2);
	mov	dpl,#0x02
	lcall	__delay_ms
	C$HD44780.c$152$1$27 ==.
;	HD44780.c:152: LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);// inkrementaja adresu i przesuwanie kursora
	mov	dpl,#0x06
	lcall	_LCD_WriteCommand
	C$HD44780.c$153$1$27 ==.
;	HD44780.c:153: LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); // w��cz LCD, bez kursora i mrugania
	mov	dpl,#0x0C
	lcall	_LCD_WriteCommand
	C$HD44780.c$154$1$27 ==.
	XG$LCD_Initalize$0$0 ==.
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
