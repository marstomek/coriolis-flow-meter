;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.2.0 #8008 (Jul  6 2012) (MINGW32)
; This file was generated Fri Feb 08 23:32:47 2013
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-medium
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _crc_tab16
	.globl _main
	.globl _IntO_int
	.globl __delay_us
	.globl _LCD_Initalize
	.globl _LCD_Clear
	.globl _LCD_WriteText
	.globl _powf
	.globl _sprintf
	.globl _SPR0
	.globl _SPR1
	.globl _CPHA
	.globl _CPOL
	.globl _SPIM
	.globl _SPE
	.globl _WCOL
	.globl _ISPI
	.globl _CS0
	.globl _CS1
	.globl _CS2
	.globl _CS3
	.globl _SCONV
	.globl _CCONV
	.globl _DMA
	.globl _ADCI
	.globl _B_7
	.globl _B_6
	.globl _B_5
	.globl _B_4
	.globl _B_3
	.globl _B_2
	.globl _B_1
	.globl _B_0
	.globl _I2CI
	.globl _I2CTX
	.globl _I2CRS
	.globl _I2CM
	.globl _I2CID0
	.globl _I2CID1
	.globl _I2CGC
	.globl _I2CSI
	.globl _MDI
	.globl _MCO
	.globl _MDE
	.globl _MDO
	.globl _ACC_7
	.globl _ACC_6
	.globl _ACC_5
	.globl _ACC_4
	.globl _ACC_3
	.globl _ACC_2
	.globl _ACC_1
	.globl _ACC_0
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _CAP2
	.globl _CNT2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK
	.globl _RCLK
	.globl _EXF2
	.globl _TF2
	.globl _WDWR
	.globl _WDE
	.globl _WDS
	.globl _WDIR
	.globl _PRE0
	.globl _PRE1
	.globl _PRE2
	.globl _PRE3
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _PADC
	.globl _PSI
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES
	.globl _ET2
	.globl _EADC
	.globl _EA
	.globl _RI
	.globl _TI
	.globl _RB8
	.globl _TB8
	.globl _REN
	.globl _SM2
	.globl _SM1
	.globl _SM0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _T2
	.globl _T2EX
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl _DACCON
	.globl _DAC1H
	.globl _DAC1L
	.globl _DAC0H
	.globl _DAC0L
	.globl _SPICON
	.globl _SPIDAT
	.globl _ADCGAINH
	.globl _ADCGAINL
	.globl _ADCOFSH
	.globl _ADCOFSL
	.globl _ADCDATAH
	.globl _ADCDATAL
	.globl _ADCCON3
	.globl _ADCCON2
	.globl _ADCCON1
	.globl _B
	.globl _I2CCON
	.globl _ACC
	.globl _PSMCON
	.globl _PLLCON
	.globl _DMAP
	.globl _DMAH
	.globl _DMAL
	.globl _PSW
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _CHIPID
	.globl _WDCON
	.globl _EADRH
	.globl _EADRL
	.globl _EDATA4
	.globl _EDATA3
	.globl _EDATA2
	.globl _EDATA1
	.globl _ECON
	.globl _IP
	.globl _PWM1H
	.globl _PWM1L
	.globl _PWM0H
	.globl _PWM0L
	.globl _PWMCON
	.globl _IEIP2
	.globl _IE
	.globl _INTVAL
	.globl _HOUR
	.globl _MIN
	.globl _SEC
	.globl _HTHSEC
	.globl _TIMECON
	.globl _T3CON
	.globl _T3FD
	.globl _SBUF
	.globl _SCON
	.globl _I2CDAT
	.globl _I2CADD3
	.globl _I2CADD2
	.globl _I2CADD1
	.globl _I2CADD
	.globl _P3
	.globl _P2
	.globl _P1
	.globl _P0
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPCON
	.globl _DPP
	.globl _DPH
	.globl _DPL
	.globl _SPH
	.globl _SP
	.globl _CFG842
	.globl _CFG841
	.globl _update_crc_16_PARM_2
	.globl _putchar
	.globl _update_crc_16
	.globl _Obl_przeplywM
	.globl _Obl_czestotliwosc
	.globl _Obl_przeplywO
	.globl _Obl_gestosc
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$CFG841$0$0 == 0x00af
_CFG841	=	0x00af
G$CFG842$0$0 == 0x00af
_CFG842	=	0x00af
G$SP$0$0 == 0x0081
_SP	=	0x0081
G$SPH$0$0 == 0x00b7
_SPH	=	0x00b7
G$DPL$0$0 == 0x0082
_DPL	=	0x0082
G$DPH$0$0 == 0x0083
_DPH	=	0x0083
G$DPP$0$0 == 0x0084
_DPP	=	0x0084
G$DPCON$0$0 == 0x00a7
_DPCON	=	0x00a7
G$PCON$0$0 == 0x0087
_PCON	=	0x0087
G$TCON$0$0 == 0x0088
_TCON	=	0x0088
G$TMOD$0$0 == 0x0089
_TMOD	=	0x0089
G$TL0$0$0 == 0x008a
_TL0	=	0x008a
G$TL1$0$0 == 0x008b
_TL1	=	0x008b
G$TH0$0$0 == 0x008c
_TH0	=	0x008c
G$TH1$0$0 == 0x008d
_TH1	=	0x008d
G$P0$0$0 == 0x0080
_P0	=	0x0080
G$P1$0$0 == 0x0090
_P1	=	0x0090
G$P2$0$0 == 0x00a0
_P2	=	0x00a0
G$P3$0$0 == 0x00b0
_P3	=	0x00b0
G$I2CADD$0$0 == 0x009b
_I2CADD	=	0x009b
G$I2CADD1$0$0 == 0x0091
_I2CADD1	=	0x0091
G$I2CADD2$0$0 == 0x0092
_I2CADD2	=	0x0092
G$I2CADD3$0$0 == 0x0093
_I2CADD3	=	0x0093
G$I2CDAT$0$0 == 0x009a
_I2CDAT	=	0x009a
G$SCON$0$0 == 0x0098
_SCON	=	0x0098
G$SBUF$0$0 == 0x0099
_SBUF	=	0x0099
G$T3FD$0$0 == 0x009d
_T3FD	=	0x009d
G$T3CON$0$0 == 0x009e
_T3CON	=	0x009e
G$TIMECON$0$0 == 0x00a1
_TIMECON	=	0x00a1
G$HTHSEC$0$0 == 0x00a2
_HTHSEC	=	0x00a2
G$SEC$0$0 == 0x00a3
_SEC	=	0x00a3
G$MIN$0$0 == 0x00a4
_MIN	=	0x00a4
G$HOUR$0$0 == 0x00a5
_HOUR	=	0x00a5
G$INTVAL$0$0 == 0x00a6
_INTVAL	=	0x00a6
G$IE$0$0 == 0x00a8
_IE	=	0x00a8
G$IEIP2$0$0 == 0x00a9
_IEIP2	=	0x00a9
G$PWMCON$0$0 == 0x00ae
_PWMCON	=	0x00ae
G$PWM0L$0$0 == 0x00b1
_PWM0L	=	0x00b1
G$PWM0H$0$0 == 0x00b2
_PWM0H	=	0x00b2
G$PWM1L$0$0 == 0x00b3
_PWM1L	=	0x00b3
G$PWM1H$0$0 == 0x00b4
_PWM1H	=	0x00b4
G$IP$0$0 == 0x00b8
_IP	=	0x00b8
G$ECON$0$0 == 0x00b9
_ECON	=	0x00b9
G$EDATA1$0$0 == 0x00bc
_EDATA1	=	0x00bc
G$EDATA2$0$0 == 0x00bd
_EDATA2	=	0x00bd
G$EDATA3$0$0 == 0x00be
_EDATA3	=	0x00be
G$EDATA4$0$0 == 0x00bf
_EDATA4	=	0x00bf
G$EADRL$0$0 == 0x00c6
_EADRL	=	0x00c6
G$EADRH$0$0 == 0x00c7
_EADRH	=	0x00c7
G$WDCON$0$0 == 0x00c0
_WDCON	=	0x00c0
G$CHIPID$0$0 == 0x00c2
_CHIPID	=	0x00c2
G$T2CON$0$0 == 0x00c8
_T2CON	=	0x00c8
G$RCAP2L$0$0 == 0x00ca
_RCAP2L	=	0x00ca
G$RCAP2H$0$0 == 0x00cb
_RCAP2H	=	0x00cb
G$TL2$0$0 == 0x00cc
_TL2	=	0x00cc
G$TH2$0$0 == 0x00cd
_TH2	=	0x00cd
G$PSW$0$0 == 0x00d0
_PSW	=	0x00d0
G$DMAL$0$0 == 0x00d2
_DMAL	=	0x00d2
G$DMAH$0$0 == 0x00d3
_DMAH	=	0x00d3
G$DMAP$0$0 == 0x00d4
_DMAP	=	0x00d4
G$PLLCON$0$0 == 0x00d7
_PLLCON	=	0x00d7
G$PSMCON$0$0 == 0x00df
_PSMCON	=	0x00df
G$ACC$0$0 == 0x00e0
_ACC	=	0x00e0
G$I2CCON$0$0 == 0x00e8
_I2CCON	=	0x00e8
G$B$0$0 == 0x00f0
_B	=	0x00f0
G$ADCCON1$0$0 == 0x00ef
_ADCCON1	=	0x00ef
G$ADCCON2$0$0 == 0x00d8
_ADCCON2	=	0x00d8
G$ADCCON3$0$0 == 0x00f5
_ADCCON3	=	0x00f5
G$ADCDATAL$0$0 == 0x00d9
_ADCDATAL	=	0x00d9
G$ADCDATAH$0$0 == 0x00da
_ADCDATAH	=	0x00da
G$ADCOFSL$0$0 == 0x00f1
_ADCOFSL	=	0x00f1
G$ADCOFSH$0$0 == 0x00f2
_ADCOFSH	=	0x00f2
G$ADCGAINL$0$0 == 0x00f3
_ADCGAINL	=	0x00f3
G$ADCGAINH$0$0 == 0x00f4
_ADCGAINH	=	0x00f4
G$SPIDAT$0$0 == 0x00f7
_SPIDAT	=	0x00f7
G$SPICON$0$0 == 0x00f8
_SPICON	=	0x00f8
G$DAC0L$0$0 == 0x00f9
_DAC0L	=	0x00f9
G$DAC0H$0$0 == 0x00fa
_DAC0H	=	0x00fa
G$DAC1L$0$0 == 0x00fb
_DAC1L	=	0x00fb
G$DAC1H$0$0 == 0x00fc
_DAC1H	=	0x00fc
G$DACCON$0$0 == 0x00fd
_DACCON	=	0x00fd
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
G$TF1$0$0 == 0x008f
_TF1	=	0x008f
G$TR1$0$0 == 0x008e
_TR1	=	0x008e
G$TF0$0$0 == 0x008d
_TF0	=	0x008d
G$TR0$0$0 == 0x008c
_TR0	=	0x008c
G$IE1$0$0 == 0x008b
_IE1	=	0x008b
G$IT1$0$0 == 0x008a
_IT1	=	0x008a
G$IE0$0$0 == 0x0089
_IE0	=	0x0089
G$IT0$0$0 == 0x0088
_IT0	=	0x0088
G$P0_0$0$0 == 0x0080
_P0_0	=	0x0080
G$P0_1$0$0 == 0x0081
_P0_1	=	0x0081
G$P0_2$0$0 == 0x0082
_P0_2	=	0x0082
G$P0_3$0$0 == 0x0083
_P0_3	=	0x0083
G$P0_4$0$0 == 0x0084
_P0_4	=	0x0084
G$P0_5$0$0 == 0x0085
_P0_5	=	0x0085
G$P0_6$0$0 == 0x0086
_P0_6	=	0x0086
G$P0_7$0$0 == 0x0087
_P0_7	=	0x0087
G$P1_0$0$0 == 0x0090
_P1_0	=	0x0090
G$P1_1$0$0 == 0x0091
_P1_1	=	0x0091
G$P1_2$0$0 == 0x0092
_P1_2	=	0x0092
G$P1_3$0$0 == 0x0093
_P1_3	=	0x0093
G$P1_4$0$0 == 0x0094
_P1_4	=	0x0094
G$P1_5$0$0 == 0x0095
_P1_5	=	0x0095
G$P1_6$0$0 == 0x0096
_P1_6	=	0x0096
G$P1_7$0$0 == 0x0097
_P1_7	=	0x0097
G$T2EX$0$0 == 0x0091
_T2EX	=	0x0091
G$T2$0$0 == 0x0090
_T2	=	0x0090
G$P2_0$0$0 == 0x00a0
_P2_0	=	0x00a0
G$P2_1$0$0 == 0x00a1
_P2_1	=	0x00a1
G$P2_2$0$0 == 0x00a2
_P2_2	=	0x00a2
G$P2_3$0$0 == 0x00a3
_P2_3	=	0x00a3
G$P2_4$0$0 == 0x00a4
_P2_4	=	0x00a4
G$P2_5$0$0 == 0x00a5
_P2_5	=	0x00a5
G$P2_6$0$0 == 0x00a6
_P2_6	=	0x00a6
G$P2_7$0$0 == 0x00a7
_P2_7	=	0x00a7
G$P3_0$0$0 == 0x00b0
_P3_0	=	0x00b0
G$P3_1$0$0 == 0x00b1
_P3_1	=	0x00b1
G$P3_2$0$0 == 0x00b2
_P3_2	=	0x00b2
G$P3_3$0$0 == 0x00b3
_P3_3	=	0x00b3
G$P3_4$0$0 == 0x00b4
_P3_4	=	0x00b4
G$P3_5$0$0 == 0x00b5
_P3_5	=	0x00b5
G$P3_6$0$0 == 0x00b6
_P3_6	=	0x00b6
G$P3_7$0$0 == 0x00b7
_P3_7	=	0x00b7
G$RXD$0$0 == 0x00b0
_RXD	=	0x00b0
G$TXD$0$0 == 0x00b1
_TXD	=	0x00b1
G$INT0$0$0 == 0x00b2
_INT0	=	0x00b2
G$INT1$0$0 == 0x00b3
_INT1	=	0x00b3
G$T0$0$0 == 0x00b4
_T0	=	0x00b4
G$T1$0$0 == 0x00b5
_T1	=	0x00b5
G$WR$0$0 == 0x00b6
_WR	=	0x00b6
G$RD$0$0 == 0x00b7
_RD	=	0x00b7
G$SM0$0$0 == 0x009f
_SM0	=	0x009f
G$SM1$0$0 == 0x009e
_SM1	=	0x009e
G$SM2$0$0 == 0x009d
_SM2	=	0x009d
G$REN$0$0 == 0x009c
_REN	=	0x009c
G$TB8$0$0 == 0x009b
_TB8	=	0x009b
G$RB8$0$0 == 0x009a
_RB8	=	0x009a
G$TI$0$0 == 0x0099
_TI	=	0x0099
G$RI$0$0 == 0x0098
_RI	=	0x0098
G$EA$0$0 == 0x00af
_EA	=	0x00af
G$EADC$0$0 == 0x00ae
_EADC	=	0x00ae
G$ET2$0$0 == 0x00ad
_ET2	=	0x00ad
G$ES$0$0 == 0x00ac
_ES	=	0x00ac
G$ET1$0$0 == 0x00ab
_ET1	=	0x00ab
G$EX1$0$0 == 0x00aa
_EX1	=	0x00aa
G$ET0$0$0 == 0x00a9
_ET0	=	0x00a9
G$EX0$0$0 == 0x00a8
_EX0	=	0x00a8
G$PSI$0$0 == 0x00bf
_PSI	=	0x00bf
G$PADC$0$0 == 0x00be
_PADC	=	0x00be
G$PT2$0$0 == 0x00bd
_PT2	=	0x00bd
G$PS$0$0 == 0x00bc
_PS	=	0x00bc
G$PT1$0$0 == 0x00bb
_PT1	=	0x00bb
G$PX1$0$0 == 0x00ba
_PX1	=	0x00ba
G$PT0$0$0 == 0x00b9
_PT0	=	0x00b9
G$PX0$0$0 == 0x00b8
_PX0	=	0x00b8
G$PRE3$0$0 == 0x00c7
_PRE3	=	0x00c7
G$PRE2$0$0 == 0x00c6
_PRE2	=	0x00c6
G$PRE1$0$0 == 0x00c5
_PRE1	=	0x00c5
G$PRE0$0$0 == 0x00c4
_PRE0	=	0x00c4
G$WDIR$0$0 == 0x00c3
_WDIR	=	0x00c3
G$WDS$0$0 == 0x00c2
_WDS	=	0x00c2
G$WDE$0$0 == 0x00c1
_WDE	=	0x00c1
G$WDWR$0$0 == 0x00c0
_WDWR	=	0x00c0
G$TF2$0$0 == 0x00cf
_TF2	=	0x00cf
G$EXF2$0$0 == 0x00ce
_EXF2	=	0x00ce
G$RCLK$0$0 == 0x00cd
_RCLK	=	0x00cd
G$TCLK$0$0 == 0x00cc
_TCLK	=	0x00cc
G$EXEN2$0$0 == 0x00cb
_EXEN2	=	0x00cb
G$TR2$0$0 == 0x00ca
_TR2	=	0x00ca
G$CNT2$0$0 == 0x00c9
_CNT2	=	0x00c9
G$CAP2$0$0 == 0x00c8
_CAP2	=	0x00c8
G$CY$0$0 == 0x00d7
_CY	=	0x00d7
G$AC$0$0 == 0x00d6
_AC	=	0x00d6
G$F0$0$0 == 0x00d5
_F0	=	0x00d5
G$RS1$0$0 == 0x00d4
_RS1	=	0x00d4
G$RS0$0$0 == 0x00d3
_RS0	=	0x00d3
G$OV$0$0 == 0x00d2
_OV	=	0x00d2
G$F1$0$0 == 0x00d1
_F1	=	0x00d1
G$P$0$0 == 0x00d0
_P	=	0x00d0
G$ACC_0$0$0 == 0x00e0
_ACC_0	=	0x00e0
G$ACC_1$0$0 == 0x00e1
_ACC_1	=	0x00e1
G$ACC_2$0$0 == 0x00e2
_ACC_2	=	0x00e2
G$ACC_3$0$0 == 0x00e3
_ACC_3	=	0x00e3
G$ACC_4$0$0 == 0x00e4
_ACC_4	=	0x00e4
G$ACC_5$0$0 == 0x00e5
_ACC_5	=	0x00e5
G$ACC_6$0$0 == 0x00e6
_ACC_6	=	0x00e6
G$ACC_7$0$0 == 0x00e7
_ACC_7	=	0x00e7
G$MDO$0$0 == 0x00ef
_MDO	=	0x00ef
G$MDE$0$0 == 0x00ee
_MDE	=	0x00ee
G$MCO$0$0 == 0x00ed
_MCO	=	0x00ed
G$MDI$0$0 == 0x00ec
_MDI	=	0x00ec
G$I2CSI$0$0 == 0x00ef
_I2CSI	=	0x00ef
G$I2CGC$0$0 == 0x00ee
_I2CGC	=	0x00ee
G$I2CID1$0$0 == 0x00ed
_I2CID1	=	0x00ed
G$I2CID0$0$0 == 0x00ec
_I2CID0	=	0x00ec
G$I2CM$0$0 == 0x00eb
_I2CM	=	0x00eb
G$I2CRS$0$0 == 0x00ea
_I2CRS	=	0x00ea
G$I2CTX$0$0 == 0x00e9
_I2CTX	=	0x00e9
G$I2CI$0$0 == 0x00e8
_I2CI	=	0x00e8
G$B_0$0$0 == 0x00f0
_B_0	=	0x00f0
G$B_1$0$0 == 0x00f1
_B_1	=	0x00f1
G$B_2$0$0 == 0x00f2
_B_2	=	0x00f2
G$B_3$0$0 == 0x00f3
_B_3	=	0x00f3
G$B_4$0$0 == 0x00f4
_B_4	=	0x00f4
G$B_5$0$0 == 0x00f5
_B_5	=	0x00f5
G$B_6$0$0 == 0x00f6
_B_6	=	0x00f6
G$B_7$0$0 == 0x00f7
_B_7	=	0x00f7
G$ADCI$0$0 == 0x00df
_ADCI	=	0x00df
G$DMA$0$0 == 0x00de
_DMA	=	0x00de
G$CCONV$0$0 == 0x00dd
_CCONV	=	0x00dd
G$SCONV$0$0 == 0x00dc
_SCONV	=	0x00dc
G$CS3$0$0 == 0x00db
_CS3	=	0x00db
G$CS2$0$0 == 0x00da
_CS2	=	0x00da
G$CS1$0$0 == 0x00d9
_CS1	=	0x00d9
G$CS0$0$0 == 0x00d8
_CS0	=	0x00d8
G$ISPI$0$0 == 0x00ff
_ISPI	=	0x00ff
G$WCOL$0$0 == 0x00fe
_WCOL	=	0x00fe
G$SPE$0$0 == 0x00fd
_SPE	=	0x00fd
G$SPIM$0$0 == 0x00fc
_SPIM	=	0x00fc
G$CPOL$0$0 == 0x00fb
_CPOL	=	0x00fb
G$CPHA$0$0 == 0x00fa
_CPHA	=	0x00fa
G$SPR1$0$0 == 0x00f9
_SPR1	=	0x00f9
G$SPR0$0$0 == 0x00f8
_SPR0	=	0x00f8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
Lmain.main$sloc0$1$0==.
_main_sloc0_1_0:
	.ds 4
Lmain.Obl_przeplywM$sloc0$1$0==.
_Obl_przeplywM_sloc0_1_0:
	.ds 4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
Fmain$NoweWyniki$0$0==.
_NoweWyniki:
	.ds 1
Fmain$modbus$0$0==.
_modbus:
	.ds 1
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
Fmain$buffer$0$0==.
_buffer:
	.ds 20
Fmain$wiadomosc$0$0==.
_wiadomosc:
	.ds 12
Fmain$str$0$0==.
_str:
	.ds 10
Fmain$WynikADC$0$0==.
_WynikADC:
	.ds 2
Fmain$CzasT2$0$0==.
_CzasT2:
	.ds 2
Fmain$WynikOkres$0$0==.
_WynikOkres:
	.ds 2
Fmain$WynikFaza$0$0==.
_WynikFaza:
	.ds 2
Fmain$n$0$0==.
_n:
	.ds 2
Fmain$sr_przeplM$0$0==.
_sr_przeplM:
	.ds 4
Fmain$sr_przeplO$0$0==.
_sr_przeplO:
	.ds 4
Fmain$calk_przeplM$0$0==.
_calk_przeplM:
	.ds 4
Fmain$calk_przeplO$0$0==.
_calk_przeplO:
	.ds 4
Fmain$crc_modbus$0$0==.
_crc_modbus:
	.ds 2
Fmain$zapytanie_master$0$0==.
_zapytanie_master:
	.ds 8
Fmain$Znak$0$0==.
_Znak:
	.ds 1
Fmain$literka$0$0==.
_literka:
	.ds 1
Lmain.update_crc_16$c$1$72==.
_update_crc_16_PARM_2:
	.ds 1
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	ljmp	_IntO_int
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	C$main.c$21$1$81 ==.
;	main.c:21: static bool NoweWyniki=0;	// Znacznik "nowe wyniki"  1 wyniki 0 czekaj
	clr	_NoweWyniki
	C$main.c$31$1$81 ==.
;	main.c:31: static bool modbus=0; //modbus uruchamia modbusa
	clr	_modbus
	C$main.c$22$1$81 ==.
;	main.c:22: static int n=0;             // Liczba do średniej arytmetycznej
	mov	r0,#_n
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	C$main.c$23$1$81 ==.
;	main.c:23: static float sr_przeplM=0;     //
	mov	r0,#_sr_przeplM
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	C$main.c$24$1$81 ==.
;	main.c:24: static float sr_przeplO=0;     // Średni przepływ objętościowy
	mov	r0,#_sr_przeplO
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	C$main.c$25$1$81 ==.
;	main.c:25: static float calk_przeplM=0;
	mov	r0,#_calk_przeplM
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	C$main.c$26$1$81 ==.
;	main.c:26: static float calk_przeplO=0;   // Całkoity
	mov	r0,#_calk_przeplO
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'IntO_int'
;------------------------------------------------------------
	G$IntO_int$0$0 ==.
	C$main.c$42$0$0 ==.
;	main.c:42: void IntO_int (void) __interrupt (0) 	// przerwanie z Int0
;	-----------------------------------------
;	 function IntO_int
;	-----------------------------------------
_IntO_int:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	push	acc
	push	ar0
	push	psw
	mov	psw,#0x00
	C$main.c$48$1$54 ==.
;	main.c:48: __endasm;								/**Koniec kodu assemblera**/
	mov _WynikFaza,TL0
	mov _WynikFaza+1,TH0
	C$main.c$49$1$54 ==.
;	main.c:49: TH0 = 0;			  	// Zeruj czêœæ doln¹ licznika T0
	mov	_TH0,#0x00
	C$main.c$50$1$54 ==.
;	main.c:50: TL0 = 0;				// Zeruj czêœæ górn¹ licznika T0
	mov	_TL0,#0x00
	C$main.c$56$1$54 ==.
;	main.c:56: __endasm;								/**Koniec kodu assemblera**/
	mov _CzasT2,RCAP2L
	mov _CzasT2+1,RCAP2H
	C$main.c$57$1$54 ==.
;	main.c:57: WynikOkres =CzasT2-WynikOkres;  	// Oblicz okres jako przyrost czasu
	mov	r0,#_WynikOkres
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	C$main.c$58$1$54 ==.
;	main.c:58: NoweWyniki = 1;			// Ustaw znacznik  "nowe wyniki"
	setb	_NoweWyniki
	pop	psw
	pop	ar0
	pop	acc
	C$main.c$59$1$54 ==.
	XG$IntO_int$0$0 ==.
	reti
;	eliminated unneeded push/pop ar1
;	eliminated unneeded push/pop dpl
;	eliminated unneeded push/pop dph
;	eliminated unneeded push/pop b
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;sloc0                     Allocated with name '_main_sloc0_1_0'
;------------------------------------------------------------
	G$main$0$0 ==.
	C$main.c$61$1$54 ==.
;	main.c:61: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	C$main.c$65$1$56 ==.
;	main.c:65: P1=0b00000001;		// p1.0  Wejscie analogowe reszta cyfrowe ( 0 cyfrowe, 1 analogowe)
	mov	_P1,#0x01
	C$main.c$67$1$56 ==.
;	main.c:67: T3CON =0x82;			// T3BAUDEN(0x80) + DIV ( DIV=log(Fosc/16/BaudRate)/log(2)) 115200
	mov	_T3CON,#0x82
	C$main.c$68$1$56 ==.
;	main.c:68: T3FD =40;			// T3FD (2*Fosc/2^(DIV-1)/BaudRate)-64
	mov	_T3FD,#0x28
	C$main.c$70$1$56 ==.
;	main.c:70: SCON = SM1 | REN;
	mov	c,_SM1
	clr	a
	rlc	a
	mov	r7,a
	mov	c,_REN
	clr	a
	rlc	a
	orl	a,r7
	mov	_SCON,a
	C$main.c$71$1$56 ==.
;	main.c:71: TI=1;					//Flaga transmisji portu szeregowego
	setb	_TI
	C$main.c$73$1$56 ==.
;	main.c:73: ADCCON1 =0xB4;		// Tryb paracy przetwornika ADC (ADC Clock 12MHz/3 )
	mov	_ADCCON1,#0xB4
	C$main.c$74$1$56 ==.
;	main.c:74: ADCCON2 =0x00;			// Ustawienie kanalu przetwornika KANAL 0
	mov	_ADCCON2,#0x00
	C$main.c$75$1$56 ==.
;	main.c:75: SCONV =0;			// Gotowosc do konwersji
	clr	_SCONV
	C$main.c$77$1$56 ==.
;	main.c:77: TH0 =0;			//Zeruj starsz¹ czêœæ licznika T0
	mov	_TH0,#0x00
	C$main.c$78$1$56 ==.
;	main.c:78: TL0 =0;			//Zeruj m³odsz¹ czêœæ licznika T0
	mov	_TL0,#0x00
	C$main.c$79$1$56 ==.
;	main.c:79: TMOD =0b00001001;		// T1: wyl,  T0: Gate0=1, C/T0=0 tryb0=01
	mov	_TMOD,#0x09
	C$main.c$80$1$56 ==.
;	main.c:80: TR0 =1;			// T0 start zliczania
	setb	_TR0
	C$main.c$82$1$56 ==.
;	main.c:82: TH2 = 0;			// Zeruj czêœæ górną licznika T2
	mov	_TH2,#0x00
	C$main.c$83$1$56 ==.
;	main.c:83: TL2 = 0;			// Zeruj czêœæ dolną licznika T2
	mov	_TL2,#0x00
	C$main.c$84$1$56 ==.
;	main.c:84: CAP2 = 1;			// T2 praca z zatraskiwaniem wartosci licznika
	setb	_CAP2
	C$main.c$85$1$56 ==.
;	main.c:85: CNT2 = 0;			// Czasomierz (zliczanie impulsow 12 MHz)
	clr	_CNT2
	C$main.c$86$1$56 ==.
;	main.c:86: EXEN2 = 1;			// Aktywacja wejscia T2EX (P1.1)
	setb	_EXEN2
	C$main.c$87$1$56 ==.
;	main.c:87: TR2 = 1;			// Sterowanie zliczaniem (1 start zliczania)
	setb	_TR2
	C$main.c$89$1$56 ==.
;	main.c:89: IT0 =1;			// I0 zglaszane zboczem opadajacym
	setb	_IT0
	C$main.c$90$1$56 ==.
;	main.c:90: EX0 =1;			// Odblokuj przerwanie od INT0
	setb	_EX0
	C$main.c$91$1$56 ==.
;	main.c:91: EA =1;			// Odblokuj niezablokowane przerwania
	setb	_EA
	C$main.c$93$1$56 ==.
;	main.c:93: CFG842 = EXTCLK; // Okblokowuje źródło zegara zewnętrznego 32,768kHz
	mov	_CFG842,#0x10
	C$main.c$94$1$56 ==.
;	main.c:94: TIMECON = TCEN | ITS0; // Inicjalizacja TIC, interwal 1/128s
	mov	_TIMECON,#0x11
	C$main.c$95$1$56 ==.
;	main.c:95: INTVAL=127; // Ustawia bit TII rejestru CFG842 co sekunde
	mov	_INTVAL,#0x7F
	C$main.c$96$1$56 ==.
;	main.c:96: LCD_Initalize(); // Inicjalizacja LCD
	lcall	_LCD_Initalize
	C$main.c$97$1$56 ==.
;	main.c:97: LCD_Clear();
	lcall	_LCD_Clear
	C$main.c$99$1$56 ==.
;	main.c:99: while(1)			//***********************Pêtla g³ówna programu**************************
00124$:
	C$main.c$101$2$57 ==.
;	main.c:101: if (NoweWyniki)		// jesli nowe wyniki to wyslij
	jb	_NoweWyniki,00192$
	ljmp	00111$
00192$:
	C$main.c$103$3$58 ==.
;	main.c:103: crc_modbus=0xffff; // CRC dla modbus musi zaczynac się od ffff
	mov	r0,#_crc_modbus
	mov	a,#0xFF
	movx	@r0,a
	inc	r0
	mov	a,#0xFF
	movx	@r0,a
	C$main.c$104$3$58 ==.
;	main.c:104: SCONV =1;     	      	// Inicjuj pomiar w kanale 0
	setb	_SCONV
	C$main.c$105$3$58 ==.
;	main.c:105: NoweWyniki =0;		    // Kasuj znacznik "nowe wyniki"
	clr	_NoweWyniki
	C$main.c$109$3$58 ==.
;	main.c:109: if (modbus==1)
	jnb	_modbus,00102$
	C$main.c$111$5$60 ==.
;	main.c:111: for (i=0; i<8; i++){
	mov	r6,#0x00
	mov	r7,#0x00
00126$:
	clr	c
	mov	a,r6
	subb	a,#0x08
	mov	a,r7
	xrl	a,#0x80
	subb	a,#0x80
	jnc	00129$
	C$main.c$112$5$60 ==.
;	main.c:112: zapytanie_master[i]=getchar();
	mov	a,r6
	add	a,#_zapytanie_master
	mov	r1,a
	push	ar7
	push	ar6
	push	ar1
	lcall	_getchar
	mov	a,dpl
	pop	ar1
	pop	ar6
	pop	ar7
	movx	@r1,a
	C$main.c$111$4$59 ==.
;	main.c:111: for (i=0; i<8; i++){
	inc	r6
	cjne	r6,#0x00,00126$
	inc	r7
	sjmp	00126$
00129$:
	C$main.c$115$1$56 ==.
;	main.c:115: for (i=0; i<8; i++){
	mov	r6,#0x00
	mov	r7,#0x00
00130$:
	clr	c
	mov	a,r6
	subb	a,#0x08
	mov	a,r7
	xrl	a,#0x80
	subb	a,#0x80
	jnc	00102$
	C$main.c$116$5$61 ==.
;	main.c:116: crc_modbus=update_crc_16(crc_modbus,zapytanie_master[i]);
	mov	a,r6
	add	a,#_zapytanie_master
	mov	r1,a
	mov	r0,#_update_crc_16_PARM_2
	movx	a,@r1
	movx	@r0,a
	mov	r0,#_crc_modbus
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	push	ar7
	push	ar6
	lcall	_update_crc_16
	mov	a,dpl
	mov	b,dph
	pop	ar6
	pop	ar7
	mov	r0,#_crc_modbus
	movx	@r0,a
	inc	r0
	mov	a,b
	movx	@r0,a
	C$main.c$115$4$59 ==.
;	main.c:115: for (i=0; i<8; i++){
	inc	r6
	cjne	r6,#0x00,00130$
	inc	r7
	sjmp	00130$
00102$:
	C$main.c$120$3$58 ==.
;	main.c:120: if (crc_modbus==0 && (zapytanie_master[0]==0x11) && (zapytanie_master[1]==0x03)){
	mov	r0,#_crc_modbus
	movx	a,@r0
	mov	b,a
	inc	r0
	movx	a,@r0
	orl	a,b
	jz	00198$
	ljmp	00111$
00198$:
	mov	r0,#_zapytanie_master
	movx	a,@r0
	mov	r7,a
	cjne	r7,#0x11,00199$
	sjmp	00200$
00199$:
	ljmp	00111$
00200$:
	mov	r0,#(_zapytanie_master + 0x0001)
	movx	a,@r0
	mov	r7,a
	cjne	r7,#0x03,00201$
	sjmp	00202$
00201$:
	ljmp	00111$
00202$:
	C$main.c$122$4$62 ==.
;	main.c:122: crc_modbus=0xffff; // CRC dla modbus musi zaczynac się od ffff
	mov	r0,#_crc_modbus
	mov	a,#0xFF
	movx	@r0,a
	inc	r0
	mov	a,#0xFF
	movx	@r0,a
	C$main.c$123$4$62 ==.
;	main.c:123: wiadomosc[0]=0x11;		// Adres SLAVE 17
	mov	r0,#_wiadomosc
	mov	a,#0x11
	movx	@r0,a
	C$main.c$124$4$62 ==.
;	main.c:124: wiadomosc[1]=0x03;		// Funkcja - czystanie stanu rejestrów
	mov	r0,#(_wiadomosc + 0x0001)
	mov	a,#0x03
	movx	@r0,a
	C$main.c$125$4$62 ==.
;	main.c:125: wiadomosc[3]=0x06;      // Liczba bajtów do wysłania
	mov	r0,#(_wiadomosc + 0x0003)
	mov	a,#0x06
	movx	@r0,a
	C$main.c$126$4$62 ==.
;	main.c:126: wiadomosc[4]=WynikOkres>>8;	// Starsza czesc Okresu
	mov	r0,#(_WynikOkres + 1)
	movx	a,@r0
	mov	r7,a
	mov	r0,#(_wiadomosc + 0x0004)
	mov	a,r7
	movx	@r0,a
	C$main.c$127$4$62 ==.
;	main.c:127: wiadomosc[5]=WynikOkres;	// Mlodsza czesc Okresu
	mov	r0,#_WynikOkres
	movx	a,@r0
	mov	r7,a
	mov	r0,#(_wiadomosc + 0x0005)
	mov	a,r7
	movx	@r0,a
	C$main.c$128$4$62 ==.
;	main.c:128: wiadomosc[6]=WynikFaza>>8;	// Starsza cesc Fazy
	mov	r0,#(_WynikFaza + 1)
	movx	a,@r0
	mov	r7,a
	mov	r0,#(_wiadomosc + 0x0006)
	mov	a,r7
	movx	@r0,a
	C$main.c$129$4$62 ==.
;	main.c:129: wiadomosc[7]=WynikFaza;	// Mlodsza czesc Fazy
	mov	r0,#_WynikFaza
	movx	a,@r0
	mov	r7,a
	mov	r0,#(_wiadomosc + 0x0007)
	mov	a,r7
	movx	@r0,a
	C$main.c$130$4$62 ==.
;	main.c:130: while (SCONV);		    // Czekaj na koniec konwersji
00103$:
	jb	_SCONV,00103$
	C$main.c$131$4$62 ==.
;	main.c:131: wiadomosc[8]=ADCDATAH;	// Wyslij czesc starsza Temperatury
	mov	r0,#(_wiadomosc + 0x0008)
	mov	a,_ADCDATAH
	movx	@r0,a
	C$main.c$132$4$62 ==.
;	main.c:132: wiadomosc[9]=ADCDATAL;	// Wyslij czesc mlodsza Temperatury
	mov	r0,#(_wiadomosc + 0x0009)
	mov	a,_ADCDATAL
	movx	@r0,a
	C$main.c$133$4$62 ==.
;	main.c:133: _delay_us(200);
	mov	dpl,#0xC8
	lcall	__delay_us
	C$main.c$134$4$62 ==.
;	main.c:134: _delay_us(147);// Czas 4 znaków
	mov	dpl,#0x93
	lcall	__delay_us
	C$main.c$135$1$56 ==.
;	main.c:135: for (i=0; i<=9; i++){
	mov	r6,#0x00
	mov	r7,#0x00
00134$:
	clr	c
	mov	a,#0x09
	subb	a,r6
	clr	a
	xrl	a,#0x80
	mov	b,r7
	xrl	b,#0x80
	subb	a,b
	jc	00137$
	C$main.c$136$5$63 ==.
;	main.c:136: crc_modbus=update_crc_16(crc_modbus,wiadomosc[i]);
	mov	a,r6
	add	a,#_wiadomosc
	mov	r1,a
	mov	r0,#_update_crc_16_PARM_2
	movx	a,@r1
	movx	@r0,a
	mov	r0,#_crc_modbus
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	push	ar7
	push	ar6
	lcall	_update_crc_16
	mov	a,dpl
	mov	b,dph
	pop	ar6
	pop	ar7
	mov	r0,#_crc_modbus
	movx	@r0,a
	inc	r0
	mov	a,b
	movx	@r0,a
	C$main.c$135$4$62 ==.
;	main.c:135: for (i=0; i<=9; i++){
	inc	r6
	cjne	r6,#0x00,00134$
	inc	r7
	sjmp	00134$
00137$:
	C$main.c$138$4$62 ==.
;	main.c:138: wiadomosc[10]=crc_modbus;       //Część mlodsza
	mov	r0,#_crc_modbus
	movx	a,@r0
	mov	r7,a
	mov	r0,#(_wiadomosc + 0x000a)
	mov	a,r7
	movx	@r0,a
	C$main.c$139$4$62 ==.
;	main.c:139: wiadomosc[11]=crc_modbus>>8;    //Część starsza
	mov	r0,#(_crc_modbus + 1)
	movx	a,@r0
	mov	r7,a
	mov	r0,#(_wiadomosc + 0x000b)
	mov	a,r7
	movx	@r0,a
	C$main.c$141$1$56 ==.
;	main.c:141: for (i=0; i<=11; i++){
	mov	r6,#0x00
	mov	r7,#0x00
00138$:
	clr	c
	mov	a,#0x0B
	subb	a,r6
	clr	a
	xrl	a,#0x80
	mov	b,r7
	xrl	b,#0x80
	subb	a,b
	jc	00111$
	C$main.c$142$5$64 ==.
;	main.c:142: putchar(wiadomosc[i]);
	mov	a,r6
	add	a,#_wiadomosc
	mov	r1,a
	movx	a,@r1
	mov	dpl,a
	push	ar7
	push	ar6
	lcall	_putchar
	pop	ar6
	pop	ar7
	C$main.c$141$4$62 ==.
;	main.c:141: for (i=0; i<=11; i++){
	inc	r6
	cjne	r6,#0x00,00138$
	inc	r7
	sjmp	00138$
00111$:
	C$main.c$149$2$57 ==.
;	main.c:149: if(TIMECON&TII) // Sprawdza czy upłynęła sekunda
	mov	a,_TIMECON
	jb	acc.2,00208$
	ljmp	00113$
00208$:
	C$main.c$151$3$65 ==.
;	main.c:151: TIMECON = ~TII; // Zeruje bit, odliczanie czasu od nowa
	mov	_TIMECON,#0xFB
	C$main.c$152$1$56 ==.
;	main.c:152: sr_przeplM = sr_przeplM/(float)n; // Uśrednianie
	mov	r0,#_n
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	lcall	___sint2fs
	mov	_main_sloc0_1_0,dpl
	mov	(_main_sloc0_1_0 + 1),dph
	mov	(_main_sloc0_1_0 + 2),b
	mov	(_main_sloc0_1_0 + 3),a
	push	_main_sloc0_1_0
	push	(_main_sloc0_1_0 + 1)
	push	(_main_sloc0_1_0 + 2)
	push	(_main_sloc0_1_0 + 3)
	mov	r0,#_sr_przeplM
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	inc	r0
	movx	a,@r0
	mov	b,a
	inc	r0
	movx	a,@r0
	lcall	___fsdiv
	mov	r2,dpl
	mov	r3,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,#_sr_przeplM
	mov	a,r2
	movx	@r0,a
	inc	r0
	mov	a,r3
	movx	@r0,a
	inc	r0
	mov	a,r6
	movx	@r0,a
	inc	r0
	mov	a,r7
	movx	@r0,a
	C$main.c$153$1$56 ==.
;	main.c:153: sr_przeplO = sr_przeplO/(float)n;
	push	_main_sloc0_1_0
	push	(_main_sloc0_1_0 + 1)
	push	(_main_sloc0_1_0 + 2)
	push	(_main_sloc0_1_0 + 3)
	mov	r0,#_sr_przeplO
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	inc	r0
	movx	a,@r0
	mov	b,a
	inc	r0
	movx	a,@r0
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,#_sr_przeplO
	mov	a,r4
	movx	@r0,a
	inc	r0
	mov	a,r5
	movx	@r0,a
	inc	r0
	mov	a,r6
	movx	@r0,a
	inc	r0
	mov	a,r7
	movx	@r0,a
	C$main.c$154$3$65 ==.
;	main.c:154: n=0;
	mov	r0,#_n
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	C$main.c$156$1$56 ==.
;	main.c:156: calk_przeplM+=sr_przeplM; //całkowanie przepływu
	mov	r0,#_sr_przeplM
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	mov	r0,#_calk_przeplM
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	inc	r0
	movx	a,@r0
	mov	b,a
	inc	r0
	movx	a,@r0
	lcall	___fsadd
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,#_calk_przeplM
	mov	a,r4
	movx	@r0,a
	inc	r0
	mov	a,r5
	movx	@r0,a
	inc	r0
	mov	a,r6
	movx	@r0,a
	inc	r0
	mov	a,r7
	movx	@r0,a
	C$main.c$157$1$56 ==.
;	main.c:157: calk_przeplO+=sr_przeplO;
	mov	r0,#_sr_przeplO
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	mov	r0,#_calk_przeplO
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	inc	r0
	movx	a,@r0
	mov	b,a
	inc	r0
	movx	a,@r0
	lcall	___fsadd
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,#_calk_przeplO
	mov	a,r4
	movx	@r0,a
	inc	r0
	mov	a,r5
	movx	@r0,a
	inc	r0
	mov	a,r6
	movx	@r0,a
	inc	r0
	mov	a,r7
	movx	@r0,a
	C$main.c$159$3$65 ==.
;	main.c:159: sprintf(str,"fi_m %f ", sr_przeplM);
	mov	r0,#_sr_przeplM
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	mov	a,#__str_0
	push	acc
	mov	a,#(__str_0 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	mov	a,#_str
	push	acc
	mov	a,#(_str >> 8)
	push	acc
	mov	a,#0x60
	push	acc
	lcall	_sprintf
	mov	a,sp
	add	a,#0xf6
	mov	sp,a
	C$main.c$160$3$65 ==.
;	main.c:160: LCD_WriteText(str);
	mov	dptr,#_str
	mov	b,#0x60
	lcall	_LCD_WriteText
	C$main.c$161$3$65 ==.
;	main.c:161: sprintf(str,"fi_m %f ", sr_przeplO);
	mov	r0,#_sr_przeplO
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	inc	r0
	movx	a,@r0
	push	acc
	mov	a,#__str_0
	push	acc
	mov	a,#(__str_0 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
	mov	a,#_str
	push	acc
	mov	a,#(_str >> 8)
	push	acc
	mov	a,#0x60
	push	acc
	lcall	_sprintf
	mov	a,sp
	add	a,#0xf6
	mov	sp,a
	C$main.c$162$3$65 ==.
;	main.c:162: LCD_WriteText(str);
	mov	dptr,#_str
	mov	b,#0x60
	lcall	_LCD_WriteText
	ljmp	00114$
00113$:
	C$main.c$167$3$66 ==.
;	main.c:167: n++;    //dzielnik do średniej arytmetycznej
	mov	r0,#_n
	movx	a,@r0
	add	a,#0x01
	movx	@r0,a
	inc	r0
	movx	a,@r0
	addc	a,#0x00
	movx	@r0,a
	C$main.c$168$3$66 ==.
;	main.c:168: sr_przeplM+=Obl_przeplywM();    //
	lcall	_Obl_przeplywM
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	r0,#_sr_przeplM
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	inc	r0
	movx	a,@r0
	mov	b,a
	inc	r0
	movx	a,@r0
	lcall	___fsadd
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,#_sr_przeplM
	mov	a,r4
	movx	@r0,a
	inc	r0
	mov	a,r5
	movx	@r0,a
	inc	r0
	mov	a,r6
	movx	@r0,a
	inc	r0
	mov	a,r7
	movx	@r0,a
	C$main.c$169$3$66 ==.
;	main.c:169: sr_przeplO+=Obl_przeplywO();    //
	lcall	_Obl_przeplywO
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	r0,#_sr_przeplO
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	inc	r0
	movx	a,@r0
	mov	b,a
	inc	r0
	movx	a,@r0
	lcall	___fsadd
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,#_sr_przeplO
	mov	a,r4
	movx	@r0,a
	inc	r0
	mov	a,r5
	movx	@r0,a
	inc	r0
	mov	a,r6
	movx	@r0,a
	inc	r0
	mov	a,r7
	movx	@r0,a
00114$:
	C$main.c$172$2$57 ==.
;	main.c:172: if(!RI) continue;				// Brak znaku skok na poczatek petli (czekaj na odbior znaku)
	C$main.c$173$2$57 ==.
;	main.c:173: RI =0;				// Zeruj flage odebranego znaku
	jbc	_RI,00209$
	ljmp	00124$
00209$:
	C$main.c$174$2$57 ==.
;	main.c:174: Znak =SBUF;		//Wysy³a znak do bufora nadawczego
	mov	r7,_SBUF
	mov	r0,#_Znak
	mov	a,r7
	movx	@r0,a
	C$main.c$175$2$57 ==.
;	main.c:175: switch (Znak)
	mov	ar6,r7
	cjne	r6,#0x41,00210$
	sjmp	00117$
00210$:
	cjne	r6,#0x42,00211$
	sjmp	00118$
00211$:
	cjne	r6,#0x4D,00212$
	sjmp	00119$
00212$:
	C$main.c$177$3$67 ==.
;	main.c:177: case 'A':			// Rzadanie potwierdzenia komunikacji
	cjne	r6,#0x6D,00121$
	sjmp	00120$
00117$:
	C$main.c$178$3$67 ==.
;	main.c:178: putchar('E');		// Potwierdzenie komunikacji
	mov	dpl,#0x45
	lcall	_putchar
	C$main.c$179$3$67 ==.
;	main.c:179: break;
	ljmp	00124$
	C$main.c$180$3$67 ==.
;	main.c:180: case 'B':
00118$:
	C$main.c$181$3$67 ==.
;	main.c:181: putchar('e');
	mov	dpl,#0x65
	lcall	_putchar
	C$main.c$182$3$67 ==.
;	main.c:182: literka=getchar();
	lcall	_getchar
	mov	a,dpl
	mov	r0,#_literka
	movx	@r0,a
	C$main.c$183$3$67 ==.
;	main.c:183: break;
	ljmp	00124$
	C$main.c$184$3$67 ==.
;	main.c:184: case 'M':
00119$:
	C$main.c$185$3$67 ==.
;	main.c:185: modbus=1;
	setb	_modbus
	C$main.c$186$3$67 ==.
;	main.c:186: break;
	ljmp	00124$
	C$main.c$187$3$67 ==.
;	main.c:187: case 'm':
00120$:
	C$main.c$188$3$67 ==.
;	main.c:188: modbus=0;
	clr	_modbus
	C$main.c$189$3$67 ==.
;	main.c:189: break;
	ljmp	00124$
	C$main.c$190$3$67 ==.
;	main.c:190: default:			// Odeslij nierozpoznany znak
00121$:
	C$main.c$191$3$67 ==.
;	main.c:191: putchar(Znak);  // Odsy³a nierozpoznany znak
	mov	dpl,r7
	lcall	_putchar
	C$main.c$193$1$56 ==.
;	main.c:193: }
	ljmp	00124$
	C$main.c$195$1$56 ==.
	XG$main$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
	G$putchar$0$0 ==.
	C$main.c$199$1$56 ==.
;	main.c:199: void putchar(unsigned char V)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
	mov	r7,dpl
	C$main.c$201$1$69 ==.
;	main.c:201: while(! TI);				// Czekaj na ewentualny koniec wyslania
00101$:
	C$main.c$202$1$69 ==.
;	main.c:202: TI =0;				// Zeruj flage wyslania
	jbc	_TI,00110$
	sjmp	00101$
00110$:
	C$main.c$203$1$69 ==.
;	main.c:203: SBUF=V;				// Wyslij znak do bufora nadawczego
	mov	_SBUF,r7
	C$main.c$204$1$69 ==.
	XG$putchar$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
	G$getchar$0$0 ==.
	C$main.c$206$1$69 ==.
;	main.c:206: extern char getchar(void)
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
	C$main.c$209$1$71 ==.
;	main.c:209: while(!RI); //czekaj aż odbierze
00101$:
	C$main.c$210$1$71 ==.
;	main.c:210: RI=0;
	jbc	_RI,00110$
	sjmp	00101$
00110$:
	C$main.c$211$1$71 ==.
;	main.c:211: znak1 = SBUF;
	mov	dpl,_SBUF
	C$main.c$212$1$71 ==.
;	main.c:212: return znak1;
	C$main.c$213$1$71 ==.
	XG$getchar$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'update_crc_16'
;------------------------------------------------------------
	G$update_crc_16$0$0 ==.
	C$main.c$225$1$71 ==.
;	main.c:225: unsigned short update_crc_16( unsigned short crc, char c )
;	-----------------------------------------
;	 function update_crc_16
;	-----------------------------------------
_update_crc_16:
	mov	r6,dpl
	mov	r7,dph
	C$main.c$229$1$73 ==.
;	main.c:229: short_c = 0x00ff & (unsigned short) c;
	mov	r0,#_update_crc_16_PARM_2
	movx	a,@r0
	mov	r4,a
	movx	a,@r0
	rlc	a
	subb	a,acc
	mov	r5,#0x00
	C$main.c$231$1$73 ==.
;	main.c:231: tmp =  crc       ^ short_c;
	mov	a,r6
	xrl	ar4,a
	mov	a,r7
	xrl	ar5,a
	C$main.c$232$1$73 ==.
;	main.c:232: crc = (crc >> 8) ^ crc_tab16[ tmp & 0xff ];
	mov	ar2,r7
	clr	a
	mov	r3,a
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
	mov	a,r4
	add	a,#_crc_tab16
	mov	dpl,a
	mov	a,r5
	addc	a,#(_crc_tab16 >> 8)
	mov	dph,a
	clr	a
	movc	a,@a+dptr
	mov	r4,a
	inc	dptr
	clr	a
	movc	a,@a+dptr
	mov	r5,a
	mov	a,r4
	xrl	a,r2
	mov	r6,a
	mov	a,r5
	xrl	a,r3
	C$main.c$234$1$73 ==.
;	main.c:234: return crc;
	C$main.c$235$1$73 ==.
	XG$update_crc_16$0$0 ==.
	mov	dpl,r6
	mov	dph,a
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Obl_przeplywM'
;------------------------------------------------------------
;sloc0                     Allocated with name '_Obl_przeplywM_sloc0_1_0'
;------------------------------------------------------------
	G$Obl_przeplywM$0$0 ==.
	C$main.c$237$1$73 ==.
;	main.c:237: float Obl_przeplywM(void)
;	-----------------------------------------
;	 function Obl_przeplywM
;	-----------------------------------------
_Obl_przeplywM:
	C$main.c$240$1$75 ==.
;	main.c:240: fiM = (float)WynikFaza;
	mov	r0,#_WynikFaza
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	lcall	___uint2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	C$main.c$241$1$75 ==.
;	main.c:241: fiM = (0.8096*fiM)-127.26;
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x41F2
	mov	b,#0x4F
	mov	a,#0x3F
	lcall	___fsmul
	mov	_Obl_przeplywM_sloc0_1_0,dpl
	mov	(_Obl_przeplywM_sloc0_1_0 + 1),dph
	mov	(_Obl_przeplywM_sloc0_1_0 + 2),b
	mov	(_Obl_przeplywM_sloc0_1_0 + 3),a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	a,#0x1F
	push	acc
	mov	a,#0x85
	push	acc
	mov	a,#0xFE
	push	acc
	mov	a,#0x42
	push	acc
	mov	dpl,_Obl_przeplywM_sloc0_1_0
	mov	dph,(_Obl_przeplywM_sloc0_1_0 + 1)
	mov	b,(_Obl_przeplywM_sloc0_1_0 + 2)
	mov	a,(_Obl_przeplywM_sloc0_1_0 + 3)
	lcall	___fssub
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	C$main.c$242$1$75 ==.
;	main.c:242: return fiM;
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	C$main.c$243$1$75 ==.
	XG$Obl_przeplywM$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Obl_czestotliwosc'
;------------------------------------------------------------
	G$Obl_czestotliwosc$0$0 ==.
	C$main.c$245$1$75 ==.
;	main.c:245: float Obl_czestotliwosc(void)
;	-----------------------------------------
;	 function Obl_czestotliwosc
;	-----------------------------------------
_Obl_czestotliwosc:
	C$main.c$248$1$77 ==.
;	main.c:248: f = (float)WynikOkres/12000000.0;
	mov	r0,#_WynikOkres
	movx	a,@r0
	mov	dpl,a
	inc	r0
	movx	a,@r0
	mov	dph,a
	lcall	___uint2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	clr	a
	push	acc
	mov	a,#0x1B
	push	acc
	mov	a,#0x37
	push	acc
	mov	a,#0x4B
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	C$main.c$249$1$77 ==.
;	main.c:249: f = 1/f;
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x80
	mov	a,#0x3F
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	C$main.c$250$1$77 ==.
;	main.c:250: return f;
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	C$main.c$251$1$77 ==.
	XG$Obl_czestotliwosc$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Obl_przeplywO'
;------------------------------------------------------------
	G$Obl_przeplywO$0$0 ==.
	C$main.c$253$1$77 ==.
;	main.c:253: float Obl_przeplywO(void)
;	-----------------------------------------
;	 function Obl_przeplywO
;	-----------------------------------------
_Obl_przeplywO:
	C$main.c$256$1$79 ==.
;	main.c:256: fiO = Obl_przeplywM()/Obl_gestosc();
	lcall	_Obl_przeplywM
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	lcall	_Obl_gestosc
	mov	r0,dpl
	mov	r1,dph
	mov	r2,b
	mov	r3,a
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	push	ar0
	push	ar1
	push	ar2
	push	ar3
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	C$main.c$257$1$79 ==.
;	main.c:257: return fiO;
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	C$main.c$258$1$79 ==.
	XG$Obl_przeplywO$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Obl_gestosc'
;------------------------------------------------------------
	G$Obl_gestosc$0$0 ==.
	C$main.c$260$1$79 ==.
;	main.c:260: float Obl_gestosc(void)
;	-----------------------------------------
;	 function Obl_gestosc
;	-----------------------------------------
_Obl_gestosc:
	C$main.c$263$1$81 ==.
;	main.c:263: rho = (powf(81.92543/Obl_czestotliwosc(),2)-1)/0.084090608;
	lcall	_Obl_czestotliwosc
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0xD9D2
	mov	b,#0xA3
	mov	a,#0x42
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,#_powf_PARM_2
	clr	a
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	movx	@r0,a
	inc	r0
	mov	a,#0x40
	movx	@r0,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	_powf
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x80
	push	acc
	mov	a,#0x3F
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fssub
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	a,#0xB2
	push	acc
	mov	a,#0x37
	push	acc
	mov	a,#0xAC
	push	acc
	mov	a,#0x3D
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	C$main.c$264$1$81 ==.
;	main.c:264: return rho;
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	C$main.c$265$1$81 ==.
	XG$Obl_gestosc$0$0 ==.
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
G$crc_tab16$0$0 == .
_crc_tab16:
	.byte #0x00,#0x00	; 0
	.byte #0xC1,#0xC0	; 49345
	.byte #0x81,#0xC1	; 49537
	.byte #0x40,#0x01	; 320
	.byte #0x01,#0xC3	; 49921
	.byte #0xC0,#0x03	; 960
	.byte #0x80,#0x02	; 640
	.byte #0x41,#0xC2	; 49729
	.byte #0x01,#0xC6	; 50689
	.byte #0xC0,#0x06	; 1728
	.byte #0x80,#0x07	; 1920
	.byte #0x41,#0xC7	; 51009
	.byte #0x00,#0x05	; 1280
	.byte #0xC1,#0xC5	; 50625
	.byte #0x81,#0xC4	; 50305
	.byte #0x40,#0x04	; 1088
	.byte #0x01,#0xCC	; 52225
	.byte #0xC0,#0x0C	; 3264
	.byte #0x80,#0x0D	; 3456
	.byte #0x41,#0xCD	; 52545
	.byte #0x00,#0x0F	; 3840
	.byte #0xC1,#0xCF	; 53185
	.byte #0x81,#0xCE	; 52865
	.byte #0x40,#0x0E	; 3648
	.byte #0x00,#0x0A	; 2560
	.byte #0xC1,#0xCA	; 51905
	.byte #0x81,#0xCB	; 52097
	.byte #0x40,#0x0B	; 2880
	.byte #0x01,#0xC9	; 51457
	.byte #0xC0,#0x09	; 2496
	.byte #0x80,#0x08	; 2176
	.byte #0x41,#0xC8	; 51265
	.byte #0x01,#0xD8	; 55297
	.byte #0xC0,#0x18	; 6336
	.byte #0x80,#0x19	; 6528
	.byte #0x41,#0xD9	; 55617
	.byte #0x00,#0x1B	; 6912
	.byte #0xC1,#0xDB	; 56257
	.byte #0x81,#0xDA	; 55937
	.byte #0x40,#0x1A	; 6720
	.byte #0x00,#0x1E	; 7680
	.byte #0xC1,#0xDE	; 57025
	.byte #0x81,#0xDF	; 57217
	.byte #0x40,#0x1F	; 8000
	.byte #0x01,#0xDD	; 56577
	.byte #0xC0,#0x1D	; 7616
	.byte #0x80,#0x1C	; 7296
	.byte #0x41,#0xDC	; 56385
	.byte #0x00,#0x14	; 5120
	.byte #0xC1,#0xD4	; 54465
	.byte #0x81,#0xD5	; 54657
	.byte #0x40,#0x15	; 5440
	.byte #0x01,#0xD7	; 55041
	.byte #0xC0,#0x17	; 6080
	.byte #0x80,#0x16	; 5760
	.byte #0x41,#0xD6	; 54849
	.byte #0x01,#0xD2	; 53761
	.byte #0xC0,#0x12	; 4800
	.byte #0x80,#0x13	; 4992
	.byte #0x41,#0xD3	; 54081
	.byte #0x00,#0x11	; 4352
	.byte #0xC1,#0xD1	; 53697
	.byte #0x81,#0xD0	; 53377
	.byte #0x40,#0x10	; 4160
	.byte #0x01,#0xF0	; 61441
	.byte #0xC0,#0x30	; 12480
	.byte #0x80,#0x31	; 12672
	.byte #0x41,#0xF1	; 61761
	.byte #0x00,#0x33	; 13056
	.byte #0xC1,#0xF3	; 62401
	.byte #0x81,#0xF2	; 62081
	.byte #0x40,#0x32	; 12864
	.byte #0x00,#0x36	; 13824
	.byte #0xC1,#0xF6	; 63169
	.byte #0x81,#0xF7	; 63361
	.byte #0x40,#0x37	; 14144
	.byte #0x01,#0xF5	; 62721
	.byte #0xC0,#0x35	; 13760
	.byte #0x80,#0x34	; 13440
	.byte #0x41,#0xF4	; 62529
	.byte #0x00,#0x3C	; 15360
	.byte #0xC1,#0xFC	; 64705
	.byte #0x81,#0xFD	; 64897
	.byte #0x40,#0x3D	; 15680
	.byte #0x01,#0xFF	; 65281
	.byte #0xC0,#0x3F	; 16320
	.byte #0x80,#0x3E	; 16000
	.byte #0x41,#0xFE	; 65089
	.byte #0x01,#0xFA	; 64001
	.byte #0xC0,#0x3A	; 15040
	.byte #0x80,#0x3B	; 15232
	.byte #0x41,#0xFB	; 64321
	.byte #0x00,#0x39	; 14592
	.byte #0xC1,#0xF9	; 63937
	.byte #0x81,#0xF8	; 63617
	.byte #0x40,#0x38	; 14400
	.byte #0x00,#0x28	; 10240
	.byte #0xC1,#0xE8	; 59585
	.byte #0x81,#0xE9	; 59777
	.byte #0x40,#0x29	; 10560
	.byte #0x01,#0xEB	; 60161
	.byte #0xC0,#0x2B	; 11200
	.byte #0x80,#0x2A	; 10880
	.byte #0x41,#0xEA	; 59969
	.byte #0x01,#0xEE	; 60929
	.byte #0xC0,#0x2E	; 11968
	.byte #0x80,#0x2F	; 12160
	.byte #0x41,#0xEF	; 61249
	.byte #0x00,#0x2D	; 11520
	.byte #0xC1,#0xED	; 60865
	.byte #0x81,#0xEC	; 60545
	.byte #0x40,#0x2C	; 11328
	.byte #0x01,#0xE4	; 58369
	.byte #0xC0,#0x24	; 9408
	.byte #0x80,#0x25	; 9600
	.byte #0x41,#0xE5	; 58689
	.byte #0x00,#0x27	; 9984
	.byte #0xC1,#0xE7	; 59329
	.byte #0x81,#0xE6	; 59009
	.byte #0x40,#0x26	; 9792
	.byte #0x00,#0x22	; 8704
	.byte #0xC1,#0xE2	; 58049
	.byte #0x81,#0xE3	; 58241
	.byte #0x40,#0x23	; 9024
	.byte #0x01,#0xE1	; 57601
	.byte #0xC0,#0x21	; 8640
	.byte #0x80,#0x20	; 8320
	.byte #0x41,#0xE0	; 57409
	.byte #0x01,#0xA0	; 40961
	.byte #0xC0,#0x60	; 24768
	.byte #0x80,#0x61	; 24960
	.byte #0x41,#0xA1	; 41281
	.byte #0x00,#0x63	; 25344
	.byte #0xC1,#0xA3	; 41921
	.byte #0x81,#0xA2	; 41601
	.byte #0x40,#0x62	; 25152
	.byte #0x00,#0x66	; 26112
	.byte #0xC1,#0xA6	; 42689
	.byte #0x81,#0xA7	; 42881
	.byte #0x40,#0x67	; 26432
	.byte #0x01,#0xA5	; 42241
	.byte #0xC0,#0x65	; 26048
	.byte #0x80,#0x64	; 25728
	.byte #0x41,#0xA4	; 42049
	.byte #0x00,#0x6C	; 27648
	.byte #0xC1,#0xAC	; 44225
	.byte #0x81,#0xAD	; 44417
	.byte #0x40,#0x6D	; 27968
	.byte #0x01,#0xAF	; 44801
	.byte #0xC0,#0x6F	; 28608
	.byte #0x80,#0x6E	; 28288
	.byte #0x41,#0xAE	; 44609
	.byte #0x01,#0xAA	; 43521
	.byte #0xC0,#0x6A	; 27328
	.byte #0x80,#0x6B	; 27520
	.byte #0x41,#0xAB	; 43841
	.byte #0x00,#0x69	; 26880
	.byte #0xC1,#0xA9	; 43457
	.byte #0x81,#0xA8	; 43137
	.byte #0x40,#0x68	; 26688
	.byte #0x00,#0x78	; 30720
	.byte #0xC1,#0xB8	; 47297
	.byte #0x81,#0xB9	; 47489
	.byte #0x40,#0x79	; 31040
	.byte #0x01,#0xBB	; 47873
	.byte #0xC0,#0x7B	; 31680
	.byte #0x80,#0x7A	; 31360
	.byte #0x41,#0xBA	; 47681
	.byte #0x01,#0xBE	; 48641
	.byte #0xC0,#0x7E	; 32448
	.byte #0x80,#0x7F	; 32640
	.byte #0x41,#0xBF	; 48961
	.byte #0x00,#0x7D	; 32000
	.byte #0xC1,#0xBD	; 48577
	.byte #0x81,#0xBC	; 48257
	.byte #0x40,#0x7C	; 31808
	.byte #0x01,#0xB4	; 46081
	.byte #0xC0,#0x74	; 29888
	.byte #0x80,#0x75	; 30080
	.byte #0x41,#0xB5	; 46401
	.byte #0x00,#0x77	; 30464
	.byte #0xC1,#0xB7	; 47041
	.byte #0x81,#0xB6	; 46721
	.byte #0x40,#0x76	; 30272
	.byte #0x00,#0x72	; 29184
	.byte #0xC1,#0xB2	; 45761
	.byte #0x81,#0xB3	; 45953
	.byte #0x40,#0x73	; 29504
	.byte #0x01,#0xB1	; 45313
	.byte #0xC0,#0x71	; 29120
	.byte #0x80,#0x70	; 28800
	.byte #0x41,#0xB0	; 45121
	.byte #0x00,#0x50	; 20480
	.byte #0xC1,#0x90	; 37057
	.byte #0x81,#0x91	; 37249
	.byte #0x40,#0x51	; 20800
	.byte #0x01,#0x93	; 37633
	.byte #0xC0,#0x53	; 21440
	.byte #0x80,#0x52	; 21120
	.byte #0x41,#0x92	; 37441
	.byte #0x01,#0x96	; 38401
	.byte #0xC0,#0x56	; 22208
	.byte #0x80,#0x57	; 22400
	.byte #0x41,#0x97	; 38721
	.byte #0x00,#0x55	; 21760
	.byte #0xC1,#0x95	; 38337
	.byte #0x81,#0x94	; 38017
	.byte #0x40,#0x54	; 21568
	.byte #0x01,#0x9C	; 39937
	.byte #0xC0,#0x5C	; 23744
	.byte #0x80,#0x5D	; 23936
	.byte #0x41,#0x9D	; 40257
	.byte #0x00,#0x5F	; 24320
	.byte #0xC1,#0x9F	; 40897
	.byte #0x81,#0x9E	; 40577
	.byte #0x40,#0x5E	; 24128
	.byte #0x00,#0x5A	; 23040
	.byte #0xC1,#0x9A	; 39617
	.byte #0x81,#0x9B	; 39809
	.byte #0x40,#0x5B	; 23360
	.byte #0x01,#0x99	; 39169
	.byte #0xC0,#0x59	; 22976
	.byte #0x80,#0x58	; 22656
	.byte #0x41,#0x98	; 38977
	.byte #0x01,#0x88	; 34817
	.byte #0xC0,#0x48	; 18624
	.byte #0x80,#0x49	; 18816
	.byte #0x41,#0x89	; 35137
	.byte #0x00,#0x4B	; 19200
	.byte #0xC1,#0x8B	; 35777
	.byte #0x81,#0x8A	; 35457
	.byte #0x40,#0x4A	; 19008
	.byte #0x00,#0x4E	; 19968
	.byte #0xC1,#0x8E	; 36545
	.byte #0x81,#0x8F	; 36737
	.byte #0x40,#0x4F	; 20288
	.byte #0x01,#0x8D	; 36097
	.byte #0xC0,#0x4D	; 19904
	.byte #0x80,#0x4C	; 19584
	.byte #0x41,#0x8C	; 35905
	.byte #0x00,#0x44	; 17408
	.byte #0xC1,#0x84	; 33985
	.byte #0x81,#0x85	; 34177
	.byte #0x40,#0x45	; 17728
	.byte #0x01,#0x87	; 34561
	.byte #0xC0,#0x47	; 18368
	.byte #0x80,#0x46	; 18048
	.byte #0x41,#0x86	; 34369
	.byte #0x01,#0x82	; 33281
	.byte #0xC0,#0x42	; 17088
	.byte #0x80,#0x43	; 17280
	.byte #0x41,#0x83	; 33601
	.byte #0x00,#0x41	; 16640
	.byte #0xC1,#0x81	; 33217
	.byte #0x81,#0x80	; 32897
	.byte #0x40,#0x40	; 16448
Fmain$_str_0$0$0 == .
__str_0:
	.ascii "fi_m %f "
	.db 0x00
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
